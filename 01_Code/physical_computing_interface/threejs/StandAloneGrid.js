var color1= 0xffffff; /*white*/
var color2= 0x020227;  /*kohly*/
var color3= 0x1c5c61; /*teal*/
var color4= 0xfa6e70; //red/orange
var color5=0x380152; //purple
var color6=0x696767; //grey

var camera, scene, renderer;
var plane;
var mouse, raycaster, isShiftDown = false;
// var rollOverMesh, rollOverMaterial;
var cubeGeo, cubeMaterial,sphereGeo,helperMaterial,helperPosition,helperColor;
var objects = [];
var container = document.getElementById( 'webgl' );
// var container1 = document.getElementById( 'threejs' );
var controls;
var voxelSpacing=50.0;
var occupancy,occupancyHelper;



var gridPresets={
    Cubic :  {
	
        xScale:1.0,
        yScale:1.0,
        zScale:1.0,
        xLineOffset:0.0,
        yLineOffset:0.0,
        xLayerOffset:0.0,
        yLayerOffset:0.0,
        zLayerRotation:0.0,
        doubleOffset:false,
        voxelSpacing:voxelSpacing,
        voxelScaleX:0.95,
        voxelScaleY:0.95,
        voxelScaleZ:0.95,
        neighbors:[
            {x: 1,y: 0,z: 0},
            {x:-1,y: 0,z: 0},
            {x: 0,y: 1,z: 0},
            {x: 0,y:-1,z: 0},
            {x: 0,y: 0,z: 1},
            {x: 0,y: 0,z:-1}
        ],
        neighborsLayerOffset:[
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0}
        ]
    },
    Aspect_2_1_4 :  {
        xScale:1.0,
        yScale:0.5,
        zScale:2.0,
        xLineOffset:0.0,
        yLineOffset:0.0,
        xLayerOffset:0.0,
        yLayerOffset:0.0,
        zLayerRotation:0.0,
        doubleOffset:false,
        voxelSpacing:voxelSpacing,
        voxelScaleX:1.0,
        voxelScaleY:1.0,
        voxelScaleZ:1.0,
        neighbors:[
            {x: 1,y: 0,z: 0},
            {x:-1,y: 0,z: 0},
            {x: 0,y: 1,z: 0},
            {x: 0,y:-1,z: 0},
            {x: 0,y: 0,z: 1},
            {x: 0,y: 0,z:-1}
        ],
        neighborsLayerOffset:[
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0}
        ]
    },
    FaceCenteredCubic:  {
		xScale:1.0,
        yScale:1.0,
        zScale:Math.sqrt(2)/2,
        xLineOffset:0.0,
        yLineOffset:0.0,
        xLayerOffset:0.5,
        yLayerOffset:0.5,
        zLayerRotation:0.0,
        doubleOffset:false,
        voxelSpacing:voxelSpacing,
        voxelScaleX:1.0,
        voxelScaleY:1.0,
        voxelScaleZ:1.0,
        neighbors:[
            {x: 1,y: 0,z: 0},
            {x:-1,y: 0,z: 0},
            {x: 0,y: 1,z: 0},
            {x: 0,y:-1,z: 0},
            {x: 0,y: 0,z: 1},
            {x: 0,y: 0,z:-1}
        ],
        neighborsLayerOffset:[
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0}
        ]
		
    },
    HexagonalClosePacked:  {
	
		xScale:1.0,
        yScale:Math.sqrt(3)/2,
        zScale:Math.sqrt(2)/Math.sqrt(3),
        xLineOffset:0.5,
        yLineOffset:0.0,
        xLayerOffset:0.5,
        yLayerOffset:1.0/Math.sqrt(12),
        zLayerRotation:0.0,
        doubleOffset:false,
        voxelSpacing:voxelSpacing,
        voxelScaleX:1.0,
        voxelScaleY:1.0,
        voxelScaleZ:1.0,
        neighbors:[
            {x: 1,y: 0,z: 0},
            {x:-1,y: 0,z: 0},
            {x: 0,y: 1,z: 0},
            {x: 0,y:-1,z: 0},
            {x: 0,y: 0,z: 1},
            {x: 0,y: 0,z:-1}
        ],
        neighborsLayerOffset:[
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 0,z: 0}
        ]
		
    } ,
    Dice2:  {
		xScale:1.0,
        yScale:1.0,
        zScale:0.2,
        xLineOffset:0.0,
        yLineOffset:0.0,
        xLayerOffset:0.0,
        yLayerOffset:0.5,
        zLayerRotation:Math.PI/2.0,
        voxelSpacing:voxelSpacing,
        doubleOffset:false,
        voxelScaleX:0.5,
        voxelScaleY:0.9,
        voxelScaleZ:0.2,
        neighbors:[
            {x: 0,y: 0,z: 1},
            {x: 0,y: 0,z:-1},
            {x: 0,y:-1,z: 1},
            {x: 0,y:-1,z:-1}
        ],
        neighborsLayerOffset:[
            {x: 0,y: 1,z: 0},
            {x: 0,y: 0,z: 0},
            {x: 0,y: 1,z: 0},
            {x: 0,y: 1,z: 0} //if rotated
        ]
		
    },
    Dice:  {
		xScale:1.0,
        yScale:1.0,
        zScale:0.2,
        xLineOffset:0.0,
        yLineOffset:0.0,
        xLayerOffset:0.0,
        yLayerOffset:0.5,
        doubleOffset:true,
        xLayerOffset2:0.5,
        yLayerOffset2:0.5,
        zLayerRotation:Math.PI/2.0,
        voxelSpacing:voxelSpacing,
        voxelScaleX:0.5,
        voxelScaleY:0.9,
        voxelScaleZ:0.2,
        neighbors:[ // layer 0, 4,8
            {x: 0,y:-1,z:-1},
            {x:-1,y:-1,z:-1},
            {x: 0,y:-1,z: 1},
            {x: 0,y: 0,z: 1}
        ],
        neighborsLayerOffset:[ // layer 1, 5, 9
            {x: 0,y: 1,z:-1},
            {x: 0,y: 0,z:-1},
            {x:-1,y: 0,z: 1},
            {x: 0,y: 0,z: 1} 
        ],
        neighborsLayerOffset1:[ // layer 2, 6, 19
            {x: 1,y: 0,z:-1},
            {x: 0,y: 0,z:-1},
            {x: 0,y:-1,z: 1},
            {x: 0,y: 0,z: 1} 
        ],
        neighborsLayerOffset2:[ // layer 3, 7, 11
            {x: 0,y: 1,z:-1},
            {x: 0,y: 0,z:-1},
            {x: 0,y: 1,z: 1},
            {x: 1,y: 1,z: 1} 
        ]
		
    },
    Dice3:  {
        xScale:1.0,
        yScale:0.5,
        zScale:0.2,
        xLineOffset:0.5,
        yLineOffset:0.0,
        xLayerOffset:0.0,
        yLayerOffset:1.0,
        doubleOffset:false,
        xLayerOffset2:0.5,
        yLayerOffset2:0.5,
        zLayerRotation:Math.PI/2.0,
        voxelSpacing:this.voxelSpacing,
        gridSize:this.gridSize,
        voxelScaleX:0.5,
        voxelScaleY:0.9,
        voxelScaleZ:0.2,
        neighbors:[ // layer 0, 4,8
            {x: 0,y:-1,z:-1},
            {x:-1,y:-1,z:-1},
            {x: 0,y:-1,z: 1},
            {x: 0,y: 0,z: 1}
        ],
        neighborsLayerOffset:[ // layer 1, 5, 9
            {x: 0,y: 1,z:-1},
            {x: 0,y: 0,z:-1},
            {x:-1,y: 0,z: 1},
            {x: 0,y: 0,z: 1} 
        ],
        neighborsLayerOffset1:[ // layer 2, 6, 19
            {x: 1,y: 0,z:-1},
            {x: 0,y: 0,z:-1},
            {x: 0,y:-1,z: 1},
            {x: 0,y: 0,z: 1} 
        ],
        neighborsLayerOffset2:[ // layer 3, 7, 11
            {x: 0,y: 1,z:-1},
            {x: 0,y: 0,z:-1},
            {x: 0,y: 1,z: 1},
            {x: 1,y: 1,z: 1} 
        ]
        
    }
    
    
}

var grid=gridPresets.Dice3;
var gridSize=20.0;

init();

// buildGrid(5);

function getWidth(){
    // return container.style.width;
    return window.innerWidth;
}

function getHeight(){
    // return container.style.height;
    return window.innerHeight ;
}

function init() {
    camera = new THREE.PerspectiveCamera( 45, getWidth()/ getHeight()  , 1, 10000 );
    camera.position.set( 500, 800, 1300 );
    camera.lookAt( gridSize/2, 0, gridSize/2 );
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xffffff );

    cubeGeo = new THREE.BoxBufferGeometry( voxelSpacing, voxelSpacing, voxelSpacing );
    sphereGeo = new THREE.SphereGeometry( voxelSpacing/2, 32, 32 );
    cubeMaterial = new THREE.MeshPhongMaterial( { color: 0x1c5c61 } );

    // grid
    var gridHelper = new THREE.GridHelper( (gridSize)*voxelSpacing, gridSize );
    gridHelper.position.x=gridSize/2.0*voxelSpacing-voxelSpacing/2.0;
    gridHelper.position.z=gridSize/2.0*voxelSpacing-voxelSpacing/2.0;
    gridHelper.position.y=-voxelSpacing/2.0*grid.voxelScaleZ;

    gridHelper.scale.x=grid.xScale;
    gridHelper.scale.z=grid.yScale;
    gridHelper.scale.y=grid.zScale;
    scene.add( gridHelper );

   

    // roll-over helpers
    var rollOverGeo = new THREE.BoxBufferGeometry( voxelSpacing, voxelSpacing, voxelSpacing  );
    rollOverMaterial = new THREE.MeshBasicMaterial( { color: color4, opacity: 0.8, transparent: true } );
    rollOverMesh = new THREE.Mesh( rollOverGeo, rollOverMaterial );
    rollOverMesh.scale.x=grid.voxelScaleX;
    rollOverMesh.scale.z=grid.voxelScaleY;
    rollOverMesh.scale.y=grid.voxelScaleZ;
    scene.add( rollOverMesh );

    raycaster = new THREE.Raycaster();
    mouse = new THREE.Vector2();
    // var geometry = new THREE.PlaneBufferGeometry( 1000, 1000 );
    // geometry.rotateX( - Math.PI / 2 );
    // plane = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { visible: false } ) );
    // scene.add( plane );
    // objects.push( plane );
    
    helperPosition = new Float32Array( 1 * 3 );
    var c = new THREE.Color( color1 );
    helperColor = new Float32Array( 1 * 3 );
    helperColor[0]=c.r;
    helperColor[1]=c.g;
    helperColor[2]=c.b;
    helperMaterial = new THREE.PointsMaterial( { size: 5, vertexColors: THREE.VertexColors,color:color3 } );

    buildGrid(gridSize);
    createHelperMeshes(gridSize);
    

    // var axesHelper = new THREE.AxesHelper( 20 *grid.voxelSpacing);
    // scene.add( axesHelper );

    // lights
    var ambientLight = new THREE.AmbientLight( 0x606060 );
    scene.add( ambientLight );
    var directionalLight = new THREE.DirectionalLight( 0xffffff );
    directionalLight.position.set( 1, 0.75, 0.5 ).normalize();
    scene.add( directionalLight );
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( getWidth(), getHeight() );

    renderer = new THREE.WebGLRenderer();
    
    container.appendChild( renderer.domElement );
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    controls.target=new THREE.Vector3( gridSize/2*voxelSpacing,0 ,gridSize/2*voxelSpacing );
    controls.update();
    
    // document.body.appendChild( renderer.domElement );
    onWindowResize();
    container.addEventListener( 'mousemove', onDocumentMouseMove, false );
    container.addEventListener( 'mousedown', onDocumentMouseDown, false );
    // window.addEventListener( 'keydown', onDocumentKeyDown, false );
    // window.addEventListener( 'keyup', onDocumentKeyUp, false );
    //
    window.addEventListener( 'mouseup', onWindowResize, false );

    window.addEventListener( 'resize', onWindowResize, false );


    animate();
}


////////////////////////

function onWindowResize( event ) {

	camera.aspect = getWidth() / getHeight();
    camera.updateProjectionMatrix();

	renderer.setSize( getWidth(), getHeight() );
}

function animate() {

    requestAnimationFrame( animate );

    render();
}

function render() {
    renderer.render( scene, camera );
    controls.update();
}

////////////////////////////
function buildGrid(gridSize){
    occupancy=[];
    occupancyHelper=[];
    for (var i=0;i<gridSize;++i){
        occupancy.push([]);
        occupancyHelper.push([]);
        for (var j=0;j<gridSize;++j){
            occupancy[i].push([]);
            occupancyHelper[i].push([]);
            for (var k=0;k<gridSize;++k){
                occupancy[i][j].push(false);
                occupancyHelper[i][j].push(false);
            }
        }
    }

    // buildVoxelAt(grid, 0, 0,0);
    // buildVoxelAt(grid, 1, 0,0);

    // buildVoxelAt(grid, 0, 1,0);
    // buildVoxelAt(grid, 1, 1,0);

    // buildVoxelAt(grid, 5, 5,4); //voxel
    // buildVoxelAt(grid, 5, 4,3); //neighbour
    // buildVoxelAt(grid, 4, 4,3); //neighbour
    // buildVoxelAt(grid, 5, 4,5); //neighbour
    // buildVoxelAt(grid, 5, 5,5); //neighbour

    //rule 1 if (5,5) z==4, neighbors: (0,-1,-1) (-1,-1,-1) (0,-1, 1) (0,0, 1) // layer 0, 4,8

    // buildVoxelAt(grid, 15, 3, 1); //voxel
    // buildVoxelAt(grid, 15, 4, 0); //neighbour
    // buildVoxelAt(grid, 15, 3, 0); //neighbour
    // buildVoxelAt(grid, 14, 3, 2); //neighbour
    // buildVoxelAt(grid, 15, 3, 2); //neighbour

    //rule 2 if (10,5) z==4, neighbors: (0,1,-1) (0,0,-1) (-1, 0, 1) (0,0,1) // layer 1, 5, 9


    // buildVoxelAt(grid, 10, 10,6); //voxel
    // buildVoxelAt(grid, 11, 10,5); //neighbour
    // buildVoxelAt(grid, 10, 10,5); //neighbour
    // buildVoxelAt(grid, 10,  9,7); //neighbour
    // buildVoxelAt(grid, 10, 10,7); //neighbour

    //rule 2 if (10,5) z==4, neighbors: (1,0,-1) (0,0,-1) (0,-1, 1) (0,0,1) // layer 2, 6, 19

    // buildVoxelAt(grid, 10, 3,11); //voxel

    // buildVoxelAt(grid, 10, 4,10); //neighbour
    // buildVoxelAt(grid, 10, 3,10); //neighbour
    // buildVoxelAt(grid, 10, 4,12); //neighbour
    // buildVoxelAt(grid, 11, 4,12); //neighbour

    //rule 2 if (10,5) z==4, neighbors: (0,1,-1) (0,0,-1) (0, 1, 1) (1,1,1) // layer 3, 7, 11


    
    
    // buildVoxelAt( 10, 0,0);
    // for (var i=0;i<gridSize;++i){
    //     for (var j=0;j<gridSize;++j){
    //         for (var k=0;k<3;++k){
    //             // if(k<5){
    //                 buildVoxelAt(grid, i, j,k);
    //                 // console.log(i+" "+ j+" "+k)
    //                 occupancy[i][j][k]=true;
    //             // }
    //         }
    //     }
    // }
}

function buildVoxelAt( grid,x, y,z){
    occupancy[x][y][z]=true;
    var voxel = new THREE.Mesh( cubeGeo, cubeMaterial );
    // var voxel = new THREE.Mesh( sphereGeo, cubeMaterial );

    var p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y;
    [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y]=getTransforms(grid,x, y,z);
    // console.log(getTransforms(grid,x, y,z))

    voxel.scale.x=s_x;
    voxel.scale.z=s_z;
    voxel.scale.y=s_y;

    voxel.position.x=0;
    voxel.position.y=0;
    voxel.position.z=0;

    voxel.rotation.y=r_y;

    
    voxel.position.x=p_x;
    voxel.position.y=p_y;
    voxel.position.z=p_z;
    
    // voxel.scale.x=grid.voxelScaleX;
    // voxel.scale.z=grid.voxelScaleY;
    // voxel.scale.y=grid.voxelScaleZ;

    // console.log([voxel.position.x ,voxel.position.y ,voxel.position.z ,voxel.scale.x,voxel.scale.y,voxel.scale.z,voxel.rotation.y])
    console.log([x,y,z])

    // voxel.position.x=0.0;
    // voxel.position.y=0.0;
    // voxel.position.z=0.0;
    // if(z%2!=0){
    //     voxel.rotation.y=grid.zLayerRotation;
    //     voxel.position.x+=grid.voxelSpacing*grid.xScale*(grid.xLayerOffset);
    //     voxel.position.z+=grid.voxelSpacing*grid.yScale*(grid.yLayerOffset);

    // }

    // voxel.position.x+=grid.voxelSpacing*grid.xScale*(x );
    // voxel.position.z+=grid.voxelSpacing*grid.yScale*(y );
    // voxel.position.y+=grid.voxelSpacing*grid.zScale*(z );

    // if(y%2!=0){
    //     voxel.position.x+=grid.voxelSpacing*grid.xScale*(grid.xLineOffset);
    //     voxel.position.z+=grid.voxelSpacing*grid.yScale*(grid.yLineOffset);

    // }

    // console.log([voxel.position.x ,voxel.position.y ,voxel.position.z ,voxel.scale.x,voxel.scale.y,voxel.scale.z,voxel.rotation.y])
    

    scene.add(voxel);
}

function getTransforms(grid,x, y,z){
    var s_x=grid.voxelScaleX;
    var s_z=grid.voxelScaleY;
    var s_y=grid.voxelScaleZ;

    var p_x=0.0;
    var p_y=0.0;
    var p_z=0.0;
    var r_y=0.0;
    if(z%2!=0){
        r_y=grid.zLayerRotation;
        p_x+=grid.voxelSpacing*grid.xScale*(grid.xLayerOffset);
        p_z+=grid.voxelSpacing*grid.yScale*(grid.yLayerOffset);

    }
    if(grid.doubleOffset){
        if(parseInt(z/2)%2!=0){
            p_x+=grid.voxelSpacing*grid.xScale*(grid.xLayerOffset2);
            p_z+=grid.voxelSpacing*grid.yScale*(grid.yLayerOffset2);
        }
    }

    p_x+=grid.voxelSpacing*grid.xScale*(x );
    p_z+=grid.voxelSpacing*grid.yScale*(y );
    p_y+=grid.voxelSpacing*grid.zScale*(z );

    if(y%2!=0){
        p_x+=grid.voxelSpacing*grid.xScale*(grid.xLineOffset);
        p_z+=grid.voxelSpacing*grid.yScale*(grid.yLineOffset);

    }
    return [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y];
}

function createHelperMeshes(gridSize){
    for (var i=0;i<gridSize;++i){
        for (var j=0;j<gridSize;++j){
            buildHelperSnap(grid,i,j,0);
        }
    }

}

function buildHelperSnap(grid,x,y,z){
    occupancyHelper[x][y][z]=true;
    var geometry = new THREE.PlaneBufferGeometry( grid.voxelSpacing*grid.xScale, grid.voxelSpacing*grid.yScale );
    geometry.rotateX( - Math.PI / 2 );
    plane = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { visible: false } ) );
    // plane = new THREE.Mesh( geometry,helperMaterial );
    

    [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y]=getTransforms(grid,x, y,z);
        // console.log(getTransforms(grid,x, y,z))

    plane.scale.x=s_x;
    plane.scale.z=s_z;
    // rollOverMesh.scale.y=s_y;

    plane.position.x=0;
    plane.position.y=0;
    plane.position.z=0;

    plane.rotation.y=r_y;

    
    plane.position.x=p_x;
    plane.position.y=p_y;
    plane.position.z=p_z;

    // plane.scale.x=grid.xScale*0.95;
    // plane.scale.z=grid.yScale*0.95;

    // plane.position.x=0.0;
    // plane.position.y=0.0;
    // plane.position.z=0.0;

    // plane.position.x+=grid.voxelSpacing*grid.xScale*(x );
    // plane.position.z+=grid.voxelSpacing*grid.yScale*(y );
    // plane.position.y+=grid.voxelSpacing*grid.zScale*(z );

    // if(y%2!=0){
    //     plane.position.x+=grid.voxelSpacing*grid.xScale*(grid.xLineOffset);
    //     plane.position.z+=grid.voxelSpacing*grid.yScale*(grid.yLineOffset);

    // }
    // plane.position.y-=grid.voxelSpacing*grid.zScale/2.0;
    plane.name="p["+parseInt(x) +","+parseInt(y) +","+parseInt(z) +"]";
    

    

    

    helperPosition[0]=plane.position.x;
    helperPosition[1]=plane.position.y;
    helperPosition[2]=plane.position.z;
    var helperGeometry = new THREE.BufferGeometry();
    helperGeometry.setAttribute( 'position', new THREE.BufferAttribute( helperPosition.slice(), 3 ) );
    helperGeometry.setAttribute( 'color', new THREE.BufferAttribute( helperColor.slice(), 3 ) );
    helper = new THREE.Points( helperGeometry, helperMaterial );
    helper.name="s["+parseInt(x) +","+parseInt(y) +","+parseInt(z) +"]";

    scene.add( helper );
    scene.add( plane );
    objects.push( plane );
}

function buildNeighbourHelpers(grid, x, y,z){
    
    for(var i=0;i<grid.neighbors.length;i++){
        //if 0,4,8
        var x1=parseInt(x)+grid.neighbors[i].x;
        var y1=parseInt(y)+grid.neighbors[i].y;
        var z1=parseInt(z)+grid.neighbors[i].z;
        if(z%2!=0){
            x1+=grid.neighborsLayerOffset[i].x
            y1+=grid.neighborsLayerOffset[i].y
            z1+=grid.neighborsLayerOffset[i].z
        }
        if(grid.doubleOffset){
            if(parseInt(z/2)%2!=0){ //2,3 , 6,7
                if(z%2!=0){//3,7,11
                    console.log("neighbors 3 7")
                    x1=parseInt(x)+grid.neighborsLayerOffset2[i].x;
                    y1=parseInt(y)+grid.neighborsLayerOffset2[i].y;
                    z1=parseInt(z)+grid.neighborsLayerOffset2[i].z;
                }else{//2,6
                    console.log("neighbors 2 6")
                    x1=parseInt(x)+grid.neighborsLayerOffset1[i].x;
                    y1=parseInt(y)+grid.neighborsLayerOffset1[i].y;
                    z1=parseInt(z)+grid.neighborsLayerOffset1[i].z;

                }
            }else{ //0,1 ,4,5
                if(z%2!=0){//1,5
                    console.log("neighbors 1 5")
                    x1=parseInt(x)+grid.neighborsLayerOffset[i].x;
                    y1=parseInt(y)+grid.neighborsLayerOffset[i].y;
                    z1=parseInt(z)+grid.neighborsLayerOffset[i].z;
                }else{ //0,4
                    console.log("neighbors 0 4")
                }

            }
        }
        if(helperAt(x1,y1,z1)){
            buildHelperSnap(grid,x1,y1,z1);
        }

    }

    //go through all neighbours
        //if occupancy empty
            //add helper meshes??
            //how to rotate plane

}

function helperAt(x,y,z){
    if(x<0||y<0||z<0){
        return false;
    }
    if(x>=gridSize||y>=gridSize||z>=gridSize){
        return false;
    }
    if(occupancy[x][y][z]||occupancyHelper[x][y][z]){
        return false;
    }
    
    return true;

}

/////////////////////

function onDocumentMouseMove( event ) {
    event.preventDefault();
    mouse.set( ( event.clientX / getWidth() ) * 2 - 1, - ( event.clientY /getHeight() ) * 2 + 1 );
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( objects );
    if ( intersects.length > 0 ) {
        var intersect = intersects[ 0 ];
        var obj=getXYZfromName(intersect.object.name);

        [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y]=getTransforms(grid,obj.x, obj.y,obj.z);
        // console.log(getTransforms(grid,x, y,z))

        // rollOverMesh.scale.x=s_x;
        // rollOverMesh.scale.z=s_z;
        // rollOverMesh.scale.y=s_y;
        rollOverMesh.name="r["+parseInt(obj.x) +","+parseInt(obj.y) +","+parseInt(obj.z) +"]"
        rollOverMesh.position.x=0;
        rollOverMesh.position.y=0;
        rollOverMesh.position.z=0;

        rollOverMesh.rotation.y=r_y;

        
        rollOverMesh.position.x=p_x;
        rollOverMesh.position.y=p_y;
        rollOverMesh.position.z=p_z;

        
        // rollOverMesh.position.copy( intersect.object.position ).add(temp );
        
    }
    render();
}

function onDocumentMouseDown( event ) {

    event.preventDefault();
    mouse.set( ( event.clientX / getWidth() ) * 2 - 1, - ( event.clientY / getHeight() ) * 2 + 1 );
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( objects );
    if ( intersects.length > 0 ) {
        var intersect = intersects[ 0 ];
        var obj=getXYZfromName(intersect.object.name);
        
        switch (event.which) {
            case 1:
                // console.log('Left Mouse button pressed.'); //add voxel
                obj=getXYZfromName(rollOverMesh.name);
                buildVoxelAt( grid, obj.x, obj.y,obj.z);

                //deleteHelper Meshes
                var name='[' +obj.x +"," +obj.y+","+obj.z+']';
                var object = scene.getObjectByName( 'p'+name );
                scene.remove( object );
                objects.splice( objects.indexOf( object ), 1 );
                var object = scene.getObjectByName( 's'+name );
                scene.remove( object );

                buildNeighbourHelpers(grid, obj.x, obj.y,obj.z);

                break;
            case 2:
                // console.log('Middle Mouse button pressed.');
                break;
            case 3:
                // console.log('Right Mouse button pressed.');//remove voxel
                break;
            default:
                // console.log('You have a strange Mouse!');
        }
        
        render();
    }

    
}

/////////////////////

function getXYZfromName(name){
    var nums=name.match(/\d+/g);
    var x=nums[0];
    var y=nums[1];
    var z=nums[2];
    return new THREE.Vector3(x,y,z);
}

//
function changeToGrid(newGrid){

}


function threejs(){
    this.camera=[];
    this.scene=[];
    this.renderer=[];
    this.plane=[];
    this.mouse=[];
    this.raycaster=[];
    this.isShiftDown = false;
    this.rollOverMesh=[];
    this.rollOverMaterial;
    this.cubeGeo=[];
    this.cubeMaterial=[];
    this.objects = [];
    this.container = document.getElementById( 'webgl' );
    this.container1 = document.getElementById( 'threejs' );
    this.controls=[];
}
    
// init();
// render();


threejs.prototype.animate=function() {

    requestAnimationFrame(this.animate.bind(this));

    this.render();
}



threejs.prototype.render=function() {
    this.renderer.render( this.scene, this.camera );
    this.controls.update();
}

threejs.prototype.getWidth=function(){
    // return container.style.width;
    return $('#threejs').width() ;
}

threejs.prototype.getHeight=function(){
    // return container.style.height;
    return $('#threejs').height() ;
}



threejs.prototype.init=function() {
    this.camera = new THREE.PerspectiveCamera( 45, this.getWidth()  / this.getHeight() , 1, 10000 );
    this.camera.position.set( 500, 800, 1300 );
    this.camera.lookAt( 0, 0, 0 );
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color( 0xffffff );
    // roll-over helpers
    var rollOverGeo = new THREE.BoxBufferGeometry( 50, 50, 50 );
    this.rollOverMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, opacity: 0.5, transparent: true } );
    this.rollOverMesh = new THREE.Mesh( rollOverGeo, this.rollOverMaterial );
    this.scene.add( this.rollOverMesh );
    // cubes
    this.cubeGeo = new THREE.BoxBufferGeometry( 50, 50, 50 );
    this.cubeMaterial = new THREE.MeshPhongMaterial( { color: 0x1c5c61 } );
    // grid
    var gridHelper = new THREE.GridHelper( 1000, 20 );
    this.scene.add( gridHelper );
    //
    this.raycaster = new THREE.Raycaster();
    this.mouse = new THREE.Vector2();
    var geometry = new THREE.PlaneBufferGeometry( 1000, 1000 );
    geometry.rotateX( - Math.PI / 2 );
    this.plane = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { visible: false } ) );
    this.scene.add( this.plane );
    this.objects.push( this.plane );
    // lights
    var ambientLight = new THREE.AmbientLight( 0x606060 );
    this.scene.add( ambientLight );
    var directionalLight = new THREE.DirectionalLight( 0xffffff );
    directionalLight.position.set( 1, 0.75, 0.5 ).normalize();
    this.scene.add( directionalLight );
    this.renderer = new THREE.WebGLRenderer( { antialias: true } );
    this.renderer.setPixelRatio( window.devicePixelRatio );
    // renderer.setSize( window.innerWidth, window.innerHeight );
    this.renderer.setSize(this.getWidth(),this.getHeight());

    this.renderer = new THREE.WebGLRenderer();
    
    this.container.appendChild( this.renderer.domElement );
    this.controls = new THREE.OrbitControls( this.camera, this.renderer.domElement );
    this.controls.update();
    // document.body.appendChild( renderer.domElement );
    onWindowResizeThree();
    this.container.addEventListener( 'mousemove', onDocumentMouseMoveThree, false );
    this.container.addEventListener( 'mousedown', onDocumentMouseDownThree, false );
    window.addEventListener( 'keydown', onDocumentKeyDownThree, false );
    window.addEventListener( 'keyup', onDocumentKeyUpThree, false );
    //
    window.addEventListener( 'mouseup', onWindowResizeThree, false );
    window.addEventListener( 'resize', onWindowResizeThree, false );
    this.animate();
    // requestAnimationFrame( this.animate );


    
}


var three=new threejs();
three.init();

function onDocumentMouseMoveThree( event ) {
    // console.log(three.mouse)
    // three.mouse = new THREE.Vector2();
    event.preventDefault();
    three.mouse.set( ( event.clientX / three.getWidth() ) * 2 - 1, - ( event.clientY / three.getHeight() ) * 2 + 1 );
    three.raycaster.setFromCamera( three.mouse, three.camera );
    var intersects = three.raycaster.intersectObjects( three.objects );
    if ( intersects.length > 0 ) {
        var intersect = intersects[ 0 ];
        three.rollOverMesh.position.copy( intersect.point ).add( intersect.face.normal );
        three.rollOverMesh.position.divideScalar( voxelSpacing ).floor().multiplyScalar( voxelSpacing ).addScalar( voxelSpacing/2 );
    }
    three.render();
}

function onDocumentMouseDownThree( event ) {
    event.preventDefault();
    three.mouse = new THREE.Vector2();
    three.mouse.set( ( event.clientX / three.getWidth() ) * 2 - 1, - ( event.clientY / three.getHeight() ) * 2 + 1 );
    three.raycaster.setFromCamera( three.mouse, three.camera );
    var intersects = three.raycaster.intersectObjects( three.objects );
    if ( intersects.length > 0 ) {
        var intersect = intersects[ 0 ];
        // delete cube
        if ( three.isShiftDown ) {
            // console.log("shiftt iss dowwn");
            if ( intersect.object !== three.plane ) {
                // scene.remove( intersect.object );
                // console.log(intersect.object.name);
                var pos=utils.getXYZfromName(intersect.object.name);
                GLOBALS.removeNode(pos.x,pos.y,pos.z);
                // objects.splice( objects.indexOf( intersect.object ), 1 );
            }
            // create cube
        } else {
            var voxel = new THREE.Mesh( three.cubeGeo, three.cubeMaterial );
            

            voxel.position.copy( intersect.point ).add( intersect.face.normal );

            var x=Math.floor(voxel.position.x/voxelSpacing)+GLOBALS.gridSize/2;
            var y=Math.floor(voxel.position.y/voxelSpacing);
            var z=Math.floor(voxel.position.z/voxelSpacing)+GLOBALS.gridSize/2;
            if(y<0){
                y=0;
            }

            GLOBALS.addNode (x,z ,y); //y is up
            // voxel.name='[' +x +"," +z+","+y+']';

            // voxel.position.divideScalar( voxelSpacing ).floor().multiplyScalar( voxelSpacing ).addScalar( voxelSpacing/2 );
            // scene.add( voxel );
            // objects.push( voxel );
            
            
        }
        three.render();
    }
}

document.addEventListener('addNode', function (e) { 
    var voxel = new THREE.Mesh( three.cubeGeo, three.cubeMaterial );
            
    // voxel.position.copy( intersect.point ).add( intersect.face.normal );
    // var x=Math.floor(voxel.position.x/voxelSpacing)+GLOBALS.gridSize/2;
    // var y=Math.floor(voxel.position.y/voxelSpacing);
    // var z=Math.floor(voxel.position.z/voxelSpacing)+GLOBALS.gridSize/2;
    voxel.position.x=e.detail.posX;
    voxel.position.y=e.detail.posZ;
    voxel.position.z=e.detail.posY;

    voxel.name='[' +e.detail.x +"," +e.detail.y+","+e.detail.z+']';
    // voxel.position.divideScalar( voxelSpacing ).floor().multiplyScalar( voxelSpacing ).addScalar( voxelSpacing/2 );
    three.scene.add( voxel );
    three.objects.push( voxel );
    
}, false);

document.addEventListener('removeNode', function (e) { 
    var name='[' +e.detail.x +"," +e.detail.y+","+e.detail.z+']';
    var object = scene.getObjectByName( name );
    three.scene.remove( object );
    three.objects.splice( objects.indexOf( object ), 1 );
    
}, false);

function onDocumentKeyDownThree( event ) {
    switch ( event.keyCode ) {
        case 16: three.isShiftDown = true; break;
    }
}

function onDocumentKeyUpThree( event ) {
    switch ( event.keyCode ) {
        case 16: three.isShiftDown = false; break;
    }
}

function onWindowResizeThree(){
    three.camera.aspect = three.getWidth() / three.getHeight();
    three.camera.updateProjectionMatrix();
    three.renderer.setSize( three.getWidth() , three.getHeight() );
}

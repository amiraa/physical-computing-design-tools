function initEditor(){

    // todo ?? move template creation to separate files
    var templates = [
        {
            text:"DEM simulation node",
            title: 'Insert prediction viewer',
            className: 'jsoneditor-type-object',
            field: 'voxel',
            value: {
                name:"prediction",
                size:5,
                E:20000,
                area:10.0
            }
        },
        {
            text: 'MNIST',
            title: 'Insert a MNIST dataset',
            className: 'jsoneditor-type-object',
            field: 'cnn',
            value: {
                name:"MNIST",
                imageSize:"(28,28)",
                numDatasets:65000,
                numTraining:55000,
                numTest:65000-55000,
                parent: 'CNN',
                classes: 'input',
                outputNeigh:-1 //0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,
            }
        },
        {
            text:"conv2d",
            title: 'Insert a 3d convolutionsl layer',
            className: 'jsoneditor-type-object',
            field: 'cnn',
            value: {
            name:"conv2d_Conv2D1",
                inputShape:"(batch,28,28,1)",
                kernelSize:5,
                filters:8,
                strides:1,
                activation: 'relu',
                kernelInitializer: 'varianceScaling',
                outputShape:"(batch,24,24,8)",
                numParams:208,
                Trainable:true,
                parent: 'CNN',
                classes: 'layers',
                outputNeigh:-1 //0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,
            }
        },
        {
            text:"testImage",
            title: 'Insert a test Image',
            className: 'jsoneditor-type-object',
            field: 'cnn',
            value: {
                name:"testImage",
                imageSize:"(1,28,28)",
                parent: 'CNN',
                classes: 'viz',
                outputNeigh:-1 //0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,
            }
        },
        {
            text:"max_pooling2d",
            title: 'Insert a 2d max pooling layer',
            className: 'jsoneditor-type-object',
            field: 'cnn',
            value: {
                name:"max_pooling2d_MaxPooling2D1",
                inputShape:"(batch,24,24,8)",
                poolSize:"[2,2]",
                strides:"[2,2]",
                outputShape:"(batch,12,12,8)",
                numParams:0,
                Trainable:true,
                parent: 'CNN',
                classes: 'layers',
                outputNeigh:-1 //0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,
            }
        
        },
        {
            text:"flatten",
            title: 'flattern layer',
            className: 'jsoneditor-type-object',
            field: 'cnn',
            value: {
                name:"flatten_Flatten1",
                inputShape:"(batch,4,4,16)",
                outputShape:"(batch,256)",
                numParams:0,
                Trainable:true,
                parent: 'CNN',
                classes: 'layers',
                outputNeigh:-1 //0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,
            }
        
        },
        {
            text:"dense",
            title: 'Insert a fully connected layer',
            className: 'jsoneditor-type-object',
            field: 'cnn',
            value: {
                name:"dense_Dense1",
                inputShape:"(batch,256)",
                kernelInitializer: 'varianceScaling',
                activation: 'softmax',
                outputShape:"(batch,10)",
                numParams:2570,
                Trainable:true,
                parent: 'CNN',
                classes: 'layers',
                outputNeigh:-1 //0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,
            }
        
        },
        {
            text:"loss_categoricalCrossentropy",
            title: 'Insert a categoricalCrossentropy loss layer',
            className: 'jsoneditor-type-object',
            field: 'cnn',
            value: {
                name:"loss_categoricalCrossentropy",
                metrics: ['accuracy'],
                parent: 'CNN',
                classes: 'loss',
                outputNeigh:-1 //0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,
            }
        
        },
        {
            text:"prediction",
            title: 'Insert prediction viewer',
            className: 'jsoneditor-type-object',
            field: 'cnn',
            value: {
                name:"prediction",
                parent: 'CNN',
                classes: 'output',
                outputNeigh:-1 //0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,
            }
        }

    ];

    var schema = {
        "title": "Example Schema",
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "firstName": {
              "type": "string"
            },
            "lastName": {
              "type": "string"
            },
            "gender": {
              "enum": ["male", "female"]
            },
            "age": {
              "description": "Age in years",
              "type": "integer",
              "minimum": 0
            },
            "job": {
              "$ref": "job"
            }
          },
          "required": ["firstName", "lastName"]
        }
    };
  
    var job = {
        "title": "Job description",
        "type": "object",
        "properties": {
            "company": {
            "type": "string"
            },
            "role": {
            "type": "string"
            }
        }
    };


    // create the editor
    var container = document.getElementById('jsoneditor');
    var options = {
        // schema: schema,
        // schemaRefs: {"job": job},
        templates: templates,
        onChangeText: function (jsonString) {
            // console.log(jsonString);
            // const json = editor.get();
            var json=JSON.parse(jsonString);
            if(json.ur10Setup === undefined){
                GLOBALS.updateNode(json);
            }else{
                integratedSetup=json; //todo remove from here
            }
            
        }
    };
    GLOBALS.editor = new JSONEditor(container, options);

    document.addEventListener('selectNode', function (e) { 
        GLOBALS.editor.set(GLOBALS.selectedjson);
    }, false);

    document.addEventListener('selectEdge', function (e) { 
        GLOBALS.editor.set(GLOBALS.selectedjson);
    }, false);

    // get json //set info
    // document.getElementById('getJSON').onclick = function () {
    //     var json = GLOBALS.editor.get();
    //     // alert(JSON.stringify(json, null, 2));
    //     GLOBALS.updateNode(json);
    // };

    // GLOBALS.editor.on('onChangeJson',function() {
    //     console.log("hiii");
    // });

    // GLOBALS.editor.onChangeJson= function(json){
    //     // console.log(json);
    //     console.log("hiii");
    // };

    // GLOBALS.document.addEventListener('onChangeJson', function (e) { 
    //     console.log("hissssii");
    // }, false);

}


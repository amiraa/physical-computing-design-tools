var solveFea;
let _rhino3dm = null;
var occupancy=[];
var gridSize=10;


// initialize 
function latticeSetup(setup,latticeSize,voxelSize,voxelBuilder,forces) {//later change type to dictiary
    
    // var voxelSize=10.0;
	// var latticeSize=5;
	const material={
		area:1.0,
		density:0.028,
		stiffness:1000000
	};

    createLattice(setup,voxelSize,latticeSize,latticeSize,latticeSize,voxelBuilder,material);

	
	//todo fix this
	if(setup.hierarchical){
		addForceAbove(setup,0.75*voxelSize*latticeSize,forces);
		restrainBelow(setup,voxelSize*0.75);
	}else{
		addForceAbove(setup,0.8*voxelSize*latticeSize,forces);
		restrainBelow(setup,-voxelSize/2);
	}
}

///
function lateralLoadSetup(setup,position,length,voxelSize,voxelBuilder,supports,loads,material) {

	createLattice(setup,voxelSize,1,1,length,voxelBuilder,material,position);
	
	//todo incorporate this into the drawing phase so you don't have to loop twice
	restrainFromBox(setup,supports);
	loadFromBox(setup,loads);
}

function lateralLoadSetup1(setup,position,length,voxelSize,voxelBuilder,supports,loads,material) {

	createLattice(setup,voxelSize,1,length,1,voxelBuilder,material,position);
	
	//todo incorporate this into the drawing phase so you don't have to loop twice
	restrainFromBox(setup,supports);
	loadFromBox(setup,loads);
}

////////////////////////////////////////////////////////////
function createLattice(setup,size,sizeX,sizeY,sizeZ,voxelBuilder,material,position=new THREE.Vector3(0,0,0)){

	for (var i=0;i<gridSize;++i){
        occupancy.push([]);
        for (var j=0;j<gridSize;++j){
            occupancy[i].push([]);
            for (var k=0;k<gridSize;++k){
                occupancy[i][j].push(-1);
            }
        }
	}
	
	var count=0;	

	var area=material.area;
	var density=material.density;
	var stiffness=material.stiffness;

    for(var i=0;i<sizeX;i++){
        for(var j=0;j<sizeY;j++){
            for(var k=0;k<sizeZ;k++){
				if(i>sizeX/3){
					stiffness=100000;

				}
				voxelBuilder(setup,size,new THREE.Vector3(i*size+position.x,j*size+position.y,k*size+position.z),area,density,stiffness);
			}
        }
	}
	
    

}

function createVoxel(setup,size,position,area,density,stiffness){
    

    // var sphere = new _rhino3dm.Sphere([0,0,0], size/2);
	// var box = sphere.toBrep().getBoundingBox();
	var box=new _rhino3dm.BoundingBox([0,0,0], [size,size,size]);
    // console.log(box.min)
	box = box.toBrep();
	
	position.x-=size/2;
	position.y-=size/2;
	position.z-=size/2;
    
    for(var i=0; i<6;i++){//for each face

        // var restrained=false;
        // var force=new THREE.Vector3(0,0,0);
        // if(i==0){
        //     restrained=true;
        // }
        // if(i==2){
        //     force=new THREE.Vector3(0,-200,0);
        //     // restrained=true;
        // }
        nodesIndices=[];
        var face=box.faces().get(i);
        
        
		nodesIndices.push(checkandAdd(setup,add(position,toPosition(face.pointAt(size  , size/2)))));
        nodesIndices.push(checkandAdd(setup,add(position,toPosition(face.pointAt(size/2, 0     )))));
        nodesIndices.push(checkandAdd(setup,add(position,toPosition(face.pointAt(0     , size/2)))));
        nodesIndices.push(checkandAdd(setup,add(position,toPosition(face.pointAt(size/2, size  )))));
        
        linkNodes(setup,nodesIndices,area,density,stiffness);
		
	}
}

function createChiralVoxel(setup,size,position,area,density,stiffness){
    

    // var sphere = new _rhino3dm.Sphere([0,0,0], size/2);
    // var box = sphere.toBrep().getBoundingBox();
	// console.log(box.min)
	var box=new _rhino3dm.BoundingBox([0,0,0], [size,size,size]);
	box = box.toBrep();
	
	position.x-=size/2;
	position.y-=size/2;
	position.z-=size/2;
    
    for(var i=0; i<6;i++){//for each face

        var nodesIndices=[];
        var face=box.faces().get(i);
        
        
		nodesIndices.push(checkandAdd(setup,add(position,toPosition(face.pointAt(size  , size/2)))));
        nodesIndices.push(checkandAdd(setup,add(position,toPosition(face.pointAt(size/2, 0     )))));
        nodesIndices.push(checkandAdd(setup,add(position,toPosition(face.pointAt(0     , size/2)))));
        nodesIndices.push(checkandAdd(setup,add(position,toPosition(face.pointAt(size/2, size  )))));
        
        // linkNodes(nodesIndices);

        var n=8;
        var c= new _rhino3dm.Circle(size/3.5);
		c=c.toNurbsCurve();
		var rot=[1,0,0];
        
        var nodesIndices1=[];
		
		c.domain=[0,1];
		// console.log(rot);
		// console.log(c.tangentAt(i*1/4+1/4));
		rot=c.tangentAt(i*1/4+1/4);
		if(i>3){
			rot=face.normalAt(0, 0);
		}
		c.rotate(Math.PI/2,rot , [0,0,0]);
		c.translate(face.pointAt(size/2, size/2));
		for(var ii=0; ii<n;ii++){//for each face
            // createNode(toPosition(c.pointAt(ii*1/n)),'e',false,[]);
            nodesIndices1.push(checkandAdd(setup,add(position,toPosition(c.pointAt(ii*1/n)))));
        }

		linkNodes(setup,nodesIndices1,area,density,stiffness);


        if(i==0){
            add_Edge(setup,nodesIndices[0], nodesIndices1[1] ,area,density,stiffness);
            add_Edge(setup,nodesIndices[1], nodesIndices1[3] ,area,density,stiffness);
            add_Edge(setup,nodesIndices[2], nodesIndices1[5] ,area,density,stiffness);
            add_Edge(setup,nodesIndices[3], nodesIndices1[7] ,area,density,stiffness);

        }else if(i==1){
            add_Edge(setup,nodesIndices[3], nodesIndices1[1],area,density,stiffness);
            add_Edge(setup,nodesIndices[0], nodesIndices1[3],area,density,stiffness);
            add_Edge(setup,nodesIndices[1], nodesIndices1[5],area,density,stiffness);
            add_Edge(setup,nodesIndices[2], nodesIndices1[7],area,density,stiffness);

        }else if(i==2){
            add_Edge(setup,nodesIndices[0], nodesIndices1[5],area,density,stiffness);
            add_Edge(setup,nodesIndices[1], nodesIndices1[7],area,density,stiffness);
            add_Edge(setup,nodesIndices[2], nodesIndices1[1],area,density,stiffness);
            add_Edge(setup,nodesIndices[3], nodesIndices1[3],area,density,stiffness);

            // var force=new THREE.Vector3(0,-400,0);
            // addForce(setup,'n'+nodesIndices[0],force);
            // addForce(setup,'n'+nodesIndices[1],force);
            // addForce(setup,'n'+nodesIndices[2],force);
            // addForce(setup,'n'+nodesIndices[3],force);

        }else if(i==3){

            add_Edge(setup,nodesIndices[3], nodesIndices1[5],area,density,stiffness);
            add_Edge(setup,nodesIndices[0], nodesIndices1[7],area,density,stiffness);
            add_Edge(setup,nodesIndices[1], nodesIndices1[1],area,density,stiffness);
            add_Edge(setup,nodesIndices[2], nodesIndices1[3],area,density,stiffness);
        }else if(i==4){
            add_Edge(setup,nodesIndices[0], nodesIndices1[5],area,density,stiffness);
            add_Edge(setup,nodesIndices[1], nodesIndices1[7],area,density,stiffness);
            add_Edge(setup,nodesIndices[2], nodesIndices1[1],area,density,stiffness);
            add_Edge(setup,nodesIndices[3], nodesIndices1[3],area,density,stiffness);
        }else if(i==5){
            add_Edge(setup,nodesIndices[0], nodesIndices1[5],area,density,stiffness);
            add_Edge(setup,nodesIndices[1], nodesIndices1[3],area,density,stiffness);
            add_Edge(setup,nodesIndices[2], nodesIndices1[1],area,density,stiffness);
            add_Edge(setup,nodesIndices[3], nodesIndices1[7],area,density,stiffness);
        }
	}
}

/////////////////
function createOldVoxelnotRhino(setup,size,position){
	add_Node(setup,position.clone().add(new THREE.Vector3(0,0,0)),true, new THREE.Vector3(0,0,0));

	add_Node(setup,position.clone().add(new THREE.Vector3(-size, size*1.5,-size)),false, new THREE.Vector3(0,-200,0));
	add_Node(setup,position.clone().add(new THREE.Vector3(-size, size*1.5, size)),false, new THREE.Vector3(0,-200,0));
	add_Node(setup,position.clone().add(new THREE.Vector3( size, size*1.5, size)),false, new THREE.Vector3(0,-200,0));
	add_Node(setup,position.clone().add(new THREE.Vector3( size, size*1.5,-size)),false, new THREE.Vector3(0,-200,0));

	add_Node(setup,position.clone().add(new THREE.Vector3(0, size*1.5*2,0)),false, new THREE.Vector3(0,-400,0));

	add_Edge(setup,0, 1, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,0, 2, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,0, 3, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,0, 4, 1.0, 0.284 , 30.0e6);
  
	add_Edge(setup,1, 2, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,2, 3, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,3, 4, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,4, 1, 1.0, 0.284 , 30.0e6);
  
	add_Edge(setup,5, 1, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,5, 2, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,5, 3, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,5, 4, 1.0, 0.284 , 30.0e6);
	
	
	
	// console.log(setup)
}

function createVoxelnotRhino(setup,size,position){
	//down square
	add_Node(setup,position.clone().add(new THREE.Vector3(-size, 0,-size)),true, new THREE.Vector3(0,0,0));//0
	add_Node(setup,position.clone().add(new THREE.Vector3(-size, 0,    0)),true, new THREE.Vector3(0,0,0));//1//mid
	add_Node(setup,position.clone().add(new THREE.Vector3(-size, 0, size)),true, new THREE.Vector3(0,0,0));//2
	add_Node(setup,position.clone().add(new THREE.Vector3(    0, 0, size)),true, new THREE.Vector3(0,0,0));//3//mid
	add_Node(setup,position.clone().add(new THREE.Vector3( size, 0, size)),true, new THREE.Vector3(0,0,0));//4
	add_Node(setup,position.clone().add(new THREE.Vector3( size, 0,    0)),true, new THREE.Vector3(0,0,0));//5//mid
	add_Node(setup,position.clone().add(new THREE.Vector3( size, 0,-size)),true, new THREE.Vector3(0,0,0));//6
	add_Node(setup,position.clone().add(new THREE.Vector3(    0, 0,-size)),true, new THREE.Vector3(0,0,0));//7//mid
	
	add_Edge(setup,0, 1, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,1, 2, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,2, 3, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,3, 4, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,4, 5, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,5, 6, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,6, 7, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,7, 0, 1.0, 0.284 , 30.0e6);

	add_Edge(setup,1, 3, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,3, 5, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,5, 7, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,7, 1, 1.0, 0.284 , 30.0e6);

	//up square
	add_Node(setup,position.clone().add(new THREE.Vector3(-size, size*2,-size)),false, new THREE.Vector3(0,-400,0));//0+8
	add_Node(setup,position.clone().add(new THREE.Vector3(-size, size*2,    0)),false, new THREE.Vector3(0,-400,0));//1+8//mid
	add_Node(setup,position.clone().add(new THREE.Vector3(-size, size*2, size)),false, new THREE.Vector3(0,-400,0));//2+8
	add_Node(setup,position.clone().add(new THREE.Vector3(    0, size*2, size)),false, new THREE.Vector3(0,-400,0));//3+8//mid
	add_Node(setup,position.clone().add(new THREE.Vector3( size, size*2, size)),false, new THREE.Vector3(0,-400,0));//4+8
	add_Node(setup,position.clone().add(new THREE.Vector3( size, size*2,    0)),false, new THREE.Vector3(0,-400,0));//5+8//mid
	add_Node(setup,position.clone().add(new THREE.Vector3( size, size*2,-size)),false, new THREE.Vector3(0,-400,0));//6+8
	add_Node(setup,position.clone().add(new THREE.Vector3(    0, size*2,-size)),false, new THREE.Vector3(0,-400,0));//7+8//mid
	
	add_Edge(setup,0+8, 1+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,1+8, 2+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,2+8, 3+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,3+8, 4+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,4+8, 5+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,5+8, 6+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,6+8, 7+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,7+8, 0+8, 1.0, 0.284 , 30.0e6);

	add_Edge(setup,1+8, 3+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,3+8, 5+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,5+8, 7+8, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,7+8, 1+8, 1.0, 0.284 , 30.0e6);
	
	//left square

	add_Node(setup,position.clone().add(new THREE.Vector3(-size, size,-size)),false, new THREE.Vector3(0,0,0));//0+16
	add_Node(setup,position.clone().add(new THREE.Vector3( size, size,-size)),false, new THREE.Vector3(0,0,0));//1+16
	
	add_Edge(setup,0, 16, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,16, 8, 1.0, 0.284 , 30.0e6);

	add_Edge(setup,6, 17, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,17, 14, 1.0, 0.284 , 30.0e6);

	add_Edge(setup,16, 07, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,15, 16, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,17, 15, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,07, 17, 1.0, 0.284 , 30.0e6);

	//right square
	add_Node(setup,position.clone().add(new THREE.Vector3(-size, size, size)),false, new THREE.Vector3(0,0,0));//0+18
	add_Node(setup,position.clone().add(new THREE.Vector3( size, size, size)),false, new THREE.Vector3(0,0,0));//1+18

	add_Edge(setup,2, 18, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,18, 10, 1.0, 0.284 , 30.0e6);

	add_Edge(setup,4, 19, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,19, 12, 1.0, 0.284 , 30.0e6);

	add_Edge(setup,18, 03, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,11, 18, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,19, 11, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,03, 19, 1.0, 0.284 , 30.0e6);

	//front square
	add_Edge(setup,16, 01, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,09, 16, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,18, 09, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,01, 18, 1.0, 0.284 , 30.0e6);


	//back square
	add_Edge(setup,17, 05, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,13, 17, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,19, 13, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,05, 19, 1.0, 0.284 , 30.0e6);
	
}

function createSimpleSetupnotRhino(setup,size,position){
	add_Node(setup,position.clone().add(new THREE.Vector3(-0, 2*size,-0)),true, new THREE.Vector3(0,0,0));//0
	add_Node(setup,position.clone().add(new THREE.Vector3(-0,      0,-0)),true, new THREE.Vector3(0,0,0));//0
	add_Node(setup,position.clone().add(new THREE.Vector3(2*size, size,)),false, new THREE.Vector3(0,-200,0));//0

	add_Edge(setup,0, 2, 1.0, 0.284 , 30.0e6);
	add_Edge(setup,1, 2, 2.0, 0.284 , 30.0e6);
	
}

/////////////hierarchical nodes///
function createHieraricalVoxel(setup,size,position,area,density,stiffness){
	var i=parseInt(position.x/size);
	var j=parseInt(position.y/size);
	var k=parseInt(position.z/size);
	occupancy[i][j][k]=checkandAdd(setup,position);
	//create node at size +position
	
	var list=[];
	list.push([0,0,1]);
	list.push([0,0,-1]);
	list.push([1,0,0]);
	list.push([-1,0,0]);
	list.push([0,1,0]);
	list.push([0,-1,0]);

	var count=0;

	for(var count=0;count<list.length;count++){
		
		var ii=i+list[count][0];
		var jj=j+list[count][1];
		var kk=k+list[count][2];
		
		if((ii>=0 && ii<gridSize) && (jj>=0 && jj<gridSize) && (kk>=0 && kk<gridSize)){//in bounds
			//check neighbours
			if(occupancy[ii][jj][kk]>-0.5){
				add_Edge(setup,occupancy[i][j][k], occupancy[ii][jj][kk], area,density,stiffness);
			}
		}
	}	
}

///old setups
var setupSimple={
	nodes: [
	{
		id: 'n0', 
		parent: '11',
		degrees_of_freedom:[0,1,2,3,4,5] ,
		restrained_degrees_of_freedom:[true,true,true,true,true,true],
		position: { x: 0, y: 10,z:0 },
		force:{ x: 0, y: 0,z:0 },
		displacement: { x: 0, y: -0,z:0 },
		angle: { x: 0, y: 0,z:0 }
	},
	{
		id: 'n1', 
		parent: '11',
		degrees_of_freedom:[6,7,8,9,10,11]  ,
		restrained_degrees_of_freedom:[true,true,true,true,true,true],
		position: { x: 0, y: 0,z:0  },
		force:{ x: 0, y: 0 },
		displacement: { x: 0, y: 0,z:0 },
		angle: { x: 0, y: 0,z:0 }
	},
	{
		id: 'n2',
		parent: '11',
		degrees_of_freedom:[12,13,14,15,16,17,18]  ,
		restrained_degrees_of_freedom:[false,false,false,false,false,false],
		position: { x: 10, y: 5,z:0  },
		force:{ x: 0, y: -200,z:0 },
		displacement: { x: 0, y: 0,z:0 },
		angle: { x: 0, y: 0,z:0 }
	}
		],
	edges: [
		{ id: 'e0', source: 0, target: 2 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e1', source: 1, target: 2 ,area:2.0,density:0.284,stiffness:30.0e6,stress:0}
	],

	//material properties - AISI 1095 Carbon Steel (Spring Steel)
	ndofs   : 3*6,

	animation :  {
	
		showDisplacement : true,
		exaggeration : 10000,
		speed:3.0
		
	},
	viz :  {
	
		
		minStress:-500,
		maxStress: 500,
		colorMaps:[YlGnBu, winter, coolwarm,jet],
		colorMap:0,
		
	},
	solve: solveFea,
	bar:false
	
};

var oldVoxel={
	nodes: [
	{
	  id: 'n0', 
	  parent: '11',
	  degrees_of_freedom:[0,1,2] , //todo automate
	  restrained_degrees_of_freedom:[true,true,true],
	  position: { x: 0, y: 0,z:0 },
	  force:{ x: 0, y: 0,z:0 },
	  displacement: { x: 0, y: 0,z:0 }
	},
	{
		id: 'n1', 
		parent: '11',
		degrees_of_freedom:[3,4,5]  ,
		restrained_degrees_of_freedom:[false,false,false],
		position: { x: -5, y: 7.5,z:-5  },
		force:{ x: 0, y: -200,z:0 },
		displacement: { x: 0, y: 0,z:0 }
	},
	{
		id: 'n2',
		parent: '11',
		degrees_of_freedom:[6,7,8]  ,
		restrained_degrees_of_freedom:[false,false,false],
		position: { x: -5, y: 7.5,z:5  },
		force:{ x: 0, y: -200,z:0 },
		displacement: { x: 0, y: 0,z:0 }
	},
	{
		id: 'n3',
		parent: '11',
		degrees_of_freedom:[9,10,11]  ,
		restrained_degrees_of_freedom:[false,false,false],
		position: { x: 5, y: 7.5,z:5  },
		force:{ x: 0, y:-200,z:0 },
		displacement: { x: 0, y: 0,z:0 }
	},
	{
		id: 'n4',
		parent: '11',
		degrees_of_freedom:[12,13,14]  ,
		restrained_degrees_of_freedom:[false,false,false],
		position: { x: 5, y: 7.5,z: -5  },
		force:{ x: 0, y: -200,z:0 },
		displacement: { x: 0, y: 0,z:0 }
	},
	{
		id: 'n5',
		parent: '11',
		degrees_of_freedom:[15,16,17]  ,
		restrained_degrees_of_freedom:[false,false,false],
		position: { x: 0, y: 15,z: 0  },
		force:{ x: 0, y: -400,z:0 },
		displacement: { x: 0, y: 0,z:0 }
	}
  	],
	edges: [
		{ id: 'e0', source: 0, target: 1 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e1', source: 0, target: 2 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e2', source: 0, target: 3 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e3', source: 0, target: 4 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },

		{ id: 'e4', source: 1, target: 2 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e5', source: 2, target: 3 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e6', source: 3, target: 4 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e7', source: 4, target: 1 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },

		{ id: 'e8', source: 5, target: 1 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e9', source: 5, target: 2 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e10', source: 5, target: 3 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },
		{ id: 'e11', source: 5, target: 4 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0 },

	],
	//material properties - AISI 1095 Carbon Steel (Spring Steel)
	ndofs   : 6*3,

	animation :  {
	
		showDisplacement : true,
		exaggeration : 10000,
		speed:3.0
		
	},
	viz :  {
	
		
		minStress:-500,
		maxStress: 500,
		colorMaps:[YlGnBu, winter, coolwarm,jet],
		colorMap:0,
		
	},
	solve: solveFea,
	bar:true
	
};

var setupEmpty={//empty
	nodes: [
		],
	edges: [
		],

	//material properties - AISI 1095 Carbon Steel (Spring Steel)
	ndofs   : 3*6,

	animation :  {
	
		showDisplacement : true,
		exaggeration : 20e2,
		speed:3.0
		
	},
	viz :  {
	
		
		minStress:10e6,
		maxStress: -10e6,
		colorMaps:[coolwarm,YlGnBu, winter ,jet],
		colorMap:0,
		
	},
	
};

/////////////////////drawing utils//////////////////////

function add_Node(setup,position,restrained,force){
	var bar=setup.bar;
	var numNode=setup.nodes.length;
	setup.nodes.push({
		id: 'n'+numNode, 
		parent: '11',
		degrees_of_freedom:[numNode*3,numNode*3+1,numNode*3+2] , 
		restrained_degrees_of_freedom:[restrained,restrained,restrained],
		position: toPos(position),
		force:toPos(force),
		displacement: { x: 0, y: 0,z:0 },
        angle: { x: 0, y: 0,z:0 }
    });
    
	setup.nodes[numNode].id='n'+numNode;
	setup.nodes[numNode].position=toPos(position);
	setup.nodes[numNode].force=toPos(force);
	setup.nodes[numNode].degrees_of_freedom=[numNode*3,numNode*3+1,numNode*3+2];
	setup.nodes[numNode].restrained_degrees_of_freedom=[restrained,restrained,restrained];
	setup.ndofs = 3 * setup.nodes.length;
	if(!bar){
		setup.nodes[numNode].degrees_of_freedom=[numNode*6,numNode*6+1,numNode*6+2,numNode*6+3,numNode*6+4,numNode*6+5];
		if(restrained){
			restrained=[true,true,true,true,true,true];
		}else{
			restrained=[false,false,false,false,false,false];
		}
		setup.nodes[numNode].restrained_degrees_of_freedom=restrained;
		setup.ndofs = 6 * setup.nodes.length;
	}
}

function add_Edge(setup,source,target,area,density,stiffness){
    if (edgeNeeded(setup,source,target)){
        var numEdge=setup.edges.length;

        setup.edges.push({ id: 'e'+numEdge, source: source, target: target ,area:area,density:density,stiffness:stiffness,stress:0 });

        // setup.edges[numEdge].id='e'+numEdge;
        // setup.edges[numEdge].source=source;
        // setup.edges[numEdge].target=target;
        // setup.edges[numEdge].area=area;
        // setup.edges[numEdge].density=density;
        // setup.edges[numEdge].stiffness=stiffness;

    }
	
}

function checkandAdd(setup,pos){
    var restrained=false;
    var force= new THREE.Vector3(0,0,0);
    var node=nodeAt(setup,pos);
    if(typeof node === 'undefined'){ //doesn't exist
        add_Node(setup,toThreeVec(pos),restrained, force);
        return parseInt(setup.nodes[setup.nodes.length-1].id.substring(1));
        
    }else{

        // setup.nodes.find(v => v.name === node.name).enabled = false;
        // if(restrained){
        //     restrain(setup,node.id)
        // }
        // addForce(setup,node.id,force);
        return parseInt(node.id.substring(1));//return name
    }
    
}

function linkNodes(setup,nodesIndices,area, density, stiffness){ //create square/circle 
    for(var i=0;i<nodesIndices.length-1;i++){
        add_Edge(setup,nodesIndices[i], nodesIndices[i+1], area, density , stiffness);

    }
    add_Edge(setup,nodesIndices[nodesIndices.length-1], nodesIndices[0], area, density , stiffness);
	//chi voxel
	add_Edge(setup,nodesIndices[0], nodesIndices[2], area, density , stiffness);
	add_Edge(setup,nodesIndices[1], nodesIndices[3], area, density , stiffness);
}

function restrain(setup,name){
    var restrained;
    if(!setup.bar){
        restrained=[true,true,true,true,true,true];
    }else{
        restrained=[true,true,true];
    }
    setup.nodes.find(v => v.id === name).restrained_degrees_of_freedom=restrained;
    
}

function addForce(setup,name,force){
    var node=setup.nodes.find(v => v.id === name);
    node.force.x += force.x;
    node.force.y += force.y;
    node.force.z += force.z;
}

function restrainBelow(setup,y){
    for(var i=0;i<setup.nodes.length;i++){
        if(setup.nodes[i].position.y<=y){
            if(setup.bar){
                setup.nodes[i].restrained_degrees_of_freedom=[true,true,true];
            }else{
                setup.nodes[i].restrained_degrees_of_freedom=[true,true,true,true,true,true];
            }
        }
    }
}

function addForceAbove(setup,y,force){
    for(var i=0;i<setup.nodes.length;i++){
        if(setup.nodes[i].position.y>=y){
            setup.nodes[i].force.x += force.x;
            setup.nodes[i].force.y += force.y;
            setup.nodes[i].force.z += force.z;
        }
    }
}

function restrainFromBox(setup,supports){
	for(var i=0;i<setup.nodes.length;i++){
		for(var j=0;j<supports.length;j++){
			if(supports[j][0].contains([setup.nodes[i].position.x,setup.nodes[i].position.y,setup.nodes[i].position.z])){
				setup.nodes[i].restrained_degrees_of_freedom=supports[j][1];
			}
		}
	}
}

function loadFromBox(setup,loads){
	for(var i=0;i<setup.nodes.length;i++){
		for(var j=0;j<loads.length;j++){
			if(loads[j][0].contains([setup.nodes[i].position.x,setup.nodes[i].position.y,setup.nodes[i].position.z])){
				var force=loads[j][1];
				setup.nodes[i].force.x += force.x;
				setup.nodes[i].force.y += force.y;
				setup.nodes[i].force.z += force.z;
			}
		}
	}
}

function changeMaterialFromBox(setup,box){
	for(var i=0;i<setup.edges.length;i++){
		for(var j=0;j<box.length;j++){
			if(edgeInBox(setup,setup.edges[i],box[j][0])){
				setup.edges[i].area=box[j][1].area;
				setup.edges[i].stiffness=box[j][1].stiffness;
				setup.edges[i].density=box[j][1].density;
			}
		}
	}
}

function edgeInBox(setup,edge,box){
	const node1=setup.nodes[edge.source];
	const node2=setup.nodes[edge.target];

	return box.contains([node1.position.x,node1.position.y,node1.position.z])&&box.contains([node2.position.x,node2.position.y,node2.position.z]);

}

function drawBox (min,max,color,scene,scale) {
    // var box = new THREE.Box3(new THREE.Vector3(min.x*scale,min.y*scale,min.z*scale),new THREE.Vector3(max.x*scale,max.y*scale,max.z*scale));
    // var mesh = new THREE.Box3Helper( box, color );
    // scene.add( mesh );
	console.log("hhhh")
    var geometry = new THREE.BoxBufferGeometry( max.x*scale-min.x*scale, max.y*scale-min.y*scale, max.z*scale-min.z*scale );
    var geometry = new THREE.BoxGeometry( max.x*scale-min.x*scale, max.y*scale-min.y*scale, max.z*scale-min.z*scale );

    var material = new THREE.MeshLambertMaterial( { wireframe:false, transparent: false,color:color, side: THREE.DoubleSide } );
    var mesh = new THREE.Mesh( geometry, material );

    scene.add( mesh );
    return mesh;
    // todo add name??
};

////////////
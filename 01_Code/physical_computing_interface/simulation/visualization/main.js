// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2019

var color1= 0xffffff; /*white*/
var color2= 0x020227;  /*kohly*/
var color3= 0x1c5c61; /*teal*/
var color4= "#fa6e70"; //red/orange
var color5="#380152"; //purple
var color6="#696767"; //grey
var color7="#03dbfc"; //blue


///////////globals (in case of hierarchal)/////////////
var camera;
// var gui = new dat.GUI();

var clock = new THREE.Clock();

var timeElapsed = clock.getElapsedTime();
var currTimeStep=0;
var increase=true;

var setupEmpty={//empty
	nodes: [
		],
	edges: [
		],

	//material properties - AISI 1095 Carbon Steel (Spring Steel)
	ndofs   : 3*6,

	animation :  {
	
		showDisplacement : true,
		// exaggeration : 10e2*5,
		exaggeration : 10e5/2,
		speed:3.0
		
	},
	viz :  {
	
		
		minStress:10e6,
		maxStress: -10e6,
		colorMaps:[YlGnBu,coolwarm, winter ,jet],
		colorMap:0,
		
	},
	
};

var setup=JSON.parse(JSON.stringify(setupEmpty));


function animate(){
	timeElapsed = clock.getElapsedTime();
	requestAnimationFrame(animate);
	if(increase){
		currTimeStep+=setup.animation.speed*setup.animation.speed*0.08; //todo change to globalSetup
	}else{
		currTimeStep-=setup.animation.speed*setup.animation.speed*0.08; //todo change to globalSetup
	}
	
}
animate();
///////////////////////////////


////////////threejs1 object and utils//////////////////////

function threejs1(setup,containerName,container1Name,static=true){
	// this.line;
	this.setup=setup;
	this.renderer;
	this.scene;
	// this.camera;
	this.controls;
	this.containerName=containerName;
	this.container = document.getElementById( this.containerName );
	this.container1Name=container1Name;
	// this.line1;
	this.matLineBasic;
	this.matLineDashed;
	this.static=static;
	this.labelRenderer=[];
	// viewport
	this.elementsGroup;
	

}

threejs1.prototype.init=function() {

	this.renderer = new THREE.WebGLRenderer( { antialias: true } );
	this.renderer.setPixelRatio( window.devicePixelRatio );
	this.renderer.setClearColor( color2, 1.0 );
	this.windowWidth=this.getWidth()
	this.windowHeight=this.getHeight()
	this.renderer.setSize( this.windowWidth, this.windowHeight );
	this.container.appendChild( this.renderer.domElement );
	this.scene = new THREE.Scene();

	camera = new THREE.PerspectiveCamera( 60, this.getWidth() / this.getHeight(), 1, 10000 );
	camera = new THREE.PerspectiveCamera( 45, this.getWidth()/ this.getHeight()  , 1, 10000*GLOBALS.voxelSpacing);
	// camera.position.set( -gridSize*voxelSize*1.0, 0, 60 );
	// camera.position.set(  40, 0, 60 );
	// camera = new THREE.PerspectiveCamera( 45, this.getWidth()/ this.getHeight()  , 1, 10000);
    
	camera.position.set( -this.setup.gridSize*voxelSize/2, this.setup.gridSize*voxelSize, this.setup.gridSize*voxelSize/2 );
    // camera.lookAt(this.setup.gridSize/2*voxelSize, 0, this.setup.gridSize/2*voxelSize );

	this.controls = new THREE.OrbitControls( camera, this.renderer.domElement );
	// this.controls.minDistance = 10;
	// this.controls.maxDistance = 500;
	this.controls.target= new THREE.Vector3(this.setup.gridSize/2*voxelSize, 0, this.setup.gridSize/2*voxelSize);
	this.controls.autoRotate=true;
	this.controls.update();

	this.matLineBasic = new THREE.LineMaterial( {
		color: 0xffffff,
		linewidth: 5, // in pixels
		vertexColors: THREE.VertexColors,
		//resolution:  // to be set by renderer, eventually
		dashed: false,
		visible:false
	} );
	// matLineDashed = new THREE.LineMaterial( {
	// 	color: 0xffffff,
	// 	linewidth: 5, // in pixels
	// 	vertexColors: THREE.VertexColors,
	// 	//resolution:  // to be set by renderer, eventually
	// 	dashed: true,
	// 	dashSize : 1.0,
	// 	gapSize : 1.0

	// } );
	
	// matLineDashed.defines.USE_DASH = ""; 
	this.matLineDashed = new THREE.LineMaterial( {
		color: 0xffffff,
		linewidth: 5, // in pixels
		vertexColors: THREE.VertexColors,
		//resolution:  // to be set by renderer, eventually
		dashed: false
	} );

	console.log(this.setup.gridSize)
	var helper = new THREE.GridHelper( this.setup.gridSize, this.setup.gridSize );
	helper.position.y = -voxelSize/2;
	helper.position.x = this.setup.gridSize*this.setup.voxelSize/2-this.setup.voxelSize/2;
	helper.position.z = this.setup.gridSize*this.setup.voxelSize/2-this.setup.voxelSize/2;
	helper.scale.x=this.setup.voxelSize;
	helper.scale.z=this.setup.voxelSize;
    helper.material.opacity = 0.5;
	helper.material.transparent = true;
	helper.name="gridHelper";
	this.scene.add( helper );

	this.elementsGroup=new THREE.Group();
	this.elementsGroup.name="elementsGroup";

	// this.labelRenderer = new THREE.CSS2DRenderer();
	// this.labelRenderer.setSize( this.getWidth(), this.getHeight() );
	// this.labelRenderer.domElement.style.position = 'absolute';
	// this.labelRenderer.domElement.style.top = 0;
	// this.container.appendChild( this.labelRenderer.domElement ); 
	// this.controls = new THREE.OrbitControls( camera, this.labelRenderer.domElement );
	
	
	// draw forces
	
	// draw degrees of freedom
	onWindowResize1();

	this.update(this.setup);

	if(!this.static){
		this.renderEuler();
		this.animateEuler()
	}else{
		this.render();
		this.animate();
	}
    // guiCreate(this);

	// this.container.addEventListener( 'mousemove', onDocumentMouseMoveThree, false );
    // this.container.addEventListener( 'mousedown', onDocumentMouseDown1, false );
    

	window.addEventListener( 'mouseup', onWindowResize1, false );
	window.addEventListener( 'resize', onWindowResize1, false );

	
};
function onDocumentMouseMove1( event ) {
    event.preventDefault();
}
function onDocumentMouseDown1( event ) {

    event.preventDefault();
}
threejs1.prototype.update=function(setup){
	this.setup=setup;
	this.drawStructure();
	this.scene.add(this.elementsGroup);
	this.colorEdges();
	this.drawConstraintBoundingBoxes();
}

threejs1.prototype.drawStructure=function() {
	//draw edges
	for(var i=0;i<this.setup.edges.length;i++){
		//this.createEdge(this.setup.nodes[this.setup.edges[i].source].position,this.setup.nodes[this.setup.edges[i].target].position,this.setup.edges[i].id,false);
		this.createEdge(this.setup.nodes[this.setup.edges[i].source].position,this.setup.nodes[this.setup.edges[i].target].position,this.setup.edges[i].id,true);
	}
	
	// draw nodes
	for(var i=0;i<this.setup.nodes.length;i++){
		//this.createNode(this.setup.nodes[i].position,this.setup.nodes[i].id,false,this.setup.nodes[i].restrained_degrees_of_freedom[0]);
		this.createNode(this.setup.nodes[i].position,this.setup.nodes[i].id,true,this.setup.nodes[i].restrained_degrees_of_freedom[0]);
	}

	this.drawForces();
	// scale=1
    // if (supports ) {
    //     for (var i=0;i< setup.supports.length;i++) {
    //         let s=setup.supports[i][0];
    //         drawBox(s.min,s.max,GLOBALS.color4,this.scene,scale);
    //     }
    // }
    // if (loads ) {
    //     for (var i=0;i< setup.loads.length;i++) {
    //         let l=setup.loads[i][0];
    //         console.log(l)
    //         drawBox(l.min,l.max,GLOBALS.color7,this.scene,scale);
    //     }
        
    // }
};

threejs1.prototype.createEdge=function(fromPoint,toPoint,name,displacement) {
	fromPoint=toThreeVec(fromPoint);//todo utils
	toPoint=toThreeVec(toPoint);//todo utils
	var positions = [];
	var colors = [];
	var points =[fromPoint,toPoint];

	var spline = new THREE.CatmullRomCurve3( points );
	var divisions = Math.round( 12 * points.length );

	var color = new THREE.Color();

	for ( var i = 0, l = divisions; i <= l; i ++ ) {
		var point = spline.getPoint( i / l );
		positions.push( point.x, point.y, point.z );
		
		// color.setHSL( i / l, 1.0, 0.5 );
		color=interpolateLinearly(i / l, this.setup.viz.colorMaps[this.setup.viz.colorMap]);
		// color=interpolateLinearly(i / l, coolwarm);
		colors.push( color[0], color[1], color[2]);
	}

	var geometry = new THREE.LineGeometry();
	geometry.setPositions( positions );
	geometry.setColors( colors );
	
	
	var line;
	if(displacement){
		line = new THREE.Line2( geometry, this.matLineDashed );
	}else{
		line = new THREE.Line2( geometry, this.matLineBasic );
	}
	line.computeLineDistances();
	line.scale.set( 1, 1, 1 );
	
	if(displacement){
		line.name='d'+name;
	}else{
		line.name=name;
	}

	
	this.elementsGroup.add(line);
	// this.scene.add( line );

	

};

threejs1.prototype.createNode=function(point,name,displacement,restrained) {
	var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	var material;

	if(restrained){
		material = new THREE.MeshBasicMaterial( {color: color4} );
	}else{
		material = new THREE.MeshBasicMaterial( {color: color3} );
	}

	if(displacement){
		material.transparent=true;
		material.opacity=0.7;
	}else{
		material.transparent=true;
		material.opacity=0.0;

	}
	var cube = new THREE.Mesh( geometry, material );
	cube.position.set(point.x, point.y, point.z);
	cube.scale.set(0.5, 0.5,0.5);

	if(this.setup.hierarchical){
		cube.scale.set(0.75*this.setup.voxelSize, 0.75*this.setup.voxelSize,0.75*this.setup.voxelSize);
	}
	
	if(displacement){
		cube.name='d'+name;
	}else{
		cube.name=name;
	}
	
	// var earthDiv = document.createElement( 'div' );
	// earthDiv.className = 'label';
	// earthDiv.textContent = name;
	// earthDiv.style.marginTop = '-1em';
	// var earthLabel = new THREE.CSS2DObject( earthDiv );
	// earthLabel.position.set( point.x*0.1, point.y*0.1, point.z*0.1 );
	// cube.add( earthLabel );
	
	this.elementsGroup.add(cube);
	// this.scene.add( cube );

	
};

threejs1.prototype.editEdge=function(fromPoint,toPoint,name) {
	var edge = this.scene.getObjectByName(name);
	var positions = [];
	var points =[fromPoint,toPoint];

	var spline = new THREE.CatmullRomCurve3( points );
	var divisions = Math.round( 12 * points.length );

	// var color = new THREE.Color();

	for ( var i = 0, l = divisions; i <=l; i ++ ) {
		var point = spline.getPoint( i / l );
		positions.push( point.x, point.y, point.z );
		
	}
	// edge.geometry.setDrawRange( 0, newValue );
	// edge.geometry.setPositions( positions );
	// edge.geometry.attributes.position.needsUpdate = true; 
	edge.geometry.setPositions( positions );
	// geometry.setColors( colors );
	
	
};

threejs1.prototype.drawForces=function() {
	for(var i=0;i<this.setup.nodes.length;i++){
		
		var dx=0.1;
		var o=toThreeVec(this.setup.nodes[i].position);
		var dir = toThreeVec(this.setup.nodes[i].force);
		var length = dir.length ();
		dir.normalize();
		var scale=0.002;
		if(length!=0){
			var arrowhelper=new THREE.ArrowHelper( dir, o.sub(dir), scale*length, color4 ,scale*length, scale*length);
			// var arrowhelper=new THREE.ArrowHelper( dir, o.sub(dir), scale*length, color5);
			arrowhelper.name="f"+i;
			this.elementsGroup.add(arrowhelper);
			// this.scene.add(arrowhelper);

		}
	}
};

threejs1.prototype.updateForce=function(id,f){
	var scale=0.0001;
	var force=new THREE.Vector3(0,f,0);
	var length = force.length ();
	var arrow = this.scene.getObjectByName("f"+id.substring(1));
	// arrowhelper=new THREE.ArrowHelper( force.normalize(), arrow.position, scale*length, color4 ,scale*length, scale*length);
	// arrowhelper.name="f"+id.substring(1);
	arrow.setDirection(force.normalize());
	arrow.setLength(scale*length,scale*length,scale*length);


}

///////
threejs1.prototype.animate=function() {
	requestAnimationFrame( this.animate.bind(this));
	this.render();
	
};

threejs1.prototype.animateEuler=function() {
	requestAnimationFrame( this.animateEuler.bind(this));
	this.renderEuler();
	
};

threejs1.prototype.render=function(){
	// main scene
	// this.labelRenderer.render( this.scene, camera );

	this.renderer.setClearColor( color2, 1 );
	
	this.renderer.setViewport( 0, 0, this.windowWidth, this.windowHeight );
	// renderer will set this eventually
	this.matLineBasic.resolution.set( this.windowWidth, this.windowHeight ); // resolution of the viewport
	this.matLineDashed.resolution.set( this.windowWidth, this.windowHeight ); // resolution of the viewport
	this.renderer.render( this.scene, camera );

	var speed=this.setup.animation.speed;
	var exaggeration=this.setup.animation.exaggeration;
	// var exaggeration=0.01;
	
	if(this.setup.animation.showDisplacement){
		//displacement animation edges
		for(var i=0;i<this.setup.edges.length;i++){
			var fromPoint=new THREE.Vector3(0,0,0);
			var toPoint=new THREE.Vector3(0,0,0);
			var node1=this.setup.nodes[this.setup.edges[i].source];
			var node2=this.setup.nodes[this.setup.edges[i].target];

			fromPoint.x = node1.position.x+node1.displacement.x*exaggeration+ Math.sin(timeElapsed*speed)* node1.displacement.x*exaggeration ;
			fromPoint.y = node1.position.y+node1.displacement.y*exaggeration+ Math.sin(timeElapsed*speed)* node1.displacement.y*exaggeration ;
			fromPoint.z = node1.position.z+node1.displacement.z*exaggeration+ Math.sin(timeElapsed*speed)* node1.displacement.z*exaggeration ;

			var node = this.scene.getObjectByName('d'+node1.id);

			node.position.x = fromPoint.x;
			node.position.y = fromPoint.y;
			node.position.z = fromPoint.z;

			node.rotation.x = 0+node1.angle.x*exaggeration+ Math.sin(timeElapsed*speed)* node1.angle.x*exaggeration ;
			node.rotation.y = 0+node1.angle.y*exaggeration+ Math.sin(timeElapsed*speed)* node1.angle.y*exaggeration ;
			node.rotation.z = 0+node1.angle.z*exaggeration+ Math.sin(timeElapsed*speed)* node1.angle.z*exaggeration ;
		

			toPoint.x   = node2.position.x+node2.displacement.x*exaggeration+ Math.sin(timeElapsed*speed)* node2.displacement.x*exaggeration ;
			toPoint.y   = node2.position.y+node2.displacement.y*exaggeration+ Math.sin(timeElapsed*speed)* node2.displacement.y*exaggeration ;
			toPoint.z   = node2.position.z+node2.displacement.z*exaggeration+ Math.sin(timeElapsed*speed)* node2.displacement.z*exaggeration ;

			node = this.scene.getObjectByName('d'+node2.id);

			node.position.x = toPoint.x;
			node.position.y = toPoint.y;
			node.position.z = toPoint.z;

			node.rotation.x = 0+node2.angle.x*exaggeration+ Math.sin(timeElapsed*speed)* node2.angle.x*exaggeration ;
			node.rotation.y = 0+node2.angle.y*exaggeration+ Math.sin(timeElapsed*speed)* node2.angle.y*exaggeration ;
			node.rotation.z = 0+node2.angle.z*exaggeration+ Math.sin(timeElapsed*speed)* node2.angle.z*exaggeration ;
		

			this.editEdge(fromPoint,toPoint,'d'+this.setup.edges[i].id);
			
		}
		//displacement animation nodes
		// for(var i=0;i<this.setup.nodes.length;i++){
		// 	var node = this.scene.getObjectByName('d'+this.setup.nodes[i].id);
		// 	// var origPos=toThreeVec(setup.nodes[i].position);
		// 	// var displacement=toThreeVec(setup.nodes[i].displacement).multiplyScalar(exaggeration);
		// 	// var currPos=origPos.clone().addVectors(displacement,displacement.clone().multiplyScalar(Math.sin(timeElapsed*speed)));
		// 	// nodeD.position.set(currPos.x,currPos.y,currPos.z);
		// 	node.position.x = this.setup.nodes[i].position.x+this.setup.nodes[i].displacement.x*exaggeration+ Math.sin(timeElapsed*speed)*this.setup.nodes[i].displacement.x*exaggeration ;
		// 	node.position.y = this.setup.nodes[i].position.y+this.setup.nodes[i].displacement.y*exaggeration+ Math.sin(timeElapsed*speed)*this.setup.nodes[i].displacement.y*exaggeration ;
		// 	node.position.z = this.setup.nodes[i].position.z+this.setup.nodes[i].displacement.z*exaggeration+ Math.sin(timeElapsed*speed)*this.setup.nodes[i].displacement.z*exaggeration ;

		// 	node.rotation.x = 0+this.setup.nodes[i].angle.x*exaggeration+ Math.sin(timeElapsed*speed)* this.setup.nodes[i].angle.x*exaggeration ;
		// 	node.rotation.y = 0+this.setup.nodes[i].angle.y*exaggeration+ Math.sin(timeElapsed*speed)* this.setup.nodes[i].angle.y*exaggeration ;
		// 	node.rotation.z = 0+this.setup.nodes[i].angle.z*exaggeration+ Math.sin(timeElapsed*speed)* this.setup.nodes[i].angle.z*exaggeration ;
		
		// }

	}

};

threejs1.prototype.renderEuler=function(){
	// main scene
	this.renderer.setClearColor( color2, 1 );
	this.renderer.setViewport( 0, 0, this.getWidth(), this.getHeight() );
	// renderer will set this eventually
	this.matLineBasic.resolution.set( this.getWidth(), this.getHeight() ); // resolution of the viewport
	this.matLineDashed.resolution.set( this.getWidth(), this.getHeight() ); // resolution of the viewport
	this.renderer.render( this.scene, camera );

	var speed=this.setup.animation.speed;
	var exaggeration=this.setup.animation.exaggeration;

	
	//todo later change how it's implemented
	if(this.setup.nodes[0].posTimeSteps){
		var numIntervals=this.setup.nodes[0].posTimeSteps.length;
		// if(currTimeStep>=numIntervals){
		// 	currTimeStep=numIntervals-1;
		// 	increase=false;
		// }else if(currTimeStep<0){
		// 	currTimeStep=0;
		// 	increase=true;
		// }
		if(currTimeStep>=numIntervals){
			currTimeStep=0;
		}

		var index=parseInt(currTimeStep);
		
		if(this.setup.animation.showDisplacement){
			//displacement animation edges
			for(var i=0;i<this.setup.edges.length;i++){
				var fromPoint=new THREE.Vector3(0,0,0);
				var toPoint=new THREE.Vector3(0,0,0);
				var node1=this.setup.nodes[this.setup.edges[i].source];
				var node2=this.setup.nodes[this.setup.edges[i].target];

				fromPoint.x =  node1.position.x+node1.posTimeSteps[index].x*exaggeration ;
				fromPoint.y =  node1.position.y+node1.posTimeSteps[index].y*exaggeration ;
				fromPoint.z =  node1.position.z+node1.posTimeSteps[index].z*exaggeration ;

				var node = this.scene.getObjectByName('d'+node1.id);
				//todo check if this is effecient or ject go thought nodes

				node.position.x = fromPoint.x;
				node.position.y = fromPoint.y;
				node.position.z = fromPoint.z;

				node.rotation.x = 0+node1.angTimeSteps[index].x*exaggeration ;
				node.rotation.y = 0+node1.angTimeSteps[index].y*exaggeration ;
				node.rotation.z = 0+node1.angTimeSteps[index].z*exaggeration ;
				
				this.updateForce(node1.id,getForce(node1.position.z,currTimeStep));
			

				toPoint.x   =  node2.position.x+node2.posTimeSteps[index].x*exaggeration ;
				toPoint.y   =  node2.position.y+node2.posTimeSteps[index].y*exaggeration ;
				toPoint.z   =  node2.position.z+node2.posTimeSteps[index].z*exaggeration ;

				node = this.scene.getObjectByName('d'+node2.id);

				node.position.x = toPoint.x;
				node.position.y = toPoint.y;
				node.position.z = toPoint.z;

				node.rotation.x = 0+node2.angTimeSteps[index].x*exaggeration ;
				node.rotation.y = 0+node2.angTimeSteps[index].y*exaggeration ;
				node.rotation.z = 0+node2.angTimeSteps[index].z*exaggeration ;

				this.updateForce(node2.id,getForce(node2.position.z,currTimeStep));
			

				this.editEdge(fromPoint,toPoint,'d'+this.setup.edges[i].id);
				if(globalSetup.updateStress){
					this.colorEdge(this.setup.edges[i].stressTimeSteps[index],'d'+this.setup.edges[i].id);
				}
				
				
			}

		}
	}

};

threejs1.prototype.getWidth=function(){
	this.windowWidth=$('#'+three.container1Name).width()
	

	
    // return container.style.width;
    return $('#'+three.container1Name).width() ;

	// return container.style.width;
	if(this.container1Name===""){
		return window.innerWidth;
	}else{
		return $('#'+this.container1Name).width() ;
	}
    
};

threejs1.prototype.getHeight=function(){
    // return container.style.height;
	this.windowHeight=window.innerHeight-$('#'+three.container1Name).height()-20
	return window.innerHeight-$('#'+three.container1Name).height()-20
	if(this.container1Name===""){
		console.log()
		return window.innerHeight ;
	}else{
		return $('#'+this.container1Name).height() ;
	}
	
};

/////events////

//todo change 
function onWindowResize1(event) {
	camera.aspect = three.getWidth() / sim.getHeight();
	camera.updateProjectionMatrix();
	sim.renderer.setSize( sim.getWidth(), sim.getHeight() );
	
}

////////////////

threejs1.prototype.colorEdges=function() {
	
	for(var ii=0;ii<this.setup.edges.length;ii++){
		var element=this.setup.edges[ii];
		//this.colorEdge(element.stress,element.id);
		this.colorEdge(element.stress,'d'+element.id);
	}
};

threejs1.prototype.colorEdge=function(val,name) {
	
	var colors = [];
	var val=map(val,this.setup.viz.minStress,this.setup.viz.maxStress,1.0,0.0);
	var divisions = Math.round( 12 * 2 );

	for ( var i = 0, l = divisions; i <= l; i ++ ) {
		color=interpolateLinearly(val, this.setup.viz.colorMaps[this.setup.viz.colorMap]);
		colors.push( color[0], color[1], color[2]);
	}
	var edge = this.scene.getObjectByName(name);
	edge.geometry.setColors( colors );

};

threejs1.prototype.drawConstraintBoundingBoxes=function() {
	let supports=this.setup.supports;
	let loads=this.setup.loads;
	if (supports ) {
		for (var i=1;i< supports.length;i++) {
			let s=supports[i][0];
			this.drawBox(s.min,s.max,GLOBALS.color7);
		}
	}
	if (loads ) {
		for (var i=1;i< loads.length;i++) {
			let l=loads[i][0];
			this.drawBox(l.min,l.max,GLOBALS.color4);
		}
		
	}
	
};

threejs1.prototype.drawBox=function(min,max,color) {
	var box = new THREE.Box3(new THREE.Vector3(min[0],min[1],min[2]),new THREE.Vector3(max[0],max[1],max[2]));
	var helper = new THREE.Box3Helper( box, color );
	this.elementsGroup.add(helper);
	// this.scene.add( helper );
	// todo add name??
};

threejs1.prototype.reset=function(setup){
	// console.log(this.elementsGroup.getObjectByName( "e0" ))
	// var object = this.scene.getObjectByName( "elementsGroup" );
	// if(object){
	// 	this.scene.remove(object);
	// }

	var scene=this.scene;
	this.scene.traverse( function ( object ) {
		
		if(object.name!="gridHelper"&&object.type!="Scene"){
			// console.log(object)
			// console.log(this.scene)
			// object.geometry.dispose();
			scene.remove(object);
		}
	//     // if ( object.type =="Mesh" ){
	//     //     if ( parseInt(object.name) >num-1){
	//     //         object.geometry.dispose();
	//     //         console.log(object)
	//     //         scene.remove(object);
	//     //     } 
	//     // }
	} );
	this.elementsGroup=new THREE.Group();
	this.elementsGroup.name="elementsGroup";

	setup=JSON.parse(JSON.stringify(setupEmpty));
	this.setup=setup;
	return this.setup;

}



/////////////////gui////////////////
function guiCreate (three){
	

	var f1 = gui.addFolder('Displacement Animation '+three.containerName);
	f1.add(three.setup.animation, 'showDisplacement');
	f1.add(three.setup.animation, 'exaggeration', 0, 10e4);
	f1.add(three.setup.animation, 'speed', 0, 5);

	var f2 = gui.addFolder('Stresses Visualization '+three.containerName);
	f2.add(three.setup.viz, 'minStress', -1000, 0).listen();
	f2.add(three.setup.viz, 'maxStress', 0, 1000).listen();
	f2.add(three.setup.viz, 'colorMap', {coolwarm:0, YlGnBu:1, winter:2,jet:3});

	// gui.add(setup, 'solve');

	for (j in f2.__controllers) f2.__controllers[j].onChange (updateColors.bind(this)); //todo check three
	
}

//todo remove this
function updateColors(){
	sim.colorEdges();
	// three1.colorEdges();
}










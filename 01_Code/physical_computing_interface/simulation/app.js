// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2019

/////////////////function calls/////////////////
//todo when calling say which gridsize and grid type
var utils= new utilities();
var GLOBALS=new globals(utils);

// 
var three=new threejs(GLOBALS,utils,'webgl','threejs1');
three.init();


initGraph();// todo enclose into class
initEditor();// todo enclose into class


function simulationFidelity() {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  
// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    if (event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }else if (event.target.matches('.High')) {
        GLOBALS.globalJson.simulationSteps=1000;
        setup.animation.exaggeration=10e2*5;
        x = document.getElementById("instructions");
        x.innerHTML = "Simulation fidelity set to high.";

    }else if (event.target.matches('.Medium')) {
        GLOBALS.globalJson.simulationSteps=500;
        setup.animation.exaggeration=10e2*5;
        x = document.getElementById("instructions");
        x.innerHTML = "Simulation fidelity set to medium.";
    }else if (event.target.matches('.Low')) {
        GLOBALS.globalJson.simulationSteps=10;
        x = document.getElementById("instructions");
        x.innerHTML = "Simulation fidelity set to low.";
    }
}

// changed for assembler
// var assembler= new Assembler(three,GLOBALS,1,50,[new THREE.Vector3(GLOBALS.gridSize*GLOBALS.voxelSpacing,0,0)],[new THREE.Vector3(GLOBALS.gridSize*GLOBALS.voxelSpacing,0,GLOBALS.gridSize/2.0*GLOBALS.voxelSpacing)]);
// assembler.run();


/////////////assmebly////////////////////
var assembler= new Assembler(three,GLOBALS,1,50,[new THREE.Vector3(0,0,0)],[new THREE.Vector3(GLOBALS.gridSize/2.0*GLOBALS.voxelSpacing,0,0)]);
assembler.run();


/////////////simulation////////////////////
///new simulation stuff 
var sim; //todo change location
var setup=JSON.parse(JSON.stringify(setupEmpty));

// var ex=gui.add(globalSetup, 'exaggeration', 0, 10e4).listen();
// var sp=gui.add(globalSetup, 'speed', 0, 5).listen();
// var sp=gui.add(globalSetup, 'updateStress').listen();

var static=true;

var latticeSize=10;
var voxelSize=5;
var globalSetup={
    exaggeration:1,
    speed:3.0,
    updateStress:false

};
var material,supports,loads;


setup.voxelSize=voxelSize;
setup.gridSize=GLOBALS.gridSize

document.addEventListener('runNode', function (e) { 
    // x = document.getElementById("instructions");x.innerHTML = "Simulation Started, wait a few seconds for the results!";

    // solveParallel(GLOBALS.globalJson.simulationSteps);
    solveFEA();

}, false);

document.addEventListener('changeNodeColor', function (e) { 
    if(e.detail.data.type=="force"){
        var load1=new _rhino3dm.BoundingBox(
            [-voxelSize/2+e.detail.x*voxelSize,
             +voxelSize/8+e.detail.z*voxelSize,
             -voxelSize/2+e.detail.y*voxelSize], 
            [voxelSize/2+voxelSize*e.detail.x,
             voxelSize/2+voxelSize*e.detail.z,
             voxelSize/2+voxelSize*e.detail.y]);
        console.log(load1)
        loads.push([load1,{x:0,y:-100,z:0}]);
    }
    resetMetavoxels(e);
}, false);

document.addEventListener('addNode', function (e) { 
    
    resetMetavoxels(e);
}, false);

document.addEventListener('removeNode', function (e) { 
    resetMetavoxels(e);
}, false);

function resetMetavoxels(e){
    setup=sim.reset();
    //get present voxels
    var present=[];
    // console.log(GLOBALS.timeline)
    GLOBALS.timeline.forEach(step => {
        
        
        if(step[0].addRemove){
            present.push(step[0])
        } else {
            
            for (var i=present.length-1; i>=0; i--) {
                var e= present[i];
                if ( (e.addRemove && e.x==step[0].x&&e.z==step[0].z &&e.y==step[0].y)) {
                    present.splice(i, 1);
                    break;       //<-- Uncomment  if only the first term has to be removed
                }
            }

        }

    });

    present.forEach(step => {
        var latticeCoordinates=new THREE.Vector3(step.x,step.z,step.y);
        var pos= latticeCoordinates.clone().multiplyScalar(voxelSize)
        createVoxel(setup,voxelSize,pos,material.area,material.density,material.stiffness);
        
        
    });
    setup.loads=loads
    setup.supports=supports
    restrainFromBox(setup,supports);
    loadFromBox(setup,loads);
    

    sim.update(setup);
    setup.animation.exaggeration=1.0;



    

}

rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    _rhino3dm = m; // global


    material={
		area:1.0,
		density:0.028,
		stiffness:10000000
    };

    material={
		area:1.6*6,
		density:0.028,
		stiffness:10000
    };
    
    var material2={
		area:1.0,
		density:0.028,
		stiffness:1000000
	};

    setup.hierarchical=false;
    setup.voxelSize=voxelSize;

    const position=new THREE.Vector3(0,0,0);
    const position2=new THREE.Vector3(voxelSize*2,0,0);

    ///
    //try bounding box conditions
    // var support=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,-voxelSize/2+position.y,-voxelSize/2+position.z], [voxelSize/2+position.x,voxelSize/2+position.y,voxelSize/2+position.z]);
    var support=new _rhino3dm.BoundingBox([-voxelSize/2,-voxelSize/2,-voxelSize/2], [voxelSize/2+voxelSize*GLOBALS.gridSize,-voxelSize/4,voxelSize/2+voxelSize*GLOBALS.gridSize]);

    // var temp=voxelSize*latticeSize-voxelSize;
    
    // var load=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,-voxelSize/2+position.y,0-voxelSize/2+position.z], [voxelSize/2+position.x,voxelSize/2+position.y,temp+voxelSize/2+position.z]);
    var load=new _rhino3dm.BoundingBox([-voxelSize/2,-voxelSize/2,-voxelSize/2], [voxelSize/2+voxelSize*GLOBALS.gridSize,voxelSize/2+voxelSize*GLOBALS.gridSize,voxelSize/2+voxelSize*GLOBALS.gridSize]);



    // var temp1=voxelSize*latticeSize-3*voxelSize;

    // var matB=new _rhino3dm.BoundingBox([-voxelSize/2+position2.x,-voxelSize/2+position2.y,temp1-voxelSize/2+position2.z], [voxelSize/2+position2.x,voxelSize/2+position2.y,temp]);

    var dof=[true,true,true,true,true,true];
    supports=[[support,dof]];
    loads=[[load,{x:0,y:-100,z:0}]];
    loads=[[load,{x:0,y:-1,z:0}]];
    // var diffMaterialBox=[[matB,material2]];
    setup.loads=loads
    setup.supports=supports
    ///
    

    // lateralLoadSetup(setup,position,latticeSize,voxelSize,createVoxel,supports,loads,material);
    // changeMaterialFromBox(setup,diffMaterialBox);

    // var latticeCoordinates=new THREE.Vector3(0,0,0);
    // var latticeCoordinates1=new THREE.Vector3(1,0,0);
    // var pos= latticeCoordinates.clone().multiplyScalar(voxelSize).add(position);
    // var pos1= latticeCoordinates1.clone().multiplyScalar(voxelSize).add(position);
    // createVoxel(setup,voxelSize,pos,material.area,material.density,material.stiffness);
    // createVoxel(setup,voxelSize,pos1,material.area,material.density,material.stiffness);
    // restrainFromBox(setup,supports);

    setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    // setup.viz.colorMaps=[coolwarm,YlGnBu, winter ,jet];
    setup.viz.minStress=10e6;
    setup.viz.maxStress=-10e6;
    
   

    setup.viz.exaggeration=globalSetup.exaggeration;
    
    setup.solve=solveParallel;
    setup.numTimeSteps=100;
    // setup.supports=supports;
    // setup.loads=loads;

   
    sim= new threejs1(setup,"webgl1","graph",static);
    sim.init();
    
    // three.drawConstraintBoundingBoxes([support],[load]);
   

    
    // gui.add(three.setup,"numTimeSteps", 0, 500).listen();
    // gui.add(three.setup,"solve");
    
    
    
    // document.getElementById("footer2").innerHTML = "Press solve for structure simulation.";

    // more stuff
    
    
});

function solveParallel(numTimeSteps){
    // document.getElementById("footer2").innerHTML = "Running...";
    
    /////////////////////////////////////////////////////
    

    var dt=0.0251646; //?? todo change to recommended step
    var dt=0.01; //?? todo change to recommended step
    

    setTimeout(function() { 
        simulateParallel( setup,numTimeSteps,dt,static,2);
    }, 1);

    // var dt=0.0251646; //?? todo change to recommended step
    // simulateParallel(setup1,numTimeSteps,dt);

    ///////////////////////////////////////
    // updateColors();
    // sim.animate();
    // three1.animate();
    
    /////////////////////

}

function solveFEA(){

    // const a = tf.tensor2d([1, 3, 3, 1, 4, 3, 1, 3, 4], [3, 3])
    // const b = tf.tensor2d([1, 3, 3], [3, 1])
    // console.log("matrix a")
    // a.print()
    // console.log("matrix d")
    // var d=det11(a)
    // console.log(d)
    // var d=det(a)
    // console.log(d)
    // // d.print()

    // const i = invertMatrix(a)
    // console.log("inverse i")
    // i.print()
    // const prod = i.dot(b)
    // console.log("b * i")
    // prod.print()

    // var displacements=lusolve(a.arraySync(), b.arraySync(),false);
    // console.log("b * i")
    // console.log(displacements)

    // console.log(setup.viz.colorMap)
    // console.log(sim.setup.viz.colorMap)
    sim.setup.viz.colorMap=1; //coolwarm
    setup.animation.exaggeration=10;

    console.log("exaggeration "+setup.animation.exaggeration)
    var ins = document.getElementById("instructions0");
	ins.innerHTML = "Simulation running, wait a few seconds for the results!";
    setTimeout(function() { 
        var ins = document.getElementById("instructions0");
	    ins.innerHTML = "Simulation running, wait a few seconds for the results!";
    }, 1);

    setTimeout(function() { 
        solveFea(setup,2)
        // simulateParallel( setup,numTimeSteps,dt,static,2);
    }, 2);

    setTimeout(function() { 
        var alumnimYieldStress=120;
		var ins = document.getElementById("instructions0");
        var txt = "Done! Displacement min:"+setup.viz.minDisplacement.toFixed(3)+" , max:"+ setup.viz.maxDisplacement.toFixed(3)+" mm.";
        var txt1 = " Stress min:"+setup.viz.minStress.toFixed(3)+" , max:"+ setup.viz.maxStress.toFixed(3)+" MPa.";
        var safety=alumnimYieldStress/Math.max(Math.abs(setup.viz.minStress),Math.abs(setup.viz.maxStress))
        var txt2 = " Safety factor is :"+safety.toFixed(2)+".";
		ins.innerHTML = txt+txt1+txt2;
	
	}, 3);

}

function runDEMViz(){
    $.getJSON("../simulation/dice_firmware/build/x86/src/sim/vis/sim_config.json", function(config) {
        var numParticles=config.particles.length;
        var maxFiles=config.parameters.save_max;
    
    
    
        var color1= 0xffffff; /*white*/
        var color2= '#020227';  /*kohly*/
        var color3= 0x1c5c61; /*teal*/
        var color4= "#fa6e70"; //red/orange
        var color5="#380152"; //purple
        var color6="#696767"; //grey
        var color7="#03dbfc"; //blue
    
        $.getJSON("../simulation/dice_firmware/build/x86/src/sim/vis/positions/0.json", function(json0) {
    
            
            var positions=json0.positions;
            var edges=[];
            var nodes=[];
    
            var scale=100.0;
    
    
            for (var i=0;i<2*numParticles;i+=2){
                nodes.push({
                    id:i,
                    x:positions[i],
                    y:positions[i+1],
                });
            }
    
            var gData = {
                nodes: nodes.map(node => ({ 
                    id:node.id,
                    px:node.x*scale-scale,
                    py:node.y*scale-scale,
                    pz:0,
                })),
                links: edges
                .filter(edge => edge.id)
                .map(edge => ({
                    source: 'n'+edge.source,
                    target: 'n'+edge.target,
                    color:getColor(edge.stress)
                }))
            };
            //color=interpolateLinearly(i / l, this.setup.viz.colorMaps[this.setup.viz.colorMap]);
    
            const Graph = ForceGraph3D().backgroundColor(color2)
                .width($('#simulation').width())
                .height($('#simulation').height())
                (document.getElementById('webgl1'))
                .d3Force('center', null)
                .d3Force('charge', null)
                // .linkDirectionalParticles(0.5)
    
                // .linkThreeObject(link => {
                //   // extend link with text sprite
                //   const sprite = new SpriteText(`${link.source} > ${link.target}`);
                //   sprite.color = 'lightgrey';
                //   sprite.textHeight = 1.5;
                //   return sprite;
                // })
                // .linkPositionUpdate((sprite, { start, end }) => {
                //   const middlePos = Object.assign(...['x', 'y', 'z'].map(c => ({
                //     [c]: start[c] + (end[c] - start[c]) / 2 // calc middle point
                //   })));
    
                //   // Position sprite
                //   Object.assign(sprite.position, middlePos);
                // })
    
                .linkWidth(1.0)
                .linkOpacity(1.0)
                .linkColor(color3)
                .nodeThreeObject(({ }) => new THREE.Mesh(
                    new THREE.BoxGeometry(scale/10.0, scale/10.0, scale/10.0),
                    new THREE.MeshLambertMaterial({
                        color: color3,
                        transparent: true,
                        opacity: 0.9
                    })
                ))
                .d3Force('box', () => {
    
                gData.nodes.forEach(node => {
                        node.fx=node.px;
                        node.fy=node.py;
                        node.fz=node.pz;
    
                    });
                })
                
                .cooldownTime(Infinity)
                .graphData(gData);
            //
    
    
            var count=0;
            var count1=0;
            var totalCount=0;
            var increment=true;
            var speed=1;
            var exaggeration=10000.0;
    
    
            setInterval(() => {
    
                $.getJSON("../simulation/dice_firmware/build/x86/src/sim/vis/positions/"+count+".json", function(json) {
    
                    var positions=json.positions;
                    var edges=[];
                    var nodes=[];
    
                    var scale=100.0;
    
    
                    for (var i=0;i<2*numParticles;i+=2){
                        nodes.push({
                            id:i,
                            x:positions[i],
                            y:positions[i+1],
                        });
                    }
    
                    gData = {
                        nodes: nodes.map(node => ({ 
                            id:node.id,
                            px:node.x*scale-scale,
                            py:node.y*scale-scale,
                            pz:0,
                        })),
                        links: edges
                        .filter(edge => edge.id)
                        .map(edge => ({
                            source: 'n'+edge.source,
                            target: 'n'+edge.target,
                            color:getColor(edge.stress)
                        }))
                    };
    
    
                    Graph.d3Force('box', () => {
    
                        gData.nodes.forEach(node => {   
    
    
                            node.fx=node.px;
                            node.fy=node.py;
                            node.fz=node.pz;
    
                        });
                    }).graphData(gData);;
    
                    count++;
                    if(count>=maxFiles){
                        count=0;
                    }
    
                        
                    //     count1=0;
                    // }
                    // count1++;
    
                    
                    // console.log(count)
                    // console.log(maxFiles)
                    
    
    
                    // if(count>speed){
                    // increment=false;
                    // }else if (count<0){
                    // increment=true;
                    // }
    
    
                    // if(increment){
                    // count++;
                    // }else{
                    // count--;
                    // }
                    // totalCount++;
                    // console.log(totalCount);
                });
    
                
            }, 200);
    
    
            var helper = new THREE.GridHelper( 2, 2 );
            helper.material.opacity = 0.5;
            helper.material.transparent = true;
            helper.scale.x=1.0*scale
            helper.scale.z=1.0*scale
            helper.rotation.x=Math.PI/2.0;
            Graph.scene().add(helper);
    
            Graph.scene()
    
        });
    });
}


// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2019

///////////////////////////globals///////////////////////////////
function Assembler(three,GLOBALS,numberOfRobots,speed,robotLocations,depositLocations){
	///////////////////////////THREE///////////////////////////////

	this.container =three.container;
	this.camera    =three.camera;    
	this.scene     =three.scene  ;   
	this.renderer  =three.renderer ; 
	this.guiControls =[];
	this.geometryGui; //todo needed?
	this.targetGui;//todo needed?
	this.jointsGui;//todo needed?
	this.jointsParams;//todo needed?
	this.targetParams;//todo needed?
	this.geometryParams;//todo needed?
	this.end;//todo needed?
	this.currentStep;
	this.DEG_TO_RAD = Math.PI / 180;
	this.RAD_TO_DEG = 180 / Math.PI;
	this.color2=GLOBALS.color2;//kohly
	this.color3=GLOBALS.color3;///*teal*/
	this.color4= GLOBALS.color4; //red/orange
	this.viewRobotArm=GLOBALS.viewRobotArm;

	///////////////////////////voxels///////////////////////////////
	this.voxel;
	this.voxelSpacing=GLOBALS.voxelSpacing;
	this.voxelLocations=[];
	this.voxelSlices=[];
	this.voxelSlicesCount=[];

	this.currentVoxelCount=0;
	this.build=[];

	this.globalZ=0;
	this.globalRank=0;

	this.voxelNum=0;
	this.gridSize=GLOBALS.gridSize;

	this.stepsCap=100; //change with very big simulations

	this.grid=[];

	///////////////////////////robot///////////////////////////////
	this.numberOfRobots=numberOfRobots; //change number of robots
	this.speed=speed; //change to speed up the simulation
	this.THREE1=[];
	this.robotBones = [];
	this.joints = [];
	this.angles = [];
	this.IK=[];

	this.robotState=[];

	this.THREERobot;
	this.VisualRobot= [];
	this.THREESimulationRobot= [];
	this.geo=[];
	this.defaultRobotState= [];
	this.target = [];
	this.control= [];
	this.leg= [];

	this.voxelNormal= [];
	this.normalAdjustmentVector= [];

	//TODO assert number of start locations same as deposit locations
	this.startLocations= depositLocations.slice();
	this.robotLocations= robotLocations.slice();

	this.THREE1dir= [];
	this.THREE1Pos= [];

	//path
	this.targetPositionMesh= [];
	this.carriedVoxel= [];

	this.goHome= [];

	//////////////////////

	this.steps=[];
	this.path= [];
	this.carry=[];

	this.totalNumberofSteps=[];

	this.locations=[]
	this.showPath=false;
	this.init(); //TODO INIT LATER, CHECK IF i CAN CHANGE THIS
}

///////////////////////function calls///////////////////////////////

Assembler.prototype.run=function(){
	for( i=0;i<this.numberOfRobots;i++)
	{
		// this.assemble(i);
		this.steps[i].push(this.startLocations[i]);
	}
}

///////////////////////////////scene///////////////////////////////
Assembler.prototype.declareGlobals=function(){
	for( i=0;i<this.numberOfRobots;i++)
	{
	    this.guiControls.push(null);
	
		this.THREE1.push(new THREE.Vector3(0,0,0) );
		this.robotBones.push([]);
		this.joints.push([]);
		this.angles.push([0, 0, 0, 0, 0, 0]);
		this.robotState.push({
			leg1Pos:new THREE.Vector3(1,0,0),
			leg2Pos:new THREE.Vector3(0,0,0),
			up:new THREE.Vector3(0,0,1),
			forward:new THREE.Vector3(1,0,0),
			Z:0,
			rank:0
		});

		// THREERobot.push(null);
		this.VisualRobot.push(null);
		this.THREESimulationRobot.push(null);
		this.geo.push(null);
		this.defaultRobotState.push(null);
		this.target.push(null);
		this.control.push(null);
		this.leg.push(1);
		this.voxelNormal.push(180);
		this.normalAdjustmentVector.push(new THREE.Vector3( 0, 0, 0));	
		// this.startLocations.push(new THREE.Vector3(0*this.voxelSpacing,10*this.voxelSpacing,0) );
		this.THREE1dir.push(new THREE.Vector3(1,0,0) );
		this.THREE1Pos.push(new THREE.Vector3(0,0,0) );

		//path
		this.targetPositionMesh.push(this.startLocations[i]);
		this.carriedVoxel.push(null);

		this.goHome.push(false);
		this.totalNumberofSteps.push(0);

		//////////////////////

		this.steps.push([]);
		this.carry.push([]);
		this.path.push({
			curve: null,
			currentPoint: 0,
			points:[],
			number:20,
			delay:1000/this.speed,
			timeout:0,
			cHeight:5.0*this.voxelSpacing,
			showPath:this.showPath,
			normals:[],
			changeLegs:[],
			changeRotation:[],
			normalAdjustments:[]
		});
		this.IK.push([]);

	}

	// //starting positions and pickup stations
	// if(numberOfRobots>1){
	// 	robotState[1].leg1Pos=new THREE.Vector3((GLOBALS.gridSize-1),(GLOBALS.gridSize-2),0);
	// 	robotState[1].leg2Pos=new THREE.Vector3((GLOBALS.gridSize-1),(GLOBALS.gridSize-1),0);
	// 	startLocations[1]=new THREE.Vector3((GLOBALS.gridSize-1)*voxelSpacing,(GLOBALS.gridSize-1)*voxelSpacing,0*voxelSpacing);

	// }
	// if(numberOfRobots>2){
	// 	robotState[2].leg1Pos=new THREE.Vector3((GLOBALS.gridSize-1),1,0);
	// 	robotState[2].leg2Pos=new THREE.Vector3((GLOBALS.gridSize-1),0,0);
	// 	startLocations[2]=new THREE.Vector3((GLOBALS.gridSize-1)*voxelSpacing,0*voxelSpacing,0*voxelSpacing);

	// }
	// if(numberOfRobots>3){
	// 	robotState[3].leg1Pos=new THREE.Vector3(1,GLOBALS.gridSize-1,0);
	// 	robotState[3].leg2Pos=new THREE.Vector3(0,GLOBALS.gridSize-1,0);
	// 	startLocations[3]=new THREE.Vector3(0*voxelSpacing,(GLOBALS.gridSize-1)*voxelSpacing,0*voxelSpacing);
	// }
	
}

Assembler.prototype.init=function() {
	this.declareGlobals();
	////////////////////load voxel/////////////////////////
	this.loadVoxel();
	//todo load dice????????
	///////////////////
	for( i=0;i<this.numberOfRobots;i++)
	{
		this.setupGUI(i);
		this.defaultRobot(i);
		this.buildHelperMeshes(i);
	}
}

///////////////////////////geometry///////////////////////////////
//implemented based on https://github.com/glumb/robot-gui
Assembler.prototype.THREERobot=function(robotIndex,V_initial, limits,group) {
	// this.THREERobot= function (robotIndex,V_initial, limits, scene) {
		
	this.THREE1[robotIndex] = new THREE.Group();

	

	let parentObject = this.THREE1[robotIndex];
	this.robotBones[robotIndex] = [];
	this.joints[robotIndex] = [];

	let x = 0,
	y = 0,
	z = 0;

	V_initial.push([0, 0, 0]) // add a 6th pseudo link for 6 axis	

	for (let i = 0; i < V_initial.length; i++) {
		var link = V_initial[i];

		var linkGeo = this.createCube(robotIndex,x, y, z, link[0], link[1], link[2], limits[i][0], limits[i][1], i);
		x = link[0];
		y = link[1];
		z = link[2];

		if(i==4)//todo change later
		{

			this.carriedVoxel[robotIndex]=this.voxel.clone();
			this.carriedVoxel[robotIndex].position.x=x+2.5*this.voxelSpacing;
			this.carriedVoxel[robotIndex].position.y=y;
			this.carriedVoxel[robotIndex].position.z=z ;
			this.carriedVoxel[robotIndex].visible=false;
			linkGeo.add( this.carriedVoxel[robotIndex] );
		}

		parentObject.add(linkGeo);
		parentObject = linkGeo;
		this.robotBones[robotIndex].push(linkGeo);
	}

	group.add(this.THREE1[robotIndex]);
	group.rotation.x=-Math.PI/2.0;

}

Assembler.prototype.createCube=function(robotIndex,x, y, z, w, h, d, min, max, jointNumber) {
	
	
	var thicken = 1.2*this.voxelSpacing;

	var w_thickened = Math.abs(w) + thicken;
	var h_thickened = Math.abs(h) + thicken;
	var d_thickened = Math.abs(d) + thicken;
	var op=0.5;

	var material = new THREE.MeshLambertMaterial({ color: this.color2,transparent:true,opacity:op ,});
	var geometry = new THREE.CubeGeometry(w_thickened, h_thickened, d_thickened);
	var mesh = new THREE.Mesh(geometry, material);

	

	mesh.position.set(w / 2, h / 2, d / 2);
	var group = new THREE.Object3D();
	group.position.set(x, y, z);
	group.add(mesh);

	// min = min / 180 * Math.PI
	// max = max / 180 * Math.PI

	var jointGeo1 = new THREE.CylinderGeometry  (1*this.voxelSpacing,1*this.voxelSpacing, 1 * 2*this.voxelSpacing, 32, 32, false, -min, 2 * Math.PI - max + min)
	var jointGeoMax = new THREE.CylinderGeometry(1*this.voxelSpacing,1*this.voxelSpacing, 1 * 2*this.voxelSpacing, 32, 32, false, -max, max)
	var jointGeoMin = new THREE.CylinderGeometry(1*this.voxelSpacing,1*this.voxelSpacing, 1 * 2*this.voxelSpacing, 32, 32, false, 0, -min)
	var jointMesh1 = new THREE.Mesh(jointGeo1, new THREE.MeshBasicMaterial({
	color: this.color2,transparent:false,opacity:op ,//0xffbb00,
	}));
	var jointMeshMax = new THREE.Mesh(jointGeoMax, new THREE.MeshBasicMaterial({
	color: this.color2,transparent:false,opacity:op ,//0x009900,
	}));
	var jointMeshMin = new THREE.Mesh(jointGeoMin, new THREE.MeshBasicMaterial({
	color: this.color2,transparent:false,opacity:op ,//0xdd2200,
	}));

	var joint = new THREE.Group();
	joint.add(jointMeshMax, jointMeshMin, jointMesh1);

	this.joints[robotIndex].push(joint);

	switch (jointNumber) {
		case 0:
			joint.rotation.x = Math.PI / 2
			break
		case 1:
			// joint.rotation.x = Math.PI / 2
			break
		case 2:
			// joint.rotation.x = Math.PI / 2
			break
		case 3:
			joint.rotation.z = Math.PI / 2
			// joint.rotation.y = Math.PI
			break
		case 4:
			// joint.rotation.x = Math.PI / 2
			joint.rotation.y = Math.PI / 2
			break
		case 5:
			joint.rotation.x = Math.PI / 2
			group.rotation.y = Math.PI / 2
		break;
	}

	group.add(joint);
	return group;
}

Assembler.prototype.setAngles=function(robotIndex){
	this.robotBones[robotIndex][0].rotation.z = this.angles[robotIndex][0];
	this.robotBones[robotIndex][1].rotation.y = this.angles[robotIndex][1];
	this.robotBones[robotIndex][2].rotation.y = this.angles[robotIndex][2];
	this.robotBones[robotIndex][3].rotation.x = this.angles[robotIndex][3];
	this.robotBones[robotIndex][4].rotation.y = this.angles[robotIndex][4];
	this.robotBones[robotIndex][5].rotation.z = this.angles[robotIndex][5];

}

Assembler.prototype.setAngle=function(robotIndex,index, angle){
	this.angles[robotIndex][index] = angle;
	this.setAngles(robotIndex);

}

///////////////////////initialization/////////////////////////////
Assembler.prototype.defaultRobot=function(robotIndex) {
	localState = {
		jointOutOfBound: [false, false, false, false, false, false],
	  };
	var maxAngleVelocity = 90.0 / (180.0 * Math.PI) / 1000.0;

	d1 = 0.1273;
	a2 = -0.612;
	a3 = -0.5723;
	d4 = 0.163941;
	d5 = 0.1157;
	d6 = 0.0922;

	shoulder_offset = 0.220941;
	elbow_offset = -0.1719;
	
	var scale=0.015*this.voxelSpacing;
	shoulder_height  =scale* d1;
	upper_arm_length =scale* -a2;
	forearm_length   =scale* -a3;
	wrist_1_length   =scale* d4 - elbow_offset - shoulder_offset;
	wrist_2_length   =scale* d5;
	wrist_3_length   =scale* d6;
	this.geo = [
		// [scale*200, 0, scale*50],
		// [-scale*100, 0, scale*600],
		// [0, 0, scale*600],
		// [scale*200, 0,0],
		// [0, 0, scale*200],
		// [scale*200, 0, 0],
		[scale*200, 0, scale*100],
		[0, 0, scale*700],
		[0, 0,scale*700],
		[scale*700, 0, 0],
		[scale*200, 0, 0],
		[scale*200, 0, 0],

	];
	this.defaultRobotState[robotIndex]  = {
		target: {
		  position: {
			x: this.startLocations[robotIndex].x,
			y: this.startLocations[robotIndex].y,
			z: this.startLocations[robotIndex].z,
		  },
		  rotation: {
			x: Math.PI,
			y: 0,
			z: 0,
		  },
		},
		angles: {
		  A0: 0,
		  A1: 0,
		  A2: 0,
		  A3: 0,
		  A4: 0,
		  A5: 0,
		},
		jointOutOfBound: [false, false, false, false, false, false],
		maxAngleVelocities: {
		  J0: maxAngleVelocity,
		  J1: maxAngleVelocity,
		  J2: maxAngleVelocity,
		  J3: maxAngleVelocity,
		  J4: maxAngleVelocity,
		  J5: maxAngleVelocity,
		},
		jointLimits: {
		  J0: [-190 / 180 * Math.PI, 190 / 180 * Math.PI],
		  J1: [-90 / 180 * Math.PI, 90 / 180 * Math.PI],
		  J2: [-135 / 180 * Math.PI, 45 / 180 * Math.PI],
		  J3: [-90 / 180 * Math.PI, 75 / 180 * Math.PI],
		  J4: [-139 / 180 * Math.PI, 90 / 180 * Math.PI],
		  J5: [-188 / 180 * Math.PI, 181 / 180 * Math.PI],
		},
		configuration: [false, false, false],
		geometry: {
		  V0: {
			x: this.geo[0][0],
			y: this.geo[0][1],
			z: this.geo[0][2],
		  },
		  V1: {
			x: this.geo[1][0],
			y: this.geo[1][1],
			z: this.geo[1][2],
		  },
		  V2: {
			x: this.geo[2][0],
			y: this.geo[2][1],
			z: this.geo[2][2],
		  },
		  V3: {
			x: this.geo[3][0],
			y: this.geo[3][1],
			z: this.geo[3][2],
		  },
		  V4: {
			x: this.geo[4][0],
			y: this.geo[4][1],
			z: this.geo[4][2],
		  },
		},
	  };
	  
	this.THREESimulationRobot[robotIndex] = new THREE.Group();
	
	this.THREESimulationRobot[robotIndex].visible=this.viewRobotArm;
	
	this.scene.add(this.THREESimulationRobot[robotIndex]);
	

	  ////////////////

	//////////////////
	
	this.updateIK(robotIndex);


	var geometry = Object.values(this.defaultRobotState[robotIndex].geometry).map((val, i, array) => [val.x, val.y, val.z]);
    var jointLimits = Object.values(this.defaultRobotState[robotIndex].jointLimits);

	this.THREERobot(robotIndex,geometry, jointLimits, this.THREESimulationRobot[robotIndex]);
	// var angles = Object.values(defaultRobotState.angles);
	// VisualRobot[robotIndex].setAngles(angles);
	var restAngle=60;
	// VisualRobot[robotIndex].setAngle(0,restAngle/ 180 * Math.PI);
	this.setAngle(robotIndex,1,restAngle*this.RAD_TO_DEG);
	this.setAngle(robotIndex,2,restAngle*this.RAD_TO_DEG);
	this.setAngle(robotIndex,3,restAngle*this.RAD_TO_DEG);
	this.setAngle(robotIndex,4,restAngle*this.RAD_TO_DEG);
	this.setAngle(robotIndex,5,restAngle*this.RAD_TO_DEG);
	
	this.targetControl(robotIndex);
	this.THREE1[robotIndex].position.x=this.robotLocations[robotIndex].x;
	this.THREE1[robotIndex].position.y=this.robotLocations[robotIndex].y;
	this.THREE1[robotIndex].position.z=this.robotLocations[robotIndex].z;

	this.updateAngles(robotIndex);

}

////////////////////////////////GUI///////////////////////////////
Assembler.prototype.setupGUI=function(robotIndex){
	//Parameters that can be modified.
	var start=this.startLocations[robotIndex];
	this.guiControls[robotIndex] = new function() {
		this.x = start.x;
		this.y = start.y;
		this.z = start.z;
		this.j1 = 0.0;
		this.j2 = 0.0;
		this.j3 = 0.0;
		this.j4 = 0.0;
		this.j5 = 0.0;
		this.j6 = 0.0;
		this.leg1 = 5.0;
		this.leg2 = 5.0;
		this.offset = 2.0;
		this.targetEnd="end 1";
		this.step=0;
	};

	
}

Assembler.prototype.updateGUI=function(robotIndex){
	for (var i=0;i<targetParams.length;i++) {
		targetParams[i].onChange(function(value) {
			this.updateAngles(robotIndex);
			this.control[robotIndex].position.x =this.guiControls[robotIndex].x;
			this.control[robotIndex].position.y =this.guiControls[robotIndex].y;
			this.control[robotIndex].position.z =this.guiControls[robotIndex].z;
			this.target[robotIndex].position.x = this.guiControls[robotIndex].x;
			this.target[robotIndex].position.y = this.guiControls[robotIndex].y;
			this.target[robotIndex].position.z = this.guiControls[robotIndex].z;
		});
	}

	for (var i=0;i<jointsParams.length;i++) {
		this.jointsParams[i].onChange(function(value) {
			if(this.leg[robotIndex]==1)
			{
				this.setAngle(robotIndex,0,this.guiControls[robotIndex].j1*this.DEG_TO_RAD);
				this.setAngle(robotIndex,1,this.guiControls[robotIndex].j2*this.DEG_TO_RAD);
				this.setAngle(robotIndex,2,this.guiControls[robotIndex].j3*this.DEG_TO_RAD);
				this.setAngle(robotIndex,3,this.guiControls[robotIndex].j4*this.DEG_TO_RAD);
				this.setAngle(robotIndex,4,this.guiControls[robotIndex].j5*this.DEG_TO_RAD);
				this.setAngle(robotIndex,5,this.guiControls[robotIndex].j6*this.DEG_TO_RAD);

			}else
			{
				this.setAngle(robotIndex,5,this.guiControls[robotIndex].j1*this.DEG_TO_RAD);
				this.setAngle(robotIndex,4,this.guiControls[robotIndex].j2*this.DEG_TO_RAD);
				this.setAngle(robotIndex,3,this.guiControls[robotIndex].j3*this.DEG_TO_RAD);
				this.setAngle(robotIndex,2,this.guiControls[robotIndex].j4*this.DEG_TO_RAD);
				this.setAngle(robotIndex,1,this.guiControls[robotIndex].j5*this.DEG_TO_RAD);
				this.setAngle(robotIndex,0,this.guiControls[robotIndex].j6*this.DEG_TO_RAD);
				

			}
			
			updateTarget(robotIndex);
		});
	}

	for (var i=0 ;i<geometryParams.length ;i++) {
		this.geometryParams[i].onChange(function(value) {
			this.updateRobotGeometry(robotIndex);
		});
	}

	this.currentStep.onChange(function(value) {
		this.step(robotIndex,guiControls[robotIndex].step);
	});

	this.end.onChange(function(value) {
		
		this.changeEnd(robotIndex);
	});


}

///////////////////////Inverse Kinematics////////////////////////
Assembler.prototype.updateIK=function(robotIndex) {
	const geo = Object.values(this.defaultRobotState[robotIndex].geometry).map((val, i, array) => [val.x, val.y, val.z])
	// todo not optimal, since IK is a sideeffect
	this.IK[robotIndex] = new InverseKinematic(geo);
}

Assembler.prototype.updateAngles=function(robotIndex){
	// const calculateAngles = (jointLimits, position, rotation, configuration) => {
	const angles = []
	
	this.IK[robotIndex].calculateAngles(
		this.guiControls[robotIndex].x,
		-this.guiControls[robotIndex].z,
		this.guiControls[robotIndex].y +2.5*this.voxelSpacing,
		this.defaultRobotState[robotIndex].target.rotation.x,
		this.defaultRobotState[robotIndex].target.rotation.y,
		this.defaultRobotState[robotIndex].target.rotation.z,
		angles,
		this.defaultRobotState[robotIndex].target.configuration
	)
	outOfBounds = [false, false, false, false, false, false]
	// let i = 0
	// for (const index in jointLimits) {
	// 	if (angles[i] < jointLimits[index][0] || angles[i] > jointLimits[index][1]) {
	// 	outOfBounds[i] = true
	// 	}
	// 	i++
	// }
	this.setAngle(robotIndex,0,angles[0]);
	this.setAngle(robotIndex,1,angles[1]);
	this.setAngle(robotIndex,2,angles[2]);
	this.setAngle(robotIndex,3,angles[3]);
	this.setAngle(robotIndex,4,angles[4]);
	this.setAngle(robotIndex,5,angles[5]);

	
	// this.angles[robotIndex][0]=angles[0];
	// this.angles[robotIndex][1]=angles[1];
	// this.angles[robotIndex][2]=angles[2];
	// this.angles[robotIndex][3]=angles[3];
	// this.angles[robotIndex][4]=angles[4];
	// this.angles[robotIndex][5]=angles[5];

	// this.guiControls[robotIndex].j1=this.angles[robotIndex][0]*this.RAD_TO_DEG;
	// this.guiControls[robotIndex].j2=this.angles[robotIndex][1]*this.RAD_TO_DEG;
	// this.guiControls[robotIndex].j3=this.angles[robotIndex][2]*this.RAD_TO_DEG;
	// this.guiControls[robotIndex].j4=this.angles[robotIndex][3]*this.RAD_TO_DEG;
	// this.guiControls[robotIndex].j5=this.angles[robotIndex][4]*this.RAD_TO_DEG;
	// this.guiControls[robotIndex].j6=this.angles[robotIndex][5]*this.RAD_TO_DEG;

	// this.control[robotIndex].size=0.5;
	return angles;

}

Assembler.prototype.getNormalAdjustment=function(robotIndex,n,vnormal,forward){//n is normal degree

	var result=new THREE.Vector3(0,0,0);
	if(n==180)
	{
		return result;
	}
	var theta=Math.abs(180-n);
	var base=2*Math.sin(theta/2*this.DEG_TO_RAD)*this.guiControls[robotIndex].offset;
	var x= Math.sin(((180-theta)/2)*this.DEG_TO_RAD)*base;
	var y= Math.cos(((180-theta)/2)*this.DEG_TO_RAD)*base;

	result= vnormal.clone().multiplyScalar(-y);

	if(n > 180)
	{
		var tempV=forward.clone().multiplyScalar(x);
		result.add(tempV);
		return result;
	}else
	{
		var tempV=forward.clone().multiplyScalar(-x);
		result.add(tempV);
		return result;
	}

}

////////////////////////Taget Control////////////////////////////
Assembler.prototype.targetControl=function(robotIndex){
	this.target[robotIndex] = new THREE.Group();
	this.target[robotIndex].visible=this.viewRobotArm;
	this.scene.add(this.target[robotIndex]);

	this.control[robotIndex] = new THREE.TransformControls(this.camera, this.renderer.domElement);
	this.target[robotIndex].position.x = this.guiControls[robotIndex].x;
	this.target[robotIndex].position.y = this.guiControls[robotIndex].y;
	this.target[robotIndex].position.z = this.guiControls[robotIndex].z;

	this.control[robotIndex].size=0.5;
	this.control[robotIndex].space = "local";
	// this.target[robotIndex].rotation.y=180*this.DEG_TO_RAD;
	// this.target[robotIndex].rotation.z=90*this.DEG_TO_RAD;
	this.target[robotIndex].rotation.x=-90*this.DEG_TO_RAD;
	this.target[robotIndex].rotation.z=-90*this.DEG_TO_RAD;
	// control[robotIndex].setSpace( control[robotIndex].space === "local" ? "world" : "local" );
	this.control[robotIndex].addEventListener('change', () => {
		this.control[robotIndex].size=0.5;
		this.guiControls[robotIndex].x= this.target[robotIndex].position.x;
		this.guiControls[robotIndex].y= this.target[robotIndex].position.y;
		this.guiControls[robotIndex].z= this.target[robotIndex].position.z;
		this.defaultRobotState[robotIndex].target.position.x=this.target[robotIndex].position.x;
		this.defaultRobotState[robotIndex].target.position.y=this.target[robotIndex].position.y;
		this.defaultRobotState[robotIndex].target.position.z=this.target[robotIndex].position.z;
		// defaultRobotState[robotIndex].target.rotation.x
		// defaultRobotState[robotIndex].target.rotation.y
		// defaultRobotState[robotIndex].target.rotation.z

		this.updateAngles(robotIndex);
	});
	this.control[robotIndex].attach(this.target[robotIndex]);

	this.control[robotIndex].visible=this.viewRobotArm;
	this.scene.add(this.control[robotIndex]);
	// control[robotIndex].visible = false;
}

Assembler.prototype.updateTarget=function(robotIndex){
	// console.log("hereee")
	var tempPosition=new THREE.Vector3(0,0,0);

	parent.updateMatrixWorld();

	var vector = new THREE.Vector3();
	vector.setFromMatrixPosition( child.matrixWorld );



	var object=this.THREE1[robotIndex].children[0].children[2].children[2].children[2].children[2];
	object.updateMatrixWorld();
	var vector = object.geometry.vertices[i].clone();
	vector.applyMatrix4( object.matrixWorld );

	tempPosition.x=this.THREE1[robotIndex].parent.parent.children[0].position.x;
	tempPosition.y=this.THREE1[robotIndex].parent.parent.children[0].position.y;
	tempPosition.z=this.THREE1[robotIndex].parent.parent.children[0].position.z;
	this.guiControls[robotIndex].x=tempPosition.x;
	this.guiControls[robotIndex].y=tempPosition.y;
	this.guiControls[robotIndex].z=tempPosition.z;

	this.control[robotIndex].position.x = this.guiControls[robotIndex].x;
	this.control[robotIndex].position.y = this.guiControls[robotIndex].y;
	this.control[robotIndex].position.z = this.guiControls[robotIndex].z;

	this.target[robotIndex].position.x = this.guiControls[robotIndex].x;
	this.target[robotIndex].position.y = this.guiControls[robotIndex].y;
	this.target[robotIndex].position.z = this.guiControls[robotIndex].z;
}

///////////////////////assembly/////////
Assembler.prototype.assemble=function(robotIndex){
	this.resetPath(robotIndex);
	this.buildShape();
	this.generatePoints(robotIndex);
	
	// steps[robotIndex]=[startLocations[robotIndex],new THREE.Vector3(10*voxelSpacing,0*voxelSpacing,0)];
	// showTargetPosition(robotIndex,minIndex,true);
	
	for(var i=0;i<this.steps[robotIndex].length-1;i++){
		if(i%2!=0){
			this.moveRobot(robotIndex,true,this.steps[robotIndex][i]);
		}else{
			this.moveRobot(robotIndex,false,this.steps[robotIndex][i+1]);
		}
	}

}

Assembler.prototype.createPath=function(robotIndex,start,end){
	var snormal=new THREE.Vector3(0,1,0);
	var enormal=new THREE.Vector3(0,1,0);
	var robotUp=new THREE.Vector3(0,1,0);
	var p1=start.clone();
	p1.add(snormal.clone().multiplyScalar(0));
	var p2=new THREE.Vector3(0,0,0);
	var p3=new THREE.Vector3(0,0,0);
	var p4=end.clone();
	p4.add(enormal.clone().multiplyScalar(0));

	var nor = snormal.clone();
	nor.add(enormal);
	nor.normalize();

	var dir=end.clone().sub(start);

	var temp1=new THREE.Vector3(0,0,0);
	var temp2=new THREE.Vector3(0,0,0);
	nor.multiplyScalar(this.path[robotIndex].cHeight);


	temp1.addVectors(start,dir.multiplyScalar(1/3));
	temp2.addVectors(start,dir.multiplyScalar(2));


	p2.addVectors(nor,temp1);
	p3.addVectors(nor,temp2);

	//create bezier curve
	this.path[robotIndex].curve= new THREE.CubicBezierCurve3 (
		p1,
		p2,
		p3,
		p4
	);
	this.dividePath(robotIndex,snormal.clone(),enormal.clone());
}

Assembler.prototype.dividePath=function(robotIndex,snormal,enormal,robotUp,robotLocation){

	var snormal=new THREE.Vector3(0,0,1);
	var enormal=new THREE.Vector3(0,0,1);
	var robotUp=new THREE.Vector3(0,0,1);
	var robotLocation=new THREE.Vector3(0,0,1);
	
	//points
	var d=1/this.path[robotIndex].number;
	var tempPoints=this.path[robotIndex].curve.getSpacedPoints(this.path[robotIndex].number);

	var forward=new THREE.Vector3(0,0,0);
	var tempV=tempPoints[0].clone().sub(robotLocation);
	if(tempV.x!=0 && robotUp.x==0)
	{
		forward.x=tempV.x;
	}else if(tempV.y!=0 && robotUp.y==0)
	{
		forward.y=tempV.y;
	}else if(tempV.z!=0 && robotUp.z==0)
	{
		forward.z=tempV.z;
	}
	forward.normalize();

	var p1=tempPoints[0];
	var p2=tempPoints[this.path[robotIndex].number];//was -1 todo
	var diff=p2.clone().sub(p1);
	diff.multiply(snormal);
	
	//normals
	var vnormal1=180;
	var vnormal2=180;
	if(!snormal.equals(enormal))
	{

		if(diff.x>0||diff.y>0||diff.z>0)
		{
			if(robotUp.equals(snormal))
			{
				vnormal1=180;
				vnormal2=90;

			}else
			{
				vnormal2=180;
				vnormal1=90;

			}
				
		}else if(diff.x<0||diff.y<0||diff.z<0)
		{
			if(robotUp.equals(snormal))
			{
				vnormal1=180;
				vnormal2=180+90;

			}else
			{
				vnormal2=180;
				vnormal1=180+90;

			}
		}
	}
	var dn=(vnormal2-vnormal1)/this.path[robotIndex].number;

	

	for (var i=0;i<=this.path[robotIndex].number;i++)
	{
		this.path[robotIndex].normals.push(vnormal1+i*dn);
		this.path[robotIndex].normalAdjustments.push(this.getNormalAdjustment(robotIndex,vnormal1+i*dn,robotUp,forward));
		this.path[robotIndex].points.push(tempPoints[i].clone());
		
		
		if(this.path[robotIndex].showPath)
		{
			var material = new THREE.MeshLambertMaterial({ color:this.color4,});
			var geometry = new THREE.SphereGeometry(0.05*this.voxelSpacing, 0.05*this.voxelSpacing, 0.05*this.voxelSpacing);
			var mesh = new THREE.Mesh(geometry, material);
			mesh.position.x=tempPoints[i].x;
			mesh.position.y=tempPoints[i].y;
			mesh.position.z=tempPoints[i].z;
			this.scene.add(mesh);
		}
	}
	
}

Assembler.prototype.moveRobot=function(robotIndex,leaveVoxel,voxelLoc){
	var ps= this.path[robotIndex].points.slice();
	var robot=this;
	if(leaveVoxel){
		setTimeout(function(){ robot.buildVoxelAt(voxelLoc);robot.showTargetPosition(robotIndex,voxelLoc,false) }, this.path[robotIndex].timeout);
		

	}else{
		setTimeout(function(){robot.showTargetPosition(robotIndex,voxelLoc,true) }, this.path[robotIndex].timeout);

	}
	for(var i=0;i<=robot.path[robotIndex].number;i++)
	{
		setTimeout(function(){ robot.move(robotIndex); }, this.path[robotIndex].timeout+=this.path[robotIndex].delay);
	}
	
	
	
}

Assembler.prototype.move=function(robotIndex,forward){
	this.guiControls[robotIndex].x=this.path[robotIndex].points[this.path[robotIndex].currentPoint].x;
	this.guiControls[robotIndex].y=this.path[robotIndex].points[this.path[robotIndex].currentPoint].y;
	this.guiControls[robotIndex].z=this.path[robotIndex].points[this.path[robotIndex].currentPoint].z;
	this.voxelNormal[robotIndex]=this.path[robotIndex].normals[this.path[robotIndex].currentPoint];
	this.normalAdjustmentVector[robotIndex]=this.path[robotIndex].normalAdjustments[this.path[robotIndex].currentPoint];

	this.updateAngles(robotIndex);

	this.control[robotIndex].position.x = this.guiControls[robotIndex].x;
	this.control[robotIndex].position.y = this.guiControls[robotIndex].y;
	this.control[robotIndex].position.z = this.guiControls[robotIndex].z;

	this.target[robotIndex].position.x = this.guiControls[robotIndex].x;
	this.target[robotIndex].position.y = this.guiControls[robotIndex].y;
	this.target[robotIndex].position.z = this.guiControls[robotIndex].z;
	
	if(forward){
		this.path[robotIndex].currentPoint++;
	}else{
		this.path[robotIndex].currentPoint--;
	}
	
}

Assembler.prototype.buildShape=function(){
	for(var i=0;i<this.gridSize;i++)
	{
		var t=[];
		this.grid.push(t)
		for(var j=0;j<this.gridSize;j++)
		{
			var tt=[];
			this.grid[i].push(tt);
			for(var k=0;k<this.gridSize;k++)
			{
				this.grid[i][j].push(false);
			}	
		}
	}
	for(var i=this.gridSize/2.0;i<this.gridSize;i++)
	{
		this.grid[i][0][0]=true;
		this.locations.push(new THREE.Vector3(i*this.voxelSpacing,0,0)); //TODO CHANGE LATER BASED ON NUMBER OF ROBOTS
	}

}

Assembler.prototype.generatePoints=function(robotIndex){
	for(var i=0;i<this.locations.length;i++){
		this.steps[robotIndex].push(this.startLocations[robotIndex]);
		this.steps[robotIndex].push(this.locations[i]);
		
	}
	this.steps[robotIndex].push(this.startLocations[robotIndex]);
	// console.log(this.steps[robotIndex])
	for(var i=0;i<this.steps[robotIndex].length-1;i++){
		this.createPath(robotIndex,this.steps[robotIndex][i],this.steps[robotIndex][i+1]);
	}
	

}

Assembler.prototype.addPath=function(robotIndex,loc,carry){
	this.steps[robotIndex].push(loc);
	this.carry[robotIndex].push(carry);
	this.createPath(robotIndex,this.steps[robotIndex][this.steps[robotIndex].length-2],this.steps[robotIndex][this.steps[robotIndex].length-1]);
	this.steps[robotIndex].push(this.startLocations[robotIndex]);
	this.carry[robotIndex].push(!carry);
	this.createPath(robotIndex,this.steps[robotIndex][this.steps[robotIndex].length-2],this.steps[robotIndex][this.steps[robotIndex].length-1]);

}

Assembler.prototype.stepRobot=function(robotIndex,stepIndex,forward){
	if(stepIndex>0&&stepIndex<this.steps[robotIndex].length){
		var robot=this;
		var voxelLoc=this.steps[robotIndex][stepIndex];
		
		// if(this.carry[stepIndex]){
		// 	setTimeout(function(){ robot.buildVoxelAt(voxelLoc);robot.showTargetPosition(robotIndex,voxelLoc,false) }, this.path[robotIndex].timeout);
			

		// }else{
		// 	setTimeout(function(){robot.showTargetPosition(robotIndex,voxelLoc,true) }, this.path[robotIndex].timeout);

		// }
		for(var i=0;i<=robot.path[robotIndex].number;i++)
		{
			setTimeout(function(){ robot.move(robotIndex,forward); }, this.path[robotIndex].timeout+=this.path[robotIndex].delay);
		}

	}
	

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

Assembler.prototype.getDirVec=function(direction){
	var vec = new THREE.Vector3(0,0,0);
	
	switch(direction)
	{
		case 0:	
			vec = new THREE.Vector3(1,0,0);
			break;
		case 1:	
			vec = new THREE.Vector3(-1,0,0);
			break;
		case 2:	
			vec = new THREE.Vector3(0,1,0);
			break;
		case 3:	
			vec = new THREE.Vector3(0,-1,0);
			break;
	}
	return vec;

}

Assembler.prototype.resetPath=function(robotIndex){
	//////////////////////
	this.steps[robotIndex]=[];
	this.path[robotIndex].curve=null;
	this.path[robotIndex].points=[];
	this.path[robotIndex].normals=[];
	this.path[robotIndex].changeRotation=[];
	this.path[robotIndex].normalAdjustments=[];
	this.path[robotIndex].currentPoint=0;
	this.path[robotIndex].timeout=0;
}

//////////////////////////load Voxels////////////////////////////
Assembler.prototype.loadVoxel=function(){
	var geometry = new THREE.BufferGeometry();
	// create a simple square shape. We duplicate the top left and bottom right
	// vertices because each vertex needs to appear once per triangle.
	var vertices = voxelData;
	var normals = voxelNormalData;
	var uv = voxelUVData;

	// itemSize = 3 because there are 3 values (components) per vertex
	geometry.setAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
	geometry.setAttribute( 'normal', new THREE.BufferAttribute( normals, 3 ) );
	geometry.setAttribute( 'uv', new THREE.BufferAttribute( uv, 2 ) );

	var material = new THREE.MeshLambertMaterial( { color: this.color3 } );
	var object = new THREE.Mesh( geometry, material );
	// object.scale.x=0.04;
	// object.scale.y=0.04;
	// object.scale.z=0.04;
	// object.position.z=-1.5;
	object.scale.x=0.5/3.0*this.voxelSpacing;
	object.scale.y=0.5/3.0*this.voxelSpacing;
	object.scale.z=0.5/3.0*this.voxelSpacing;
	object.position.x=-15;
	object.position.y=-15;
	this.voxel=object;
}

Assembler.prototype.frepVoxel=function(rangeX,rangeY,rangeZ,stringFunction){
	//build grid
	for(var i=0;i<this.gridSize;i++)
	{
		var t=[];
		this.grid.push(t)
		for(var j=0;j<this.gridSize;j++)
		{
			var tt=[];
			this.grid[i].push(tt);
			for(var k=0;k<this.gridSize;k++)
			{
				this.grid[i][j].push(false);
			}	
		}
	}
	//build first layer
	for(var i=0;i<this.gridSize;i++)
	{
		for(var j=0;j<this.gridSize;j++)
		{
			this.buildVoxelAt(new THREE.Vector3(i*this.voxelSpacing,j*this.voxelSpacing,0));
			this.grid[i][j][0]=true;	
		}
	}
	
	// string function= "Math.min(Math.min(Math.min(Math.min(X-(-1),(1)-X),Math.min(Y-(-1),(1)-Y)),Math.min(Z-(-1),(1)-Z)),-(Math.min(Math.min(Math.min(X-(-0.8),(0.8)-X),Math.min(Y-(-0.8),(0.8)-Y)),Math.min(Z-(-0.8),(0.8)-Z))))";
	var maxZslices=[];
	var tempVoxelSlices=[];
	for (var Z=0;Z<GLOBALS.gridSize;Z++)
	{
		voxelSlices.push([]);
		tempVoxelSlices.push([]);
		voxelSlicesCount.push([]);
		var max=- Infinity;
		var maxIndex=new THREE.Vector3(0,0,0);
		for (var Y=0;Y<GLOBALS.gridSize;Y++)
		{
			for (var X=0;X<GLOBALS.gridSize;X++)
			{
				var func= frep(X,Y,Z);

				if(func>=0 && !grid[X][Y][Z])
				{
					if(func>max)
					{
						max=func;
						maxIndex=new THREE.Vector3(X*voxelSpacing,Y*voxelSpacing, Z*voxelSpacing);

					}
					var loc=new THREE.Vector3(X*voxelSpacing,Y*voxelSpacing,Z*voxelSpacing);
					tempVoxelSlices[Z].push(loc);
					voxelNum++;
				}
			}
		}
		maxZslices.push(maxIndex);//check if right later
	}

	for (var Z=0;Z<GLOBALS.gridSize;Z++)
	{
		
		for(var i=0;i<tempVoxelSlices[Z].length;i++)
		{
			var rank = Math.ceil(maxZslices[Z].distanceTo(tempVoxelSlices[Z][i]) /voxelSpacing);
			while(voxelSlices[Z].length<=rank)
			{
				voxelSlicesCount[Z].push([]);

				voxelSlices[Z].push([]);

				voxelSlicesCount[Z][voxelSlices[Z].length-1]=0;

				voxelSlices[Z][voxelSlices[Z].length-1]=[];
			}
			voxelSlices[Z][rank].push(tempVoxelSlices[Z][i]);
		}
	}

	//////////
	for(var i=0; i<numberOfRobots;i++)
	{
		buildHelperMeshes(i);
	}
	

}

Assembler.prototype.frep=function(X,Y,Z){
	// return (3*3-((X-(5))*(X-(5))+(Y-(5))*(Y-(5))+(Z-(3))*(Z-(3)))); //sphere FIX!!
	// return Math.min(Math.min(Math.min(Math.min(X-(2),(7)-X),Math.min(Y-(2),(7)-Y)),Math.min(Z-(1),(5)-Z)),-(Math.min(Math.min(Math.min(X-(4),(8)-X),Math.min(Y-(4),(8)-Y)),Math.min(Z-(0),(4)-Z))));
	// return Math.min(Math.min(Math.min(Math.min(X-(2),(5)-X),Math.min(Y-(2),(5)-Y)),Math.min(Z-(1),(5)-Z)),-(Math.min(Math.min(Math.min(X-(3),(6)-X),Math.min(Y-(3),(6)-Y)),Math.min(Z-(0),(4)-Z))));
	// return Math.min(Math.min(Math.min(Math.min(X-(2),(7)-X),Math.min(Y-(2),(7)-Y)),Math.min(Z-(1),(6)-Z)),-(Math.min(Math.min(Math.min(X-(3),(6)-X),Math.min(Y-(3),(6)-Y)),Math.min(Z-(0),(7)-Z))));//empty cube
	// return Math.min(((6)-Z),(Math.min((Z-(1)),(((3*(5-(Z-(1)))/5)*(3*(5-(Z-(1)))/5)-((X-(5))*(X-(5))+(Y-(5))*(Y-(5))))))));//CONE
	return Math.min(Math.min(Math.min(X-(2),((GLOBALS.gridSize-3))-X),Math.min(Y-(2),((GLOBALS.gridSize-3))-Y)),Math.min(Z-(1),((GLOBALS.gridSize-4))-Z)); //CUBE
}

Assembler.prototype.buildVoxelAt=function(loc){
	var object1=this.voxel.clone();
	object1.position.x=loc.x;
	object1.position.y=loc.y;
	object1.position.z=loc.z+this.voxelSpacing/2;
	// if(loc.z==0){
	// 	var color1= new THREE.Color( 0, 0, 0 )

	// 	var material = new THREE.MeshLambertMaterial( { color: 0x000000 } );

	// 	// var color = new THREE.Color( 1/globalRank, 0, 0 );
		
	// 	object1.material=material.clone();
	// 	object1.material.color=color1;

	// }
	

	
	this.scene.add( object1 );
}
//////////////////////////path planing///////////////////////////

Assembler.prototype.voxelAt=function(location){
	if(location.x<0||location.y<0||location.z<0||location.x>=this.gridSize||location.y>=this.gridSize||location.z>=this.gridSize)
	{
		return false;
	}
	return this.grid[location.x][location.y][location.z];
}

Assembler.prototype.getDifference=function(location, targetPos){
	var diff=[];
	diff.push(location.x-targetPos.x/voxelSpacing);//0
	diff.push(location.y-targetPos.y/voxelSpacing);//1
	diff.push(location.z-targetPos.z/voxelSpacing);//2
	return diff.slice();
}

Assembler.prototype.getNeighbours=function( targetPos){
	var n=[];
	var t=targetPos.clone().multiplyScalar(1/voxelSpacing);
	// for(var i=-1;i<2;i++)
	// {
	// 	for(var j=-1;j<2;j++)
	// 	{
	// 		for(var k=-1;k<2;k++)
	// 		{
	// 			var tt=new THREE.Vector3(t.x+i,t.y+j,t.z+k);
	// 			if(!(tt.equals(t)) && voxelAt(tt) )
	// 			{
	// 				n.push(new THREE.Vector3(tt*voxelSpacing,tt*voxelSpacing,tt*voxelSpacing));
	// 			}
	// 		}
	// 	}
	// }
	var tt=new THREE.Vector3(t.x+1,t.y,t.z);
	if(voxelAt(tt))
	{
		n.push(new THREE.Vector3(tt.x*voxelSpacing,tt.y*voxelSpacing,tt.z*voxelSpacing));
	}
	tt=new THREE.Vector3(t.x-1,t.y,t.z);
	if(voxelAt(tt))
	{
		n.push(new THREE.Vector3(tt.x*voxelSpacing,tt.y*voxelSpacing,tt.z*voxelSpacing));
	}
	tt=new THREE.Vector3(t.x,t.y+1,t.z);
	if(voxelAt(tt))
	{
		n.push(new THREE.Vector3(tt.x*voxelSpacing,tt.y*voxelSpacing,tt.z*voxelSpacing));
	}
	tt=new THREE.Vector3(t.x,t.y-1,t.z);
	if(voxelAt(tt))
	{
		n.push(new THREE.Vector3(tt.x*voxelSpacing,tt.y*voxelSpacing,tt.z*voxelSpacing));
	}
	tt=new THREE.Vector3(t.x,t.y,t.z+1);
	if(voxelAt(tt))
	{
		n.push(new THREE.Vector3(tt.x*voxelSpacing,tt.y*voxelSpacing,tt.z*voxelSpacing));
	}
	tt=new THREE.Vector3(t.x,t.y,t.z-1);
	if(voxelAt(tt))
	{
		n.push(new THREE.Vector3(tt.x*voxelSpacing,tt.y*voxelSpacing,tt.z*voxelSpacing));
	}
	return n;
}
//////////////////////////utilities////////////////////////////

Assembler.prototype.buildHelperMeshes=function(robotIndex){
	var material = new THREE.MeshLambertMaterial({ color:this.color4,});
	var geometry = new THREE.SphereGeometry(0.5, 0.5, 0.5);
	this.targetPositionMesh[robotIndex] = new THREE.Mesh(geometry, material);
	this.targetPositionMesh[robotIndex].scale.x=0.8;
	this.targetPositionMesh[robotIndex].scale.y=0.8;
	this.targetPositionMesh[robotIndex].scale.z=0.8;
	// this.targetPositionMesh[robotIndex].visible=this.viewRobotArm;
	this.scene.add(this.targetPositionMesh[robotIndex]);

	for (var count=0; count<this.numberOfRobots;count++)
	{
		geometry = new THREE.BoxGeometry(this.voxelSpacing*1.1, this.voxelSpacing*1.1, this.voxelSpacing*1.1);
		mesh = new THREE.Mesh(geometry, material);
		mesh.position.x=this.startLocations[count].x;
		mesh.position.y=this.startLocations[count].y;
		mesh.position.z=this.startLocations[count].z;
		mesh.visible=this.viewRobotArm;
		this.scene.add(mesh);
	}

}

Assembler.prototype.showTargetPosition=function(robotIndex,targetPos,show){
	if(show)
	{
		
		this.targetPositionMesh[robotIndex].position.x=targetPos.x;
		this.targetPositionMesh[robotIndex].position.y=targetPos.y;
		this.targetPositionMesh[robotIndex].position.z=targetPos.z+this.voxelSpacing/2;
		// this.carriedVoxel[robotIndex].visible=true;//todo change
		// this.targetPositionMesh[robotIndex].visible=true;//todo change
		
	}else
	{
		this.carriedVoxel[robotIndex].visible=false;
		this.targetPositionMesh[robotIndex].visible=false;
		

	}
	
}

///////////////////////////////////////////////////////////////////////

document.addEventListener('addNode', function (e) { 
	var robotIndex=0;
	if(!e.detail.replay){ //first one create path
		assembler.addPath(robotIndex,new THREE.Vector3(e.detail.posX,e.detail.posY,e.detail.posZ),true);
		assembler.path[robotIndex].currentPoint=2*GLOBALS.timeStep*assembler.path[robotIndex].number;
		
	}else{ //move
		assembler.path[robotIndex].currentPoint=2*GLOBALS.timeStep*assembler.path[robotIndex].number;
		assembler.path[robotIndex].timeout=0;
		assembler.stepRobot(robotIndex,2*GLOBALS.timeStep,true);
		assembler.stepRobot(robotIndex,2*GLOBALS.timeStep+1,true);

		
    }
}, false);

document.addEventListener('removeNode', function (e) { 
	var robotIndex=0;
	if(!e.detail.replay){//first one create path
		assembler.addPath(robotIndex,new THREE.Vector3(e.detail.posX,e.detail.posY,e.detail.posY),false);
		assembler.path[robotIndex].currentPoint=2*GLOBALS.timeStep*assembler.path[robotIndex].number;
		
	}else{//first one create path
		assembler.path[robotIndex].currentPoint=2*GLOBALS.timeStep*assembler.path[robotIndex].number;
		assembler.path[robotIndex].timeout=0;
		assembler.stepRobot(robotIndex,2*GLOBALS.timeStep,false);
		assembler.stepRobot(robotIndex,2*GLOBALS.timeStep+1,false);
		
    }
}, false);









// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2019

/////////////////function calls/////////////////
//todo when calling say which gridsize and grid type
var utils= new utilities();
var GLOBALS=new globals(utils);

// 
var three=new threejs(GLOBALS,utils,'webgl','threejs1');
three.init();


initGraph();// todo enclose into class
initEditor();// todo enclose into class

// changed for assembler
// var assembler= new Assembler(three,GLOBALS,1,50,[new THREE.Vector3(GLOBALS.gridSize*GLOBALS.voxelSpacing,0,0)],[new THREE.Vector3(GLOBALS.gridSize*GLOBALS.voxelSpacing,0,GLOBALS.gridSize/2.0*GLOBALS.voxelSpacing)]);
// assembler.run();


/////////////assmebly////////////////////
var assembler= new Assembler(three,GLOBALS,1,50,[new THREE.Vector3(10,10,10)],[new THREE.Vector3(GLOBALS.gridSize/2.0*GLOBALS.voxelSpacing,0,0)]);
assembler.run();


//////////////////////////////python-urx//////////////////

var integratedSetup={
    ur10Setup:{
        parameters:{
            run:true,
            pythonFileName: "ur10test", //in ./assembly/python-urx/
            ipNode: "192.168.1.52",
            ipEdge: "192.168.1.53",
            latticePitch:{
                x:0.031,
                y:0.031,
                z:0.006,
            },
            nodePickupRobotHome:{
                x: -1.5394771734820765,
                y: -1.6685369650470179,
                z: -2.495819155369894,
                rx:-2.1159094015704554,
                ry:-1.5422142187701624,
                rz:-1.5874159971820276,
            },
            edgePickupRobotHome:{
                x: -0.21965343156923467,
                y: -1.5261443297015589,
                z: 2.5553131103515625,
                rx:-1.0277064482318323,
                ry:1.3498035669326782,
                rz:-1.5769990126239222,
            },
           edge90PickupRobotHome:{
                x: 0.14550010859966278,
                y:  -1.2639334837542933,
                z:  2.2356719970703125,
                rx:  -0.966320816670553,
                ry:   0.1400364190340042,
                rz: -1.5755623022662562,
            },
            programmingRelativeDistance:{
                x: -0.203,
                y: 0.0127,
                z: 0.013,
                rx:0,
                ry:0,
                rz:0,
            },
            nodeRelativeDistance:{
                x: -0.4417,
                y: 0.005,
                z: 0.04,
                rx:0,
                ry:0,
                rz:0
            },
            edgeRelativeDistance:{
                x: -0.2833,
                y: -0.008,
                z: 0.038,
                rx:0,
                ry:0,
                rz:0
            },
            v:0.5,
            a:0.5,
            v1:0.05,
            a1:0.05,
            sleep:0.2,
            sleep1:0.1,
    
    
        },
        nodes:[ //relative to the pickup
        ],
        edges:[ //relative to the pickup
            // { //s1
            //     x: 0.0075,
            //     y: -0.2833,
            //     z: 0.038,
            //     rx:0,
            //     ry:0,
            //     rz:0,
            // },
            // { //s2
            //     x: 0.0075,
            //     y: -0.3143,
            //     z: 0.038,
            //     rx:0,
            //     ry:0,
            //     rz:0,
            // },
            // { //s3
            //     x: 0,
            //     y: 0,
            //     z: 0.03,
            //     rx:0,
            //     ry:0,
            //     rz:Math.PI,
            // },
            // { //s4
            //     x: 0,
            //     y: -0.031,
            //     z: 0.03,
            //     rx:0,
            //     ry:0,
            //     rz:Math.PI,
            // },
            // { //s5
            //     x: 0,
            //     y: -0.062,
            //     z: 0.03,
            //     rx:0,
            //     ry:0,
            //     rz:Math.PI,
            // },
            // { //s6
            //     x: -0.0235,
            //     y: -0.2833,
            //     z: 0.038,
            //     rx:0,
            //     ry:0,
            //     rz:0,
            // },
            // { //s7
            //     x: -0.0235,
            //     y: -0.3143,
            //     z: 0.038,
            //     rx:0,
            //     ry:0,
            //     rz:0,
            // },

        ],
    },
    DEMSetup:{
        parameters: {
            run:false,
            make:false,
            delta_t: 1e-4,
            cutoff: 0.1,
            spring_constant: 1000.0,
            num_particles_per_node:10,
            region_pitch:1.0,
            save_every:20, //visualize every how much frames
            save_max:1000, //max num of files saved
        },
        particles:[],
    }
}



GLOBALS.selectedjson=integratedSetup;
GLOBALS.editor.set(GLOBALS.selectedjson);

document.addEventListener('runNode', function (e) { 
    startAssembler(integratedSetup);
}, false);

document.addEventListener('addNode', function (e) { 
    assembleNode(e,integratedSetup);
}, false);


function assembleNode(e,integratedSetup){

    var ur10Setup=integratedSetup.ur10Setup;
    // ur10Setup.nodes=[];
    // ur10Setup.edges=[];

    var x=e.detail.x;
    var y=e.detail.y;
    var z=e.detail.z;
    //
    ur10Setup.nodes.push({
        y: x,
        x: (GLOBALS.gridSize-y-1),
        z: z,
        rx:0,
        ry:0,
        rz:0,

    });
    //add edges
    console.log("x:"+x+", y:"+y+", z:"+z)
    var list=utils.getNeighborsList(GLOBALS.grid,x,y,z);
    console.log(list)

    for(var i=0;i<list.length;i++){
        var i1,j1,k1;
        [i1,j1,k1]=list[i];
        if ( !GLOBALS.occupancy[i1][j1][k1] && k1==z  ) {
            ur10Setup.edges.push({
                y: (x+0.5*(j1-y)),
                x: (GLOBALS.gridSize-y-1+0.5*(i1-x)),
                z: z*ur10Setup.parameters.latticePitch.z,
                rx:0,
                ry:0,
                rz:((j1-y)==0)? 0:Math.PI/2.0,
        
            });
        }
    }

    //add boundary
    var i=x-1;
    if ( i<0 ){
        //build
        ur10Setup.edges.push({
            y: (x),
            x: (GLOBALS.gridSize-y-1-0.5),
            z: z,
            rx:0,
            ry:0,
            rz:0,
    
        });
    }

    var i=x+1;
    if ( i==GLOBALS.gridSize ){
        //build
        ur10Setup.edges.push({
            y: (x),
            x: (GLOBALS.gridSize-y-1+0.5),
            z: z,
            rx:0,
            ry:0,
            rz:0,
    
        });
    }

    var j=y-1;
    if (  j<0 ){
        //build
        ur10Setup.edges.push({
            y: (x-0.5),
            x: (GLOBALS.gridSize-y-1),
            z: z,
            rx:0,
            ry:0,
            rz:Math.PI/2.0,
    
        });
    }

    var j=y+1;
    if (  j==GLOBALS.gridSize  ){
        //build
        ur10Setup.edges.push({
            y: (x+0.5),
            x: (GLOBALS.gridSize-y-1),
            z: z,
            rx:0,
            ry:0,
            rz:Math.PI/2.0,
    
        });
    }


    /////////////////////////////////////////////////
    //add particles to the DEM simulator
    for(var i=0;i<integratedSetup.DEMSetup.parameters.num_particles_per_node;i++){
        integratedSetup.DEMSetup.particles.push({
            position:[
                (Math.random()+(GLOBALS.gridSize-y-1))*integratedSetup.DEMSetup.parameters.region_pitch ,
                (Math.random()+(x))*integratedSetup.DEMSetup.parameters.region_pitch 
            ],
            velocity:[
                Math.random()*2.0-1.0 , Math.random()*2.0-1.0 
            ]
        });
    }
}

function startAssembler(integratedSetup){

    if(integratedSetup.DEMSetup.parameters.run){
        runDEMViz();

    }

    var ur10Setup=integratedSetup.ur10Setup;

    console.log("Run Assembler to assemble "+ur10Setup.nodes.length+ " node(s) and "+ur10Setup.edges.length+ " edge(s)!");
    console.log(ur10Setup.edges)
    $.post("/", { data : { setup : JSON.stringify(integratedSetup) } }, function(temp) {
    });
    //todo change that to message down 

    //delete list of nodes in setup
    ur10Setup.nodes=[];
    ur10Setup.edges=[];

    

    

}


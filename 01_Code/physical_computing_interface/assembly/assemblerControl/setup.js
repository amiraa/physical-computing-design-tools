var currentPort = null;
var macroList = ["homeXY","placeDice","pickDice","demoPickPlaceDice","setLowSpeed","setModerateSpeed","toggleCameraToolhead"];
var macroListSpan = document.getElementById("macroList");
var homeXY,placeDice,pickDice,demoPickPlaceDice,setLowSpeed,setModerateSpeed,toggleCameraToolhead;

// var pickupOrigin = {x:-100,y:-100};
// var placeOrigin = {x:-100,y:-28.545};
var pickupOrigin = {x:-151.1,y:-43.5};
var placeOrigin = {x:-139.7,y:-67.2};
var aOffset = 0;
var pickStep = {x:13,y:8};
var placeStep = {x:4.5,y:4.5,z:1.7};
var pickHeight =  -38.4;    
var placeHeight = -35;
// var pickHeight =  -35.7;    
// var placeHeight = -34;
var traverseHeight = -26;

let updatePickupOrigin_btn= document.getElementById('updatePickupOrigin');
updatePickupOrigin_btn.addEventListener( 'click', () => {
    pickupOrigin.x=machineCoords.x;
    pickupOrigin.y=machineCoords.y;
    // relativeMove("X",-xstepselect.value);
});

let updatePlaceOrigin_btn= document.getElementById('updatePlaceOrigin');
updatePickupOrigin_btn.addEventListener( 'click', () => {
    placeOrigin.x=machineCoords.x;
    placeOrigin.y=machineCoords.y;
    // relativeMove("X",-xstepselect.value);
});

// pick and place list in voxel coordinates
var pickupList = [{x:0,y:0},
                {x:1,y:0},
                {x:2,y:0},
                {x:0,y:1},
                {x:1,y:1},
                {x:2,y:1},
                {x:0,y:2},
                {x:1,y:2},
                {x:2,y:2}];

var placeList = [{x:0,y:0,z:0,a:0},
                {x:2,y:0,z:0,a:0},
                {x:1,y:1,z:0,a:0},
                {x:3,y:1,z:0,a:0},
                // {x:0,y:2,z:0,a:0},
                // {x:2,y:2,z:0,a:0},                     
                {x:1,y:0,z:1,a:90},
                // {x:1,y:2,z:1,a:90},
                // {x:1,y:1,z:2,a:0}
                ];
placeList=[];

document.addEventListener('addNode', function (e) { 

    var rot=e.detail.rotY*180/Math.PI;
    if (e.detail.z==0){
        placeList.push({x:2*e.detail.y,y:e.detail.x,z:e.detail.z,a:rot});
    }else{
        placeList.push({x:e.detail.y+1,y:e.detail.x,z:e.detail.z,a:rot});
    }
    
    // console.log(placeList);

}, false);

// assemblerControl();
// function assemblerControl()
// {
    // implementing a move queue:
    // using aysnc? https://caolan.github.io/async/docs.html#queue
    //              https://github.com/dustMason/GCode-Sender/blob/master/drawing-machine.js
    // make a class: queue
    //   has functions: pause, resume
    // send command calls a queue.pause()
    // on receipt of an appropriate sr or qr, call a queue.resume() (which sends the next command)
    // to call a function from the queue, simply have to have pause() and then have the function call a resume()

    const FPS = 30;
    const node_g2_url = "ws://localhost:8989/ws"; //The address that start-json-server script opens the WebSocket.

    var logging = true;
    var portsFlag = false;
    var statusFlag = false;

    var visionDisplay = "Color";

    var feeders = [];
    let utils1 = new Utils('errorMessage'); // Class instantiation
    let streaming = false, connected_to_tinyg = false, printing=false;

    var socket_connected = false;

    let socket = new WebSocket(node_g2_url); // actually opening the WebSocket

    let move_lock = false;
    let move_timeout;

    let last_command = ""; // Stores the last command sent to TinyG.

    let coordsysToolselect = document.getElementById('cordSystemToolSelect');

    let xminus_btn = document.getElementById('xleftbutton');
    let xplus_btn = document.getElementById('xrightbutton');
    let xstepselect = document.getElementById('xStepSelect');
    let yminus_btn = document.getElementById('ydownbutton');
    let yplus_btn = document.getElementById('yupbutton');
    let ystepselect = document.getElementById('yStepSelect');
    let zminus_btn = document.getElementById('zdownbutton');
    let zplus_btn = document.getElementById('zupbutton');
    let zstepselect = document.getElementById('zStepSelect');

    let aminus_btn = document.getElementById('adownbutton');
    let aplus_btn = document.getElementById('aupbutton');
    let astepselect = document.getElementById('aStepSelect');

    let zmax_btn = document.getElementById('gotozmaxbtn');
    
    let ztraverse_btn = document.getElementById('gotoztraversebtn');
    let zapproach_btn = document.getElementById('gotozapproachbtn');
    let zmin_btn = document.getElementById('gotozminbtn');

    let start_stop_btn = document.getElementById('start-stop');
    let start_stopp_btn = document.getElementById('start-stopp');
    let set_target_btn = document.getElementById('set-target');
    let tinyg_btn = document.getElementById('tinyg-setup');
    let start_printing_btn = document.getElementById('start-printing');
    let start_extruding_btn = document.getElementById('start-extruding');
    // let video_input = document.getElementById("videoInput"); // "videoInput" is a <video> element used as OpenCV.js input.
    let canvas_frame = document.getElementById("canvasFrame"); // "canvasFrame" a <canvas> element used as OpenCv.js output.
    let canvas_context = canvas_frame.getContext("2d");
    // let mapCanvas = document.getElementById("mapCanvas");
    // let mapContext = mapCanvas.getContext("2d");
    let mycanvas_frame = document.getElementById("mycanvas");
    let portSelect = document.getElementById("portSelect");
    // let calibrate_btn = document.getElementById("fourPointCalibrateBtn");
    let serialListAndConnect_btn = document.getElementById("serialListAndConnect");
    let visionSelect = document.getElementById("visionSelect");

    let flush_q_btn = document.getElementById("flush_q_btn");
    let stop_btn = document.getElementById("stop_btn");
    let start_resume_btn = document.getElementById("start_resume_btn");
    // let get_status_btn = document.getElementById("get_status_btn");
    // let print_measurements_btn = document.getElementById("printMeasurementsBtn");
    // let download_image_btn = document.getElementById("downloadImageBtn");

    let mapImage = document.getElementById("mapImage");

    let xPos = document.getElementById("xPosInput");
    let yPos = document.getElementById("yPosInput");
    let zPos = document.getElementById("zPosInput");
    let aPos = document.getElementById("aPosInput");

    let zBuildLayer = document.getElementById("zBuildLayerDisplay");
    let nodeCountDisplay = document.getElementById("nodeCountDisplay");
    let strutCountDisplay = document.getElementById("strutCountDisplay");

    let xPosInput = document.getElementById("xPosInput");

    let feeder1Select = document.getElementById("feeder1Select");
    feeders.push(feeder1Select);
    let feeder2Select = document.getElementById("feeder2Select");
    feeders.push(feeder2Select);
    let feeder3Select = document.getElementById("feeder3Select");
    feeders.push(feeder3Select);
    let feeder4Select = document.getElementById("feeder4Select");
    feeders.push(feeder4Select);
    let feeder5Select = document.getElementById("feeder5Select");
    feeders.push(feeder5Select);

    var NOT_READY = 1;
    var READY = 2;
    var MOVING = 3;
    // var moveQueue = [];
    var state = NOT_READY;

    

    var cmdID = 0;

    var measurements = [];

    let dimx = 0;//1277
    let dimy = 0;//963
    let offsetx = 250;
    let offsety = 10;

    var machineCoords = {
        x:0,
        y:0,
        z:0,
        a:0
    };

    var last_x, last_y;


    // let moveQList = document.getElementById("moveQList");

    // var macroList = ["homeXY","placeDice","pickDice","demoPickPlaceDice","setLowSpeed","setModerateSpeed","toggleCameraToolhead"];
    // let macroListSpan = document.getElementById("macroList");

    var moveQ;

    var nextNode = 0;
    var nextStrut = 0;

    var visionLayers = ["crosshair"];
    var cameraView = false;

    $(document).ready(function(){
        var moveQList = $('#moveQList');
        moveQ = new moveQueue(moveQList);
        populateMacroList();
        zBuildLayer.value = offsets.zBuildLayer;
    })


    ////////////////////////////// SETUP UI EVENTS /////////////////////////////////

    $('#move_queue_div').hide()
    // $('#other_div').hide()


    $('#macros_link').on('click', function () {
    $('#macros_div').show()
    $('#move_queue_div').hide()
    // $('#other_div').hide()
    })

    $('#move_queue_link').on('click', function () {
    $('#macros_div').hide()
    $('#move_queue_div').show()
    // $('#other_div').hide()
    })

    $('#other_link').on('click', function () {
    $('#macros_div').hide()
    $('#move_queue_div').hide()
    // $('#other_div').show()
    })

    function relativeMove(axis,distance) {
        moveQ.push(new qE("gc","G91"));
        moveQ.push(new qE("gc","G0"+axis+""+distance));
        moveQ.push(new qE("gc","G90"));
        moveQ.next();
        setTimeout(function() {
            moveQ.next();
        }, 100);
        setTimeout(function() {
            moveQ.next();
        }, 200);
    }

    function relativeMove2(commandString) {
        moveQ.push(new qE("gc","G91"));
        moveQ.push(new qE("gc","G0"+commandString));
        moveQ.push(new qE("gc","G90"));
        moveQ.next();
        setTimeout(function() {
            moveQ.next();
        }, 100);
        setTimeout(function() {
            moveQ.next();
        }, 200);
    }

    function relativeMoveImmediate(commandString) {
        sendMultiple(["G91","G0"+commandString,"G90"]);
    }

    xminus_btn.addEventListener( 'click', () => {
        // monitor.sendJSONmsg("G91G0X"+(-xstepselect.value)+"G90")
        relativeMoveImmediate("X"+(-xstepselect.value));
    })

    xplus_btn.addEventListener( 'click', () => {
        // monitor.sendJSONmsg("G91G0X"+(xstepselect.value)+"G90")
        relativeMoveImmediate("X"+xstepselect.value);
    })

    yminus_btn.addEventListener( 'click', () => {
        // monitor.sendJSONmsg("G91G0Y"+(-xstepselect.value)+"G90")
        relativeMoveImmediate("Y"+(-ystepselect.value));
    })

    yplus_btn.addEventListener( 'click', () => {
        // monitor.sendJSONmsg("G91G0Y"+(xstepselect.value)+"G90")
        relativeMoveImmediate("Y"+ystepselect.value);
    })

    zminus_btn.addEventListener( 'click', () => {
        // monitor.sendJSONmsg("G91G0Z"+(-xstepselect.value)+"G90")
        relativeMoveImmediate("Z"+(-zstepselect.value));
    })

    zplus_btn.addEventListener( 'click', () => {
        // monitor.sendJSONmsg("G91G0Z"+(xstepselect.value)+"G90")
        relativeMoveImmediate("Z"+zstepselect.value);
    })

    aminus_btn.addEventListener( 'click', () => {
        // monitor.sendJSONmsg("G91G0Z"+(-xstepselect.value)+"G90")
        relativeMoveImmediate("A"+(-astepselect.value));
    })

    aplus_btn.addEventListener( 'click', () => {
        // monitor.sendJSONmsg("G91G0Z"+(xstepselect.value)+"G90")
        relativeMoveImmediate("A"+astepselect.value);
    })

    zmax_btn.addEventListener( 'click', () => {
        monitor.sendJSONmsg("G90G0Z"+offsets.zMax);
        // relativeMove("X",-xstepselect.value);
    })

    

    ztraverse_btn.addEventListener( 'click', () => {
        monitor.sendJSONmsg("G90G0Z"+offsets.zTraverse);
        // relativeMove("X",-xstepselect.value);
    })

    zapproach_btn.addEventListener( 'click', () => {
        monitor.sendJSONmsg("G90G0Z"+offsets.zApproach);
        // relativeMove("X",-xstepselect.value);
    })

    zmin_btn.addEventListener( 'click', () => {
        monitor.sendJSONmsg("G90G0Z"+offsets.zMin);
        // relativeMove("X",-xstepselect.value);
    })


    start_stop_btn.addEventListener('click', () => {
        if (!streaming) {
            utils1.clearError();
            utils1.startCamera('vga', onVideoStarted, 'videoInput');
            
        } else {
            utils1.stopCamera();
            onVideoStopped();
        }
    });

    portSelect.addEventListener('change', (evt) => {
        monitor.log("changing port to: " + evt.target.value)
        socket.send("close " + monitor.port);
        monitor.setPort(evt.target.value);
        socket.send("open " + evt.target.value + " 115200 tinyg");
        getStatus();
    })

    // calibrate_btn.addEventListener('click', () => {
    //     if (monitor.port != null) {
    //         fourPointCalibration(50.8,45.037);
    //     }
    // })

    visionSelect.addEventListener('change', (evt) => {
        visionDisplay = evt.target.value;
        console.log("changing display: " + visionDisplay);
    });

    serialListAndConnect_btn.addEventListener('click', () => {
        if (monitor.port == null) {
            if (socket_connected) {
                socket.send("list");
                serialPortConnected();
            } else {
                monitor.err("socket not connected... try restarting the server and refreshing the page.")
            }
        } else {
            serialPortDisconnected();
        }
    })

    flush_q_btn.addEventListener('click', () => {
        flushQueue();
    });

    stop_btn.addEventListener('click', () => {
        stopImmediately();
    });

    start_resume_btn.addEventListener('click', () => {
        moveQ.resume();

        // to do: implement start --> pause --> resume --> pause

        // if (!moveQ.isStarted) {
        //     moveQ.next();
        //     start_resume_btn.innerHTML = "Pause"
        //     serialListAndConnect_btn.className = "btn";
        //     // serialListAndConnect_btn.classList.add("btn-primary");
        // } else {
        //     if (moveQ.isPaused) {
        //         // startCycle();
        //         moveQ.resume();
        //         start_resume_btn.innerHTML = "Pause"
        //         serialListAndConnect_btn.classList.remove("btn-success");
        //         // serialListAndConnect_btn.classList.add("btn-warning");
        //     } else {
        //         moveQ.pause();
        //         start_resume_btn.innerHTML = "Resume"
        //         serialListAndConnect_btn.classList.add("btn-success");
        //         // serialListAndConnect_btn.classList.remove("btn-warning");
        //     }
        // }
        

    })

    get_status_btn.addEventListener('click', () => {
        getStatus();
    })

    // print_measurements_btn.addEventListener('click', () => {
    //     printMeasurements();
    // })

    // download_image_btn.addEventListener('click', () => {
    //     downloadImage();
    // })

    /////////////////////////////// SERIALPORT HELPERS ////////////////////////////

    function serialPortConnected() {
        // enable buttons
        serialListAndConnect_btn.innerHTML = "Disconnect";
        serialListAndConnect_btn.classList.add("btn-danger");
        // calibrate_btn.removeAttribute('disabled');
        stop_btn.removeAttribute('disabled');
        flush_q_btn.removeAttribute('disabled');
        start_resume_btn.removeAttribute('disabled');
        get_status_btn.removeAttribute('disabled');
        // print_measurements_btn.removeAttribute('disabled');
    }

    function serialPortDisconnected() {
        socket.send("close " + monitor.port);
        monitor.setPort(null);
        portSelect.innerHTML = "";
        serialListAndConnect_btn.innerHTML = "List Ports";
        serialListAndConnect_btn.classList.remove("btn-danger");
        // calibrate_btn.disabled = true;
    }

    function populateSerialPorts(ports) {

        // sort ports containing "usb" to the top using a compare function
        ports = ports.sort(function(a, b) {
        return b.Name.indexOf("cu.usbserial") - a.Name.indexOf("cu.usbserial");
        });

        for (var i=0; i < ports.length; i++) {
            console.log("adding " + ports[i]["Name"])
            monitor.log("adding " + ports[i]["Name"])
            var option = document.createElement("option");
            option.text = ports[i]["Name"];
            portSelect.add(option);
        }
        
        monitor.setPort(ports[0].Name)
        socket.send("open " + ports[0].Name + " 115200 tinyg");
    }


    ////////////////////////////// OPENCV UTILITIES/SETUP ///////////////////////////

    function onVideoStarted() {
        streaming = true;
        start_stop_btn.innerText = 'Stop Camera';
        start_stop_btn.classList.remove("btn-success");
        start_stop_btn.classList.add("btn-danger");
        visionSelect.disabled = false;
        visionLayers.disabled = false;
        // download_image_btn.disabled = false;
        if (!hasLoaded) { cvHasLoaded() }
        setTimeout(processVideo(), 0)
    }

    function onVideoStopped() {
        streaming = false;
        canvas_context.clearRect(0, 0, canvas_frame.width, canvas_frame.height);
        start_stop_btn.innerText = 'Start Camera';
        start_stop_btn.classList.remove("btn-danger");
        start_stop_btn.classList.add("btn-success");
        visionSelect.disabled = true;
    }

    function reset_canvas() {
        $('#mycanvas').remove(); // this is my <canvas> element
        $('#canvas_col').append('<canvas id="mycanvas" width=320 height=240><canvas>');
    };

    utils1.loadOpenCv(() => {
        start_stop_btn.removeAttribute('disabled');
        cvHasLoaded();
        // populateFeederOptions();

        // displayMap();
    });


    /////////////////////////// WEBSOCKET UTILITIES /////////////////////////////


    socket.onopen = function (event) {
        console.log("socket opened");
        monitor.log("socket opened")
        socket_connected = true;
    };

    socket.onerror = function (err) {
        console.log(err);
        monitor.err("socket not connected... try restarting the server and refreshing the page.");
    };

    socket.onmessage = function (msg) {
        // Parsing the response from TinyG (through serial-server-json)
        if (msg.data != null) {
            if (msg.data == "list") {
                monitor.log("getting ports");
                portsFlag = true;
            } else if (portsFlag) {
                var data = JSON.parse(msg.data);
                populateSerialPorts(data.SerialPorts);
                portsFlag = false;
            } else {
                try {
                    var jsonObj = JSON.parse(msg.data.trim());

                    if (jsonObj.D != null) {

                        // log TinyG responses
                        // monitor.log("tinyg: " + jsonObj.D.trim());
                    
                        try {
                            var jsonObjData = JSON.parse(jsonObj.D);
                            // console.log(jsonObjData)

                            try { parseStatusReport(jsonObjData.r.sr) } catch(e) { }

                            try { parseStatusReport(jsonObjData.sr) } catch(e) { }
                            
                            

                            // try { parseStatusReport(jsonObjData.qr.sr) } catch(e) { }
                            

                        } catch (e) {
                            // console.log(e);
                        }
                    }
                    
                } catch (e) {
                    monitor.warn(msg.data);
                    // console.log(e)
                }
            }
        }
    };

    // low-level send command
    // (usually you'll want to use monitor.sendJSONmsg() instead)
    function socket_send(port,data,json=true) {
        var msg = {};
        msg.P = port;
        var dataObj = {};
        dataObj.D = data;
        dataObj.Id = (cmdID++).toString();
        msg.Data = [];
        msg.Data[0] = dataObj;
        if (json) {
            socket.send("sendjson " + JSON.stringify(msg));
        } else {
            socket.send(JSON.stringify(msg));
        }
        
    }

    function sendMultiple(strings) {
        var i = 0;
        for (var n=0; n<strings.length; n++) {
            setTimeout(function() {
                monitor.sendJSONmsg(strings[i]);
                i++;
            }, i*100);
        }
    }

    function dummyTinygResponse() {
        setTimeout(function(){ 
            monitor.log("finished move"); 
            // socket_send(monitor.port,"{sr:{stat:3}}",false)
            var status = {}
            status.stat = 3;
            parseStatusReport(status);
        }, 1000);
    }


    /////////////////////////// QUEUE ELEMENT //////////////////////////////

    function qE(type,content) {
        this.type = type;
        this.content = content;
        return this;
    }

    qE.prototype.toString = function() {
        var string;
        if (this.type == "gc") {
            string = this.content;
        } else if (this.type == "func") {
            string = this.content.name + "()";
        } else if (this.type == "vect") {
            string = "G0";
            if (this.content.x != undefined) {
                string += "X" + this.content.x;
            }
            if (this.content.y != undefined) {
                string += "Y" + this.content.y;
            }
        } else {
            monitor.log("unknown queue element type");
            return;
        }
        return string;
    }

    qE.prototype.add = function(otherVector) {
        if (this.type == "vect") {
            if (otherVector.x != undefined) this.content.x += otherVector.x;
            if (otherVector.y != undefined) this.content.y += otherVector.y;
            if (otherVector.z != undefined) this.content.z += otherVector.z;
        } else {
            monitor.warn("can only add vectors")
        }
    }

    /////////////////////////// MOVE QUEUE //////////////////////////////

    function moveQueue(UIElement) {
        this.queue = [];
        this.ui = UIElement;
        this.isPaused = false;
        this.isStarted = false;
        console.log(this.ui[0].children);

        this.numDisplayed = 8;
        this.initList();

        this.lastElementWasGrey = false;

        return this;
    }

    moveQueue.prototype.initList = function() {
        for (let i=0; i < this.numDisplayed; i++) {
            this.ui[0].appendChild(this.constructUIElement(" &nbsp; "));
        }
    }

    moveQueue.prototype.constructUIElement = function(cmd) {
        var newElement = document.createElement("LI");
        newElement.innerHTML = cmd;
        newElement.className = "small-font list-group-item";
        
        return newElement;
    }

    moveQueue.prototype.push = function(cmd) {
        //cmd is a queue element: (type, content)
            //type is gc (gcode) or func (function)
            //msg is the gcode string "g0x50" or the name of the function to call "measureFiducial()"

        this.queue.push(cmd);

        if (this.queue.length-1 < this.numDisplayed) {
            // need to add as well as replace whitespace
            var oldElement = this.ui[0].children[this.queue.length-1] 
            this.ui[0].replaceChild(this.constructUIElement(cmd.toString()),oldElement);
        } else {
            // don't need to add
        }
    }


    moveQueue.prototype.shift = function() {
        this.ui[0].removeChild(this.ui[0].children[0]);
        if (this.queue[this.numDisplayed] != undefined) {
            this.ui[0].appendChild(this.constructUIElement(this.queue[this.numDisplayed]))
        } else {
            this.ui[0].appendChild(this.constructUIElement(" &nbsp; "))
        }     

        return this.queue.shift();
    }

    moveQueue.prototype.next = function() {
        if (this.isPaused) {
            monitor.warn("queue paused");
            return;
        }
        var nextMove = this.shift();
        if (nextMove == undefined) {
            monitor.warn("move queue empty");
        } else {
            if (nextMove.type == "gc") {
                // var position = affineTransform(nextMove.content);
                monitor.sendJSONmsg(nextMove.content);
                
                // monitor.sendJSONmsg(position);
            } else if (nextMove.type == "vect") {
                var position = affineTransform(nextMove.content);
                // monitor.log(position.x + ", " + position.y)
                monitor.sendJSONmsg((new qE("vect",position)).toString());
            } else {
                monitor.log('calling function: ' + nextMove.toString())
                nextMove.content(this,this.next);
            }
        }
    }

    moveQueue.prototype.resume = function() {
        this.isPaused = false;
        this.next();
    }

    moveQueue.prototype.pause = function() {
        this.isPaused = true;
    }

    moveQueue.prototype.flush = function() {
        this.queue = [];

        for (let i=0; i < this.numDisplayed; i++) {
            this.ui[0].replaceChild(this.constructUIElement(" &nbsp; "),this.ui[0].children[i]);
        }
    }

    ////////////////////////////// OFFSETS ////////////////////////////////
    function add(v1, v2) {
            var x = v1[0]+v2[0];
            var y = v1[1]+v2[1];
            var z = v1[2]+v2[2];
            return [x,y,z];
    }

    function sub(v1, v2) {
        var x = v1[0]-v2[0];
        var y = v1[1]-v2[1];
        var z = v1[2]-v2[2];
        return [x,y,z];
    }

    var offsets = {};
    offsets.tool = [0,0,0,0];
    // offsets.camera = [40.75, 1.5, 13,0];
    // # camera at -159.153,-57.014
    // # toolhead at -128.608,-57.414
    offsets.cameraFromToolhead = [-30.545,0.3];
    // offsets.cameraFromToolhead = [-31.045,0.1];


    // offsets.toolrest = [-1.4, 8, -2];
    // offsets.buildplate = [66.6, 24.2, 27];
    // offsets.buildplate = [25.85, 22.7, 17, 90];
    // offsets.buildplate = [-5.709, -5.709, 5, 90]; //placement of 0th layer

    // var buildPlateOrigin = {x:-111.481,y:-57.414,z:-32.75};
    // offsets.buildPlateOrigin = [-128.608,-57.414,-32.75]; //left bottom-most node in 4x4 grid
    // offsets.buildPlateOrigin = [-128.666,-57.253,-32.75];

    // this is actually node location 2,2 (the third node location from the bottom and the left)
    // offsets.buildPlateOrigin = [-139.307, -68.457, -32.75];

    // offsets.nodePickupOrigin = [-151.694,-80.15,-30.555]; // left-most node pickup point
    // offsets.nodePickupOrigin = [-151.639,-80.087,-32.75];
    // offsets.nodePickupOrigin = [-151.438,-79.667,-32.75];

    // this is actually node location 2,2 (the third node location from the bottom and the left)
    // offsets.nodePickupOrigin = [-139.307, -68.457, -32.75];
    // offsets.nodePickupOrigin = [-139.785, -68.655, -33.5]; // old toolhead
    // offsets.nodePickupOrigin = [-139.585, -68.955, -33.5]; // new node pickup origin is same as strut
    offsets.nodePickupOrigin = [-139.246, -69.155, -33.5];
    // offsets.strutPickupOrigin = [-139.585, -68.955, -33.5];
    offsets.strutPickupOrigin = [-139.246, -68.955, -33.5];
    // offsets.actuatorPickupOrigin = [-150.222, -57.734, -30.5];
    offsets.actuatorPickupOrigin = [-150.622, -57.734, -31];


    offsets.zBuildLayer = -33.5;
    offsets.layerHeight = 1.005;
    // offsets.tool1 = [114.4, 20.5, 16, 0];
    offsets.tool1 = [-40,-31.5,-33.5,0];
    // offsets.nodes = [52.5,-22.5 ,32,87];
    // offsets.nodes = [52.15, -22.5, 30, 88.5]
    offsets.nodes = [20.591,-50.9,17,90];
    offsets.global_safe_z = 0;
    offsets.zMax = 0;
    offsets.zTraverse = -26;
    // offsets.zMin = -33.5; //-32.75
    offsets.zMin = -36;
    offsets.zApproach = -34; //30.5
    offsets.zApproachDistance = 2.25;
    offsets.zPreloadOffset = 0.25;
    offsets.pitch = 5.709;

    /////////////////////// TINYG EXECUTION //////////////////////////////

    function stopImmediately(flush_queue=false) {
        monitor.sendJSONmsg('!');
        if (flush_queue) {
            monitor.sendJSONmsg('%');
        }
    }

    function flushQueue() {
        monitor.sendJSONmsg('%');
        moveQ.flush();
    }

    function startCycle() {
        // monitor.sendJSONmsg('~');
        // moveQ.shift();
        moveQ.next();
    }

    function sayHi(context,callback) {
        setTimeout(function(){ 
            console.log('saying hi');
            monitor.log("hi!");
            callback.call(context);
        }, 1000);
    }

    function activateGripperCommand(context,callback) {
        monitor.log("gripping")
        monitor.sendJSONmsg("M3");
        setTimeout(function(){ 
            callback.call(context);
        }, 3000);
    }

    function activateHalfGripperCommand(context,callback) {
        monitor.log("gripping")
        monitor.sendJSONmsg("M4");
        setTimeout(function(){ 
            callback.call(context);
        }, 1000);
    }

    function deactivateGripperCommand(context,callback) {
        monitor.log("ungripping")
        monitor.sendJSONmsg("M5");
        setTimeout(function(){ 
            callback.call(context);
        }, 1000);
    }

    function measureFiducial(context,callback) {
        monitor.log("measuring fiducial center...")
        setTimeout(function(){ 
            monitor.log(center_x + ", " + center_y);
            recordMeasurement([center_x,center_y]);
            callback.call(context);
        }, 750);
    }

    function takePicture(context,callback) {
        monitor.log("taking picture...")
        setTimeout(function(){ 
            downloadImage()
            callback.call(context);
        }, 750);
    }

    function wait(context,callback,milliseconds=500) {
        setTimeout(function(){ 
            callback.call(context);
        }, milliseconds);
    }

    function pause(context,callback) {
        monitor.log("Paused. Waiting for user to resume...")
        moveQ.pause();
        callback.call(context);
    }

    function recordMeasurement(datapoint) {
        measurements.push(datapoint);
    }

    function printMeasurements() {
        for (let i=0; i < measurements.length; i++) {
            monitor.log("" + measurements[i][0] + ", " + measurements[i][1] + "");
        }
    }

    function clearMeasurements() {
        measurements = [];
    }

    function getStatus() {
        monitor.sendJSONmsg("{sr:n}");
    }

    function parseStatusReport(status, verbose=false) {  
        if (verbose) { monitor.log("got status report") }
        updateDRO(status);
        if (status.stat == 1 | status.stat == 3 | status.stat == 4) {
            monitor.log("ready: (status = " + status.stat + ")");
            state = READY;
            if (!moveQ.isPaused) { moveQ.next() }
            // moveQ.next();
        }
        
    }

    function parseResponse(response, verbose=false) {
        if (verbose) { monitor.log("got response") 
        monitor.log(response)}
        try { parseStatusReport(response.sr) } catch(e) {}
    }

    function parseStatusCode(statusCode, verbose=true) {
        if (verbose) { monitor.log("got status code") 
        monitor.log(statusCode)}
    }

    function updateDRO(status) {
        // try { if (status.posx != undefined) { xPos.value = status.posx } } catch (e) { }
        // try { if (status.posy != undefined) { yPos.value = status.posy } } catch (e) { }
        // try { if (status.posz != undefined) { zPos.value = status.posz } } catch (e) { }
        // try { if (status.posa != undefined) { aPos.value = status.posa } } catch (e) { }
        try { if (status.posx != undefined) { updateMachineCoord("x",status.posx) } } catch (e) { }
        try { if (status.posy != undefined) { updateMachineCoord("y",status.posy) } } catch (e) { }
        try { if (status.posz != undefined) { updateMachineCoord("z",status.posz) } } catch (e) { }
        try { if (status.posa != undefined) { updateMachineCoord("a",status.posa) } } catch (e) { }
        // displayMap(Math.round(dimx-xPos.value*21.473473933293278-offsetx),Math.round(dimy-yPos.value*21.473473933293278-offsety));
    }

    function updateMachineCoord(coord,value) {
        switch(coord) {
            case "x": xPos.value = value; break;
            case "y": yPos.value = value; break;
            case "z": zPos.value = value; break;
            case "a": aPos.value = value; break;
        }
        machineCoords[coord] = value;
        console.log(machineCoords[coord])
    }

    // function toGCode(coordObj) {
    //     var string
    // }

    function affineTransform(coordObj) {
    //[[ 9.98827416e-01 -8.86769486e-04  0.00000000e+00]
    // [-8.65512815e-03  9.99846546e-01  0.00000000e+00]
    // [-6.46633451e-02 -3.32916024e-01  1.00000000e+00]]

        var a = 0.998827416;
        var b = -0.000886769486;
        var c = -0.00865512815;
        var d = 0.999846546;

        if (coordObj.x == undefined) {
            var y = (c*coordObj.x + d*coordObj.y);
            return {y};
        } else if (coordObj.y == undefined) {
            var x = (a*coordObj.x + b*coordObj.y);
            return {x};
        } else {
            var x = (a*coordObj.x + b*coordObj.y);
            var y = (c*coordObj.x + d*coordObj.y);
            return {x,y};
        }
        
    }

    ////////////////////// MACRO ROUTINES ///////////////////////////

    function populateMacroList() {
        for (var i=0; i < macroList.length; i++) {
            console.log(macroList[i])
            var link = document.createElement('a');
            link.href="javascript:" + macroList[i] + "();";
            link.innerHTML = "" + macroList[i];
            // link.setAttribute('onclick',macroList[i]);
            macroListSpan.appendChild(link);
            macroListSpan.appendChild(document.createElement("BR"));
        }
    }

    homeXY=function() {
        moveQ.push(new qE("gc","G28.2X0Y0"));
        moveQ.next();
    }

    setLowSpeed = function() {
        // monitor.sendJSONmsg("$xvm=1000");
        // monitor.sendJSONmsg("$yvm=1000");
        // monitor.sendJSONmsg("$zvm=800");
        var msgs = ["$xvm=1000","$yvm=1000","$zvm=100"];
        // sendMultiple(msgs);
        moveQ.push(new qE("gc",msgs[0]));
        moveQ.push(new qE("gc",msgs[1]));
        moveQ.push(new qE("gc",msgs[2]));
        moveQ.next();
        setTimeout(function() {
            moveQ.next();
        }, 100);
        setTimeout(function() {
            moveQ.next();
        }, 200);
    }

    setHighSpeed= function() {
        // monitor.sendJSONmsg("$xvm=8000");
        // monitor.sendJSONmsg("$yvm=8000");
        // monitor.sendJSONmsg("$zvm=4000");
        var msgs = ["$xvm=8000","$yvm=8000","$zvm=800"];
        // var msgs = ["$xvm=8000","$yvm=8000","$zvm=800"];
        moveQ.push(new qE("gc",msgs[0]));
        moveQ.push(new qE("gc",msgs[1]));
        moveQ.push(new qE("gc",msgs[2]));
        moveQ.next();
        setTimeout(function() {
            moveQ.next();
        }, 100);
        setTimeout(function() {
            moveQ.next();
        }, 200);
    }

    setModerateSpeed=function (){
        // monitor.sendJSONmsg("$xvm=8000");
        // monitor.sendJSONmsg("$yvm=8000");
        // monitor.sendJSONmsg("$zvm=4000");
        // var msgs = ["$xvm=4000","$yvm=4000","$zvm=400"];
        var msgs = ["$xvm=4000","$yvm=4000","$zvm=400"];
        // var msgs = ["$xvm=8000","$yvm=8000","$zvm=800"];
        moveQ.push(new qE("gc",msgs[0]));
        moveQ.push(new qE("gc",msgs[1]));
        moveQ.push(new qE("gc",msgs[2]));
        moveQ.next();
        setTimeout(function() {
            moveQ.next();
        }, 100);
        setTimeout(function() {
            moveQ.next();
        }, 200);
    }

    function demoPickPlace() {
        // var pickupOrigin = {x:-100,y:-100};
        // var placeOrigin = {x:-100,y:-28.545};
        var pickupOrigin = {x:-113.1-5.709,y:-79.5}
        var placeOrigin = {x:-153.1-5.709,y:-39.5}
        var step = 5.709;
        var placeHeight = -25;
        var traverseHeight = 0;
        

        // pick and place list in voxel coordinates
        var pickupList = [{x:0,y:0},
                        {x:1,y:0},
                        {x:2,y:0},
                        {x:3,y:0},
                        {x:4,y:0},
                        {x:5,y:0},
                        {x:6,y:0},
                        {x:7,y:0},
                        {x:8,y:0}]

        var placeList = [{x:0,y:0},
                        {x:1,y:0},
                        {x:2,y:0},
                        {x:0,y:1},
                        {x:1,y:1},
                        {x:2,y:1},
                        {x:0,y:2},
                        {x:1,y:2},
                        {x:2,y:2}]

        var nParts = pickupList.length;


        moveQ.push((new qE("gc","G0Z"+traverseHeight)))

        for (var i=0; i < nParts; i++) {
            //pick
            moveQ.push((new qE("gc","G0X"+(pickupOrigin.x-pickupList[i].x*step)+"Y"+(pickupOrigin.y-pickupList[i].y*step))))
            // moveQ.push(new qE("func",wait));
            // moveQ.push(new qE("func",toggleCameraToolhead));
            moveQ.push((new qE("gc","G0Z"+placeHeight)))
            moveQ.push((new qE("gc","G0Z"+traverseHeight)))
            // moveQ.push(new qE("func",toggleCameraToolhead));
            //place
            moveQ.push((new qE("gc","G0X"+(placeOrigin.x+placeList[i].x*step)+"Y"+(placeOrigin.y-placeList[i].y*step))))
            // moveQ.push(new qE("func",wait));
            // moveQ.push(new qE("func",toggleCameraToolhead));
            moveQ.push((new qE("gc","G0Z"+placeHeight)))
            moveQ.push((new qE("gc","G0Z"+traverseHeight)))
            // moveQ.push(new qE("func",toggleCameraToolhead));
        }
        
    }


    function downloadImage() {
        // <a href="javascript:canvas_frame.toDataURL('image/jpeg');" download="download" >Download as jpeg</a>
        var link = document.createElement('a');
        link.setAttribute('download', 'image.png');
        link.setAttribute('href', canvas_frame.toDataURL("image/png").replace("image/png", "image/octet-stream"));
        link.click();
    }

    // function fourPointCalibration(dx,dy,set_zero=false) {
    //     // moveQ.push(new qE("gc","G0X"+dx));
    //     // moveQ.push(new qE("func",measureFiducial));
    //     // moveQ.push(new qE("gc","G0Y"+dy));
    //     // moveQ.push(new qE("func",measureFiducial));
    //     // moveQ.push(new qE("gc","G0X0"));
    //     // moveQ.push(new qE("func",measureFiducial));
    //     // moveQ.push(new qE("gc","G0Y0"));
    //     // moveQ.push(new qE("func",measureFiducial));

    //     moveQ.push(new qE("vect",{x:55,y:0}));
    //     moveQ.push(new qE("vect",{x:50.8,y:0}));
    //     moveQ.push(new qE("func",measureFiducial));
    //     moveQ.push(new qE("vect",{x:50.9,y:50}));
    //     moveQ.push(new qE("vect",{x:50.8,y:45.037}));
    //     moveQ.push(new qE("func",measureFiducial));
    //     moveQ.push(new qE("vect",{x:0,y:45.037}));
    //     moveQ.push(new qE("func",measureFiducial));
    //     moveQ.push(new qE("vect",{x:0,y:0}));
    //     moveQ.push(new qE("func",measureFiducial));
    // }

    function fourPointCalibration() {
        goToNodeLoc(2,2);
        toggleCameraToolheadCommand();
        moveQ.push(new qE("func",pause));
        toggleCameraToolheadCommand();
        goToNodeLoc(9,2);
        toggleCameraToolheadCommand();
        moveQ.push(new qE("func",pause));
        toggleCameraToolheadCommand();
        goToNodeLoc(9,9);
        toggleCameraToolheadCommand();
        moveQ.push(new qE("func",pause));
        toggleCameraToolheadCommand();
        goToNodeLoc(2,9);
        toggleCameraToolheadCommand();
        moveQ.push(new qE("func",pause));
        toggleCameraToolheadCommand();
    }

    function measureBacklash(dir="x") {
        var axis;
        if (dir == "x") {
            axis = "X";
        } else if (dir == "y") {
            axis = "Y";
        } else {
            monitor.err("invalid direction given");
        }

        var delta = 5;
        var ntimes = 10;
        // start centered on fiducial
        // (at 0,0)
        for (var i=0; i < ntimes; i++) {
            moveQ.push(new qE("gc","G0"+axis+"-"+delta));
            moveQ.push(new qE("gc","G0"+axis+"0"));
            moveQ.push(new qE("func",measureFiducial));
            moveQ.push(new qE("gc","G0"+axis+""+delta));
            moveQ.push(new qE("gc","G0"+axis+"0"));
            moveQ.push(new qE("func",measureFiducial));
        }
    }

    function calibrateCamera(dir="x") {
        var axis;
        if (dir == "x") {
            axis = "X";
        } else if (dir == "y") {
            axis = "Y";
        } else {
            monitor.err("invalid direction given");
        }

        var range = 1.5;
        var delta = 0.125;
        var ntimes = 1;
        // start centered on fiducial
        // (at 0,0)
        for (var i=0; i < ntimes; i++) {
            moveQ.push(new qE("gc","G0"+axis+"-"+(range+1.5)));
            for (var step=-range; step <= range; step+=delta) {
                moveQ.push(new qE("gc","G0"+axis+""+step));
                moveQ.push(new qE("func",measureFiducial));
            }
        }
    }

    function circles() {
        var spacing = 10;
        var x_range = 70;
        var y_range = 50;
        var cw = false;
        var backlashOffset = 5;
        // start centered on fiducial
        // (at 0,0)
        for (var ypos=0; ypos <= y_range; ypos+=spacing) {
            moveQ.push(new qE("gc","G0X"+(-backlashOffset)+"Y"+ypos));
            for (var xpos=0; xpos <= x_range; xpos+=spacing) {
                moveQ.push(new qE("gc","G0X"+xpos+"Y"+ypos));
                moveQ.push(new qE("func",measureFiducial));
            }
        }
    }

    function map() {
        var spacing = 5;
        var x_range = 50;
        var y_range = 50;
        var cw = false;
        var backlashOffset = 5;
        // start centered on fiducial
        // (at 0,0)
        for (var ypos=0; ypos <= y_range; ypos+=spacing) {
            moveQ.push(new qE("gc","G0X"+(-backlashOffset)+"Y"+ypos));
            for (var xpos=0; xpos <= x_range; xpos+=spacing) {
                moveQ.push(new qE("gc","G0X"+xpos+"Y"+ypos));
                moveQ.push(new qE("func",takePicture));
            }
        }
    }

    function displayMap(x,y) {
    // mapImage.position = ;
    let mapImage = new Image();
    mapImage.src = "map2.png"
    // mapImage.style.position = "absolute; top: 0px; left: 0px"
    // console.log(x + ", " + y)
    mapImage.onload = function(){
        // mapContext.rotate(Math.PI);
        // mapContext.scale(-1,1);
        // mapContext.translate(mapContext.width/2,mapContext.height/2);
        // mapContext.rotate(Math.PI);
        // mapContext.scale(-1,1);
        mapContext.drawImage(mapImage, -x, y, mapImage.width*-1,mapImage.height);
        
        // mapContext.restore();
    }

    // mapImage.style.position.top = '-400px';
    
    // console.log(mapContext);
    }

    function transformMap(x,y) {
        
    }

    function zero(context,callback,final=false) {
        var xc = precise(center_x,4);
        var yc = precise(center_y,4);
        var tol = 0.01;
        var backlashOffset = 2;
        var dx = 1;
        var dy = 1;

        moveQ.push(new qE("gc","G91G0X"+xc+"Y"+-yc));
        moveQ.push(new qE("func",checkDistance));
        last_x = xc;
        last_y = yc;
        // moveQ.next();

        try { callback.call(context); } catch(e) {}
        
        // setTimeout(function(){ 
        //     dx = center_x-xc;
        //     dy = center_y-yc;
        //     monitor.log(dx + ", " + dy);
        //     if (Math.abs(dx) < tol && Math.abs(dy) < tol && center_x < 0.1 && center_y < 0.1) {
        //         monitor.log("done");
        //         // if (!final) {
        //         //     monitor.log("found fiducial... offseting for backlash and iterating")
        //         //     moveQ.push(new qE("gc","G91G0X"+-backlashOffset+"Y"+-backlashOffset))
        //         //     moveQ.next();
        //         //     setTimeout(function(){ 
        //         //         zero(true);
        //         //     }, 750);
        //         // } else {
        //         //     monitor.log("done")
        //         // }
        //         return;
        //     } else {
        //         monitor.log("iterating")
        //         zero();
        //     }        
        // }, 750);
    }

    function checkDistance(context,callback) {
        var dx = center_x-last_x;
        var dy = center_y-last_y;
        var tol = 0.01;
        monitor.log(dx + ", " + dy);
        if (Math.abs(dx) < tol && Math.abs(dy) < tol && center_x < 0.1 && center_y < 0.1) {
            monitor.log("done");
            // if (!final) {
            //     monitor.log("found fiducial... offseting for backlash and iterating")
            //     moveQ.push(new qE("gc","G91G0X"+-backlashOffset+"Y"+-backlashOffset))
            //     moveQ.next();
            //     setTimeout(function(){ 
            //         zero(true);
            //     }, 750);
            // } else {
            //     monitor.log("done")
            // }
            return;
        } else {
            monitor.log("iterating")
            moveQ.push(new qE("func",zero));
            // moveQ.next();
            callback.call(context);
        }        
    }

    function putDownPickUpGrid() {
        var buildPlateOrigin = {x:-111.481,y:-57.414,z:-32.75};
        var traverseHeight = -20;
        var step = 5.709;

        moveQ.push(new qE("gc","G0Z"+traverseHeight));
        moveQ.push(new qE("gc","G0X"+(buildPlateOrigin.x)+"Y"+buildPlateOrigin.y));
        moveQ.push(new qE("gc","G0Z"+buildPlateOrigin.z));
        // moveQ.push(new qE("gc","M8")); //activate gripper
        moveQ.push(new qE("func",activateGripperCommand));

        
        for (var ntimes = 0; ntimes < 10; ntimes++) {
            var nextLocationX = Math.floor(Math.random()*4);
            var nextLocationY = Math.floor(Math.random()*4);
            monitor.log((buildPlateOrigin.x-(step*nextLocationX)));
            monitor.log(nextLocationY);
            moveQ.push(new qE("gc","G0Z"+traverseHeight));
            moveQ.push(new qE("gc","G0X"+(buildPlateOrigin.x-(step*nextLocationX))+"Y"+(buildPlateOrigin.y+(step*nextLocationY))));
            moveQ.push(new qE("gc","G0Z"+buildPlateOrigin.z));
            // moveQ.push(new qE("gc","M9")); //deactivate gripper
            moveQ.push(new qE("func",deactivateGripperCommand));
            moveQ.push(new qE("gc","G0Z"+traverseHeight));
            // moveQ.push(new qE("gc","G0X"+(buildPlateOrigin.x-(step*nextLocationX))+"Y"+(buildPlateOrigin.y+(step*nextLocationY))));
            moveQ.push(new qE("gc","G0Z"+buildPlateOrigin.z));
            // moveQ.push(new qE("gc","M8")); //activate gripper
            moveQ.push(new qE("func",activateGripperCommand));
        }

        moveQ.push(new qE("gc","G0Z"+traverseHeight));
        moveQ.push(new qE("gc","G0X"+(buildPlateOrigin.x)+"Y"+buildPlateOrigin.y));
        moveQ.push(new qE("gc","G0Z"+buildPlateOrigin.z));
        moveQ.push(new qE("func",deactivateGripperCommand));
        moveQ.push(new qE("gc","G0Z"+traverseHeight));
        
    }

    // function pickup(gotoPrevPosition=true) {
    //     moveQ.flush();
    //     turnCVOff();
    //     // var prevPosition = machineCoords;
    //     // check camera
    //     var camera_origin = [0,0]
    //     var blcomp = 1;
    //     moveQ.push(new qE("gc","G0Z0"));
    //     moveQ.push(new qE("gc","G0X"+(camera_origin[0]-blcomp)+"Y"+(camera_origin[1]-blcomp)));
    //     moveQ.push(new qE("gc","G0X"+camera_origin[0]+"Y"+camera_origin[1]));
    //     moveQ.push(new qE("func",turnCVOn));
    //     moveQ.push(new qE("func",pause));
    //     moveQ.push(new qE("func",turnCVOff));

    //     // move tool to feeder
    //     var tool_origin = [-40.5, -4.1];
    //     var tool_safe_z = -7;
    //     var tool_pickup_z = -12.5;
    //     moveQ.push(new qE("gc","G0X"+(tool_origin[0]-blcomp)+"Y"+(tool_origin[1]-blcomp)));
    //     moveQ.push(new qE("gc","G0X"+tool_origin[0]+"Y"+tool_origin[1]));
    //     moveQ.push(new qE("gc","G0Z"+tool_safe_z));
    //     moveQ.push(new qE("func",pause));
    //     moveQ.push(new qE("gc","G0Z"+tool_pickup_z));
    //     moveQ.push(new qE("gc","G0Z0"));
    //     if (gotoPrevPosition) {
    //         moveQ.push(new qE("gc","G0X"+machineCoords.x+"Y"+machineCoords.y));
    //         moveQ.push(new qE("gc","G0Z"+machineCoords.z));
    //     }
    // }

    function removeTool(toolNum=1,gotoPrevPosition=false) {
        var toolPos = offsets.tool1;
        var x_approach = -20;
        var z_retract = 20;

        // moveQ.push(new qE("func",unreadyGripperCommand));
        unreadyGripperCommand();
        moveQ.push(new qE("gc","G0Z"+(offsets.zMax)));
        moveQ.push(new qE("gc","G0X"+(toolPos[0]-x_approach)+"Y"+(toolPos[1]-(toolNum-1)*25)+"A"+toolPos[3]));
        moveQ.push(new qE("gc","G0Z"+toolPos[2]));
        // moveQ.push(new qE("func",pause));
        moveQ.push(new qE("gc","G0X"+toolPos[0]));
        moveQ.push(new qE("gc","G0Z"+offsets.zMax));
        // moveQ.push(new qE("func",readyGripperCommand));
        // readyGripperCommand();

        if (gotoPrevPosition) {
            moveQ.push(new qE("gc","G0X"+machineCoords.x+"Y"+machineCoords.y+"A"+machineCoords.a));
            moveQ.push(new qE("gc","G0Z"+machineCoords.z));
        }
    }

    function pickupTool(toolNum=1,gotoPrevPosition=false) {
        // var toolPos = [0,0,0];
        var toolPos = offsets.tool1;
        var x_approach = -20;
        var z_retract = 20;

        // moveQ.push(new qE("func",unreadyGripperCommand));
        unreadyGripperCommand();
        moveQ.push(new qE("gc","G0Z"+offsets.zMax));
        moveQ.push(new qE("gc","G0X"+toolPos[0]+"Y"+(toolPos[1]-(toolNum-1)*25)+"A"+toolPos[3]));
        moveQ.push(new qE("gc","G0Z"+toolPos[2]));
        // moveQ.push(new qE("func",pause));
        moveQ.push(new qE("gc","G0X"+(toolPos[0]-x_approach)));
        moveQ.push(new qE("gc","G0Z"+offsets.zMax));
        // moveQ.push(new qE("func",unreadyGripperCommand));
        readyGripperCommand();

        if (gotoPrevPosition) {
            moveQ.push(new qE("gc","G0X"+machineCoords.x+"Y"+machineCoords.y));
            moveQ.push(new qE("gc","G0Z"+machineCoords.z));
        }
    }

    function setXYOriginHere() {
        //node loc (2,2)
        var x = offsets.nodePickupOrigin[0];+offsets.cameraFromToolhead[0];
        var y = offsets.nodePickupOrigin[1];+offsets.cameraFromToolhead[1];

        if (cameraView) {
            x += offsets.cameraFromToolhead[0];
            y += offsets.cameraFromToolhead[1];
        }
        // monitor.sendJSONmsg("G92X" + x + "Y" + y);
        moveQ.push(new qE("gc","G92X"+x+"Y"+y));
    }

    function pickupNextNode() {

    }

    function readyGripper() {
        // moveQ.push(new qE("gc","M4"));
        // moveQ.next();
        monitor.sendJSONmsg("M9");
    }

    function unreadyGripper() {
        // moveQ.push(new qE("gc","M3"));
        // moveQ.next();
        monitor.sendJSONmsg("M8");
    }

    function readyGripperCommand() {
        // moveQ.push(new qE("gc","M4"));
        // moveQ.next();
        moveQ.push(new qE("gc","M9"));
    }

    function unreadyGripperCommand() {
        // moveQ.push(new qE("gc","M3"));
        // moveQ.next();
        moveQ.push(new qE("gc","M8"));
    }

    // function pulseFeeders() {
    //     monitor.sendJSONmsg("M4");
    //     setTimeout(function() {
    //         monitor.sendJSONmsg("M3");
    //     }, 10);
    // }

    function activateGripper(){
        monitor.sendJSONmsg("M3");
    }

    function halfGrip(){
        monitor.sendJSONmsg("M4");
    }

    function deactivateGripper(){
        monitor.sendJSONmsg("M5");
    }

    toggleCameraToolhead=function() {
        // var relativeMove;
        // if (cameraView == false) {
        //     // switch to toolhead view
        //     relativeMove = "G0X" + offsets.cameraFromToolhead[0] + "Y" + offsets.camera[1];
        //     cameraView = true;
        // } else {
        //     relativeMove = "G0X" + (-offsets.camera[0]) + "Y" + (-offsets.camera[1]);
        //     cameraView = false;
        // }

        // // monitor.sendJSONmsg(relativeMove);
        // moveQ.push(new qE("gc","G91"));
        // moveQ.push(new qE("gc",relativeMove));
        // moveQ.push(new qE("gc","G90"));

        if (cameraView == false) {
            relativeMove2("X"+offsets.cameraFromToolhead[0]+"Y"+offsets.cameraFromToolhead[1]);
            cameraView = true;
        } else {
            relativeMove2("X"+(-offsets.cameraFromToolhead[0])+"Y"+(-offsets.cameraFromToolhead[1]));
            cameraView = false;
        }

        toggleCameraMode();
        
    }

    function toggleCameraMode() {
        console.log(coordsysToolselect)
        if (cameraView) {
            coordsysToolselect.setAttribute('style','background-color: darkorange');
            coordsysToolselect.selectedIndex = 1;
        } else {
            coordsysToolselect.setAttribute('style','background-color: white');
            coordsysToolselect.selectedIndex = 0;
        }
    }

    function toggleCameraToolheadCommand() {
        var relativeMove = "G0X" + offsets.cameraFromToolhead[0] + "Y" + offsets.cameraFromToolhead[1];
        if (cameraView == false) {
            moveQ.push(new qE("gc","G91"));
            moveQ.push(new qE("gc",relativeMove));
            moveQ.push(new qE("gc","G90"));
            cameraView = true;
        } else {
            moveQ.push(new qE("gc","G91"));
            moveQ.push(new qE("gc",relativeMove));
            moveQ.push(new qE("gc","G90"));
            cameraView = false;
        }
    }


    function goToBuildLoc(lattice_x=0,lattice_y=0) {
        var step = offsets.pitch;

        var x = offsets.buildPlateOrigin[0]+(step*lattice_x);
        var y = offsets.buildPlateOrigin[1]+(step*lattice_y);

        coord = applyRotationMatrix([x,y])

        // moveQ.push(new qE("gc","G0Z"+offsets.zTraverse));
        moveQ.push(new qE("gc","G0X"+coord[0].toFixed(3)+"Y"+coord[1].toFixed(3)));
    }

    function goToNodeLoc(lattice_x=0,lattice_y=0) {
        var step = offsets.pitch;

        var x = offsets.nodePickupOrigin[0]+(step*(lattice_x-2));
        var y = offsets.nodePickupOrigin[1]+(step*(lattice_y-2));

        coord = applyRotationMatrix([x,y])

        // moveQ.push(new qE("gc","G0Z"+offsets.zTraverse));
        moveQ.push(new qE("gc","G0X"+coord[0].toFixed(3)+"Y"+coord[1].toFixed(3)));
    }

    function goToStrutLoc(lattice_x=0,lattice_y=0,angle=0) {
        var step = offsets.pitch;

        var x = offsets.strutPickupOrigin[0]+(step*(lattice_x-2));
        var y = offsets.strutPickupOrigin[1]+(step*(lattice_y-2));

        coord = applyRotationMatrix([x,y])

        // moveQ.push(new qE("gc","G0Z"+offsets.zTraverse));
        moveQ.push(new qE("gc","G0X"+coord[0].toFixed(3)+"Y"+coord[1].toFixed(3)+"A"+angle));
    }

    function goToActuatorLoc(lattice_x=0,lattice_y=0,angle=0) {
        var step = offsets.pitch;

        var x = offsets.actuatorPickupOrigin[0]+(step*(lattice_x));
        var y = offsets.actuatorPickupOrigin[1]+(step*(lattice_y-4));

        coord = applyRotationMatrix([x,y])

        // moveQ.push(new qE("gc","G0Z"+offsets.zTraverse));
        moveQ.push(new qE("gc","G0X"+coord[0].toFixed(3)+"Y"+coord[1].toFixed(3)+"A"+angle));
    }

    function pickAndPlace() {
        moveQ.push(new qE("gc","G0Z"+offsets.zTraverse));
        for (var j = 0; j < 3; j++) {
        for (var i = 0; i < 4; i++) {
                goToNodeLoc(i+j*4,0);
                pickPart();
                // goToBuildLoc(i,j);
                goToNodeLoc(4+i,4+j)
                placePart();
            } 
        }
        
        
    }

    pickDice=function() {
        var pickHeight = -37;
        moveQ.push(new qE("gc","G0Z"+(pickHeight)));
        moveQ.push(new qE("gc","G0Z-28"));
    }

    placeDice=function()  {
        var placeHeight = -34;
        moveQ.push(new qE("gc","G0Z"+(placeHeight)));
        // moveQ.push(new qE("gc","M3"));
        moveQ.push(new qE("func",activateGripperCommand));	
        moveQ.push(new qE("gc","G0Z-28"));
        // moveQ.push(new qE("gc","M5"));		
        moveQ.push(new qE("func",deactivateGripperCommand));	

    }

    diceDemo=function()  {

        moveQ.push(new qE("gc","G0X-151.1Y-43.5"));
        pickDice();
        moveQ.push(new qE("gc","G0X-139.7Y-67.2"));
        placeDice();

        moveQ.push(new qE("gc","G0X-138.1Y-43.5"));
        pickDice();
        moveQ.push(new qE("gc","G0X-130.7Y-67.2"));
        placeDice();

        moveQ.push(new qE("gc","G0X-151.1Y-35.5"));
        pickDice();
        moveQ.push(new qE("gc","G0X-135.2Y-71.7"));
        placeDice();

        moveQ.push(new qE("gc","G0X-138.1Y-35.5"));
        pickDice();
        moveQ.push(new qE("gc","G0X-126.2Y-71.7"));
        placeDice();

        moveQ.push(new qE("gc","G0X-130Y-55Z-29"));
    }


    demoPickPlaceDice=function()  {
        
    
    

        var nParts = placeList.length;

        moveQ.push(new qE("func",deactivateGripperCommand));
        moveQ.push(new qE("gc","G0Z"+traverseHeight));

        for (var i=0; i < nParts; i++) {
            //pick
            moveQ.push(new qE("gc","G0X"+(pickupOrigin.x+pickupList[i].x*pickStep.x)+"Y"+(pickupOrigin.y+pickupList[i].y*pickStep.y)+"A"+(aOffset)));
            // moveQ.push(new qE("func",wait));
            // moveQ.push(new qE("func",toggleCameraToolhead));
            moveQ.push(new qE("gc","G0Z"+pickHeight));
            setTimeout(function(){ 
                callback.call(context);
            }, 3000);
            moveQ.push(new qE("gc","G0Z"+traverseHeight));
            // moveQ.push(new qE("func",toggleCameraToolhead);)

            //place
            moveQ.push(new qE("gc","G0X"+(placeOrigin.x+placeList[i].x*placeStep.x)+"Y"+(placeOrigin.y-placeList[i].y*placeStep.y)+"A"+(aOffset+placeList[i].a)));
            // moveQ.push(new qE("func",wait));
            // moveQ.push(new qE("func",toggleCameraToolhead));
            moveQ.push(new qE("gc","G0Z"+(placeHeight+placeList[i].z*placeStep.z)));
            moveQ.push(new qE("func",activateGripperCommand));
            moveQ.push(new qE("gc","G0Z"+traverseHeight));
            moveQ.push(new qE("func",deactivateGripperCommand));
            // moveQ.push(new qE("func",toggleCameraToolhead));

        }
        // home
        moveQ.push(new qE("gc","G0X-130Y-55Z-29"));
    }




    function pickPart(actuator=false,dryRun=false) {
        var pickHeight;
        if (actuator) {
            pickHeight = offsets.actuatorPickupOrigin[2];
        } else {
            pickHeight = offsets.zMin;
        }
        if (dryRun) {
            moveQ.push(new qE("gc","G0Z"+(pickHeight+offsets.zApproachDistance)));
        } else {
            moveQ.push(new qE("gc","G0Z"+pickHeight));
        }

        // if (actuator) {
        //     moveQ.push(new qE("func",activateHalfGripperCommand));
        // } else {
        //     moveQ.push(new qE("func",activateGripperCommand));   
        // }

        moveQ.push(new qE("func",activateGripperCommand));
        
        moveQ.push(new qE("gc","G0Z"+offsets.zTraverse));
    }

    function placePart(actuator=false,dryRun=false) {
        var placeHeight = offsets.zBuildLayer;

        if (actuator) {
            placeHeight = offsets.zBuildLayer - (offsets.nodePickupOrigin[2]-offsets.actuatorPickupOrigin[2])
        }

        if (dryRun) {
            moveQ.push(new qE("gc","G0Z"+(placeHeight+offsets.zApproachDistance)));
        } else {
            moveQ.push(new qE("gc","G0Z"+(placeHeight-offsets.zPreloadOffset)));
        }
        moveQ.push(new qE("func",deactivateGripperCommand));
        moveQ.push(new qE("gc","G0Z"+offsets.zTraverse));
    }

    function placeNodeArray(n,m,start_x=4,start_y=4) {
        for (var j = 0; j < m; j++) {
        for (var i = 0; i < n; i++) {
                goToNodeLoc(nextNode,0);
                pickPart();
                goToNodeLoc(start_x+i,start_y+j)
                placePart();
                incrementNodeCount();
            } 
        }
    }

    function placeStrutArray(n,m,start_x=4,start_y=4) {
        for (var j = 0; j < m; j++) {
        for (var i = 0; i < (n-1); i++) {
                goToStrutLoc(nextStrut,1,0);
                pickPart();
                goToStrutLoc(start_x+i+0.5,start_y+j,0)
                placePart();
                incrementStrutCount();
            } 
        }

        for (var j = 0; j < (m-1); j++) {
        for (var i = 0; i < n; i++) {
                goToStrutLoc(nextStrut,1,0);
                pickPart();
                goToStrutLoc(start_x+i,start_y+j+0.5,90)
                placePart();
                incrementStrutCount();
            } 
        }
    }

    function placeNextNodeAt(x,y) {
        if (nextNode < 12) {
            goToNodeLoc(nextNode,0);
        } else {
            goToNodeLoc(nextNode-12,11);
        }
        pickPart();
        goToNodeLoc(x,y);
        placePart();
        incrementNodeCount();
    }

    function placeNextStrutAt(x,y,angle) {
        goToStrutLoc(nextStrut,1);
        pickPart();
        goToStrutLoc(x,y,angle);
        placePart();
        incrementStrutCount();
    }

    function placeWalkingMotorNodes() {
        placeNextNodeAt(4,4);
        placeNextNodeAt(6,4);
        placeNextNodeAt(7,4);
        placeNextNodeAt(4,5);
        placeNextNodeAt(5,5);
        placeNextNodeAt(6,5);
        placeNextNodeAt(7,5);
        placeNextNodeAt(6,6);
        placeNextNodeAt(6,7);
        placeNextNodeAt(7,7);
    }

    function placeWalkingMotorActuators() {
        // goToActuatorLoc(0,4,90);
        moveQ.push(new qE("gc","G0X-150.302Y-57.531A90"));
        pickPart(true);
        // moveQ.push(new qE("gc","M4"));
        // goToActuatorLoc(5,4,180);
        moveQ.push(new qE("gc","G0X-121.855Y-57.799A180"));
        placePart(true);
        // goToActuatorLoc(0,7,90);
        moveQ.push(new qE("gc","G0X-150.302Y-40.704A90"));
        pickPart(true);
        moveQ.push(new qE("gc","M4"));
        // goToActuatorLoc(7,6,270);
        moveQ.push(new qE("gc","G0X-110.755Y-46.236A270"));
        placePart(true);
    }

    function placeWalkingMotorStruts() {
        incrementLayerHeight();
        // placeNextStrutAt(4,4.5,90);
        // placeNextStrutAt(4.5,5,0);
        // placeNextStrutAt(5.5,5,0);
        placeNextStrutAt(6,5.5,90);
        placeNextStrutAt(6,6.5,90);
        placeNextStrutAt(6.5,7,0);
    }

    function placeWalkingMotorFlexures() {
        incrementLayerHeight();
        goToStrutLoc(11,1,0);
        pickPart();
        goToStrutLoc(6.5,4,0);
        placePart();

        goToStrutLoc(10,1,0);
        pickPart();
        goToStrutLoc(6.5,5,0);
        placePart();

        goToStrutLoc(9,1,0);
        pickPart();
        goToStrutLoc(6,4.5,90);
        placePart();

        goToStrutLoc(8,1,0);
        pickPart();
        goToStrutLoc(7,4.5,90);
        placePart();
    }

    function incrementLayerHeight() {
        offsets.zBuildLayer += offsets.layerHeight;
        zBuildLayer.value = offsets.zBuildLayer;
    }

    function decrementLayerHeight() {
        offsets.zBuildLayer -= offsets.layerHeight;
        if (offsets.zBuildLayer < offsets.zMin) {
            offsets.zBuildLayer = offsets.zMin;
        }
        zBuildLayer.value = offsets.zBuildLayer;
    }

    function setLayerHeight(height) {
        offsets.zBuildLayer = height;
        zBuildLayer.value = offsets.zBuildLayer;
    }

    function incrementNodeCount() {
        nextNode++;
        nodeCountDisplay.value = nextNode;
    }

    function decrementNodeCount() {
        nextNode--;
        nodeCountDisplay.value = nextNode;
    }

    function incrementStrutCount() {
        nextStrut++;
        strutCountDisplay.value = nextStrut;
    }

    function decrementStrutCount() {
        nextStrut--;
        strutCountDisplay.value = nextStrut;
    }

    function applyRotationMatrix(coord) {
        // [[ 1.00359082 -0.00240222  0.        ]
        //  [ 0.00359082  0.99759778  0.        ]
        //  [ 0.76779344 -0.49709526  1.        ]]

        // defining nodeLoc(2,2) as the origin
        // (X,Y) = (-139.307, -68.457)

        var a = 1.00359082;
        var b = 0.00359082;
        var c = -0.00240222;
        var d = 0.99759778;
        var e = 0.76779344;
        var f = -0.49709526;

        monitor.log("input coord:" + coord[0] + ", " + coord[1])

        var x = coord[0];
        var y = coord[1];

        var output = [a*x+b*y+e, c*x+d*y+f];
        // var output = coord;

        if (cameraView) {
            output[0] += offsets.cameraFromToolhead[0];
            output[1] += offsets.cameraFromToolhead[1];
        }

        monitor.log("output coord:" + output[0] + ", " + output[1])

        return output;

    }

    // function transformMap(x,y) {
    //     let src = cv.imread(mapImage);
    //     let dst = new cv.Mat();
    //     let M = cv.matFromArray(2, 3, cv.CV_64FC1, [1, 0, 50, 0, 1, 100]);
    //     let dsize = new cv.Size(src.rows, src.cols);
    //     // You can try more different parameters
    //     cv.warpAffine(src, dst, M, dsize, cv.INTER_LINEAR, cv.BORDER_CONSTANT, new cv.Scalar());
    //     cv.imshow('mapInput', dst);
    //     src.delete(); dst.delete(); M.delete();
    // }

    // function circles() {
    //     var spacing = 10;
    //     var x_range = 70;
    //     var y_range = 50;
    //     var cw = false;
    //     var backlashOffset = 5;
    //     // start centered on fiducial
    //     // (at 0,0)
    //     for (var ypos=0; ypos <= y_range; ypos+=spacing) {
    //         moveQ.push(new qE("gc","G0X"+(-backlashOffset)+"Y"+ypos));
    //         for (var xpos=0; xpos <= x_range; xpos+=spacing) {
    //             moveQ.push(new qE("gc","G0X"+xpos+"Y"+ypos));
    //             moveQ.push(new qE("func",measureFiducial));
    //         }
    //     }
    // }

    // function populateFeederOptions() {
    //     for (var i=0; i < feeders.length; i++) {
    //         var nodeOption = document.createElement("option");
    //         nodeOption.text = "Node";
    //         var strutOption = document.createElement("option");
    //         strutOption.text = "Strut";
    //         var oneDoFOption = document.createElement("option");
    //         oneDoFOption.text = "1-DoF";
    //         var twoDoFOption = document.createElement("option");
    //         twoDoFOption.text = "2-DoF";
    //         var actuatorOption = document.createElement("option");
    //         actuatorOption.text = "Actuator";

    //         if (feeders[i] != null) {
    //             feeders[i].add(nodeOption);
    //             feeders[i].add(strutOption);
    //             feeders[i].add(oneDoFOption);
    //             feeders[i].add(twoDoFOption);
    //             feeders[i].add(actuatorOption);
    //         }
    //     }
    // }





    console.log($( '.dropdown-menu a' ))

    $('#dropdown-button').on('click', function(evt) {
        // console.log(evt)
        if ($('.dropdown-menu').is(":visible")) { $('.dropdown-menu').hide() }
        else { $('.dropdown-menu').show(); }
    })

    $( '.dropdown-menu a' ).on( 'click', function( event ) {
        // console.log($( '.dropdown-menu a' ))

    var $target = $( event.currentTarget ),
        val = $target.attr( 'data-value' ),
        $inp = $target.find( 'input' ),
        idx;

    if ( ( idx = visionLayers.indexOf( val ) ) > -1 ) {
        visionLayers.splice( idx, 1 );
        setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
    } else {
        visionLayers.push( val );
        setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
    }

    $( event.target ).blur();
        
    console.log( visionLayers );
    return false;
    });

// }











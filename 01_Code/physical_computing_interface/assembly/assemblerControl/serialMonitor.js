var serialMonitor = $("#serialMonitor");
var serialInput = $("#serialInput");
var monitor = new SerialMonitor(currentPort, serialMonitor, serialInput);

// trigger a command send on "enter" in text box
serialInput.keydown(function(e) {
    if (e.keyCode == 13) {
        $("#sendButton").click();
    }
})  

// trigger a command send
$("#sendButton").click(function() {
    monitor.sendInput();
});

function SerialMonitor(port, scroll, input) {
    this.port = port;
    this.window = scroll;
    this.input = input;

    return this;
}

SerialMonitor.prototype.setPort = function(port,verbose=true) {
    if (verbose) {
        if (port != null) {
            console.log(port);
            monitor.log("set port to " + port);
        } else {
            console.log("no current port");
            monitor.warn("no current port");
        }
    }
    if (port != null) this.port = port;
    else this.port = null;
    
}

SerialMonitor.prototype.log = function(msg,type="log") {
    var coloredMsg;
    if (type == "log") {
        coloredMsg = msg+"\n";
    } else if (type == "warn") {
        coloredMsg = "<span style='color:orange;'>"+msg+"<span>\n";
    }
    else if (type == "err") {
        coloredMsg = "<span style='color:red;'>"+msg+"<span>\n";
    }
    this.window.html(serialMonitor.html()+coloredMsg);
    this.window[0].scrollTop = this.window[0].scrollHeight;
}

SerialMonitor.prototype.warn = function(msg) {
    this.log(msg,"warn");
}

SerialMonitor.prototype.err = function(msg) {
    this.log(msg,"err");
}

SerialMonitor.prototype.sendInput = function() {
    var text = this.input.val();
    if (text.indexOf("@") == 0) {
        var func = text.split('@')[1].trim();
        if (func.indexOf("(") >= 0 & func.indexOf(")") >= 0) {
            var arg = func.split("(")[1].split(")")[0].replace(/['"]+/g, '');
            var args = arg.split(",");
            // console.log(arg);
            func = func.split("(")[0]
        }
        monitor.log("running function: " + func + "() with args: " + args);
        // window[func](arg);
        // var args = []; args.push(arg);
        window[func].apply(null,args);
        this.input.val("");
    } else {
        console.log("sending: " + text);
        monitor.log("sending: " + text);
        socket_send(this.port,text)
        this.input.val("");
    }
   
}

SerialMonitor.prototype.sendJSONmsg = function(msg,dummy=false) {
    if (dummy) {
        console.log("sending: " + msg);
        monitor.log("sending: " + msg);
        dummyTinygResponse();
    } else {
        console.log("sending: " + msg);
        monitor.log("sending: " + msg);
        socket_send(this.port,msg,true);
    }
    
}

function sendRaw(msg) {
    console.log("sending: " + msg);
    monitor.log("sending: " + msg);
    socket.send(msg); 
    // socket.send('{"cmd":"broadcast","msg":"h"}');   
    
}


// opencv documentation: https://docs.opencv.org/2.4/modules/core/doc/core.html
function precise(x,places=2) {
  return Math.round(x*Math.pow(10,places))/Math.pow(10,places);
}

var center_x, center_y;

let video_input = document.getElementById("videoInput");
let cap;    // capture object
let src;    // source
let dst;    // destination
let gray;   // grayscale
let gb;     // blurred
let thres;  // threshold
let dsp;    // output display
let ksize;  // GB kernel size
let dsize;  // warp affine size
let color;  
let contours;
let hierarchy;
let openCloseMat // erode/dilate kernel
let ellipse;

// crosshair points
let startPoint;
let startPoint2; 
let endPoint; 
let endPoint2; 

var hasLoaded = false;

var scale = 21.473473933293278; //pixel/mm
var actual_width = video_input.width/scale;
var actual_height = video_input.height/scale;


function cvHasLoaded() {
    hasLoded = true;
    cap = new cv.VideoCapture(video_input);
    src = new cv.Mat(video_input.height, video_input.width, cv.CV_8UC4);
    dst = new cv.Mat(video_input.height, video_input.width, cv.CV_8UC4);
    gray = new cv.Mat(video_input.height, video_input.width, cv.CV_8UC1);
    gb = new cv.Mat(video_input.height, video_input.width, cv.CV_8UC1);
    thres = new cv.Mat(video_input.height, video_input.width, cv.CV_8UC1);
    dsp = new cv.Mat(video_input.height, video_input.width, cv.CV_8UC4);
    ksize = new cv.Size(3, 3);
    color = new cv.Scalar(255,0,0,255);
    contours = new cv.MatVector();
    hierarchy = new cv.Mat();
    openCloseMat = cv.Mat.ones(15, 15, cv.CV_8U);
}

function changeCVDisplay(mode) {
    visionDisplay = mode;
    visionSelect.value = visionDisplay;
}

function turnCVOff(context,callback) {
    visionDisplay = "None";
    visionSelect.value = visionDisplay;
    if (callback != undefined) callback.call(context);
}

function turnCVOn(context,callback) {
    visionDisplay = "Color";
    visionSelect.value = visionDisplay;
    if (callback != undefined) callback.call(context);
}


function processVideo() {
    
    try {
        if (!streaming) {
            // clean and stop.
            try {
                src.delete();
                dsp.delete();
                dst.delete();
                contours.delete();
                hierarchy.delete();
                gray.delete();
                gb.delete();
                thres.delete();
            } catch(e) {
                console.warn("error deleting")
                console.log(e)
            }
            return;
        }
        let begin = Date.now();
        // start processing.

        cap.read(src); // Read the video input from the camera. This has been translated to the cv.Mat format before
        
        //ROTATE 180
        // dsize = new cv.Size(src.rows, src.cols);
        // let center = new cv.Point(src.cols / 2, src.rows / 2);
        // let M2 = cv.getRotationMatrix2D(center, 180, 1);
        // cv.warpAffine(src, gray, M2, dsize, cv.INTER_LINEAR, cv.BORDER_CONSTANT, new cv.Scalar());

        if (visionDisplay != "None") {

            cv.cvtColor(src, gray, cv.COLOR_RGBA2GRAY); // Transform to grayscale using the CVT function
            cv.GaussianBlur(gray, gb, ksize, 0, 0, cv.BORDER_DEFAULT);
            cv.threshold(gb, thres, 200, 255,  cv.THRESH_BINARY_INV + cv.THRESH_OTSU);
            cv.morphologyEx(thres, dst, cv.MORPH_CLOSE, openCloseMat);
            cv.morphologyEx(dst, dst, cv.MORPH_OPEN, openCloseMat);

            switch(visionDisplay) {
                case "Gray":
                    dsp = gray;
                    break;
                case "Thresh":
                    dsp = thres;
                    break;
                case "Color":
                    dsp = src;
                    break;
                case "Blur":
                    dsp = gb;
                    break;
            }

            if (visionLayers.indexOf("contours") >= 0) {
                cv.findContours(dst, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);
                cv.drawContours(dsp, contours, 0, color, 1, cv.LINE_8, hierarchy, 100);
    
                try { 
                    ellipse = cv.fitEllipse(contours.get(0));
                    center_x = ellipse.center.x/video_input.width*actual_width-actual_width/2;
                    center_y = ellipse.center.y/video_input.height*actual_height-actual_height/2;
                    cv.putText(dsp,precise(center_x)+ ", " + precise(center_y),{x:10,y:video_input.height-10}, cv.FONT_HERSHEY_SIMPLEX, 0.8, color, 2);
                } catch (e) { 
                    // console.log(e) 
                }
            }

            if (visionLayers.indexOf("crosshair") >= 0) {
                startPoint = {x: video_input.width/2, y: 0};
                startPoint2 = {x: 0, y: video_input.height/2};
                endPoint = {x: video_input.width/2, y: video_input.height};
                endPoint2 = {x: video_input.width, y: video_input.height/2};
                cv.line(dsp, startPoint, endPoint, [255, 0, 0, 255], 1, cv.LINE_AA)// lineType = cv.LINE_4);
                cv.line(dsp, startPoint2, endPoint2, [255, 0, 0, 255], 1, cv.LINE_AA)//, lineType = cv.LINE_4);
            }

            cv.imshow('canvasFrame',dsp);
        } else {
            cv.imshow('canvasFrame',src);
        }

        let delay = 1000 / FPS - (Date.now() - begin);
        setTimeout(processVideo, delay);         // schedule the next one.

    } catch (err) {
        utils.printError(err);
        // monitor.err(err);
    }

}

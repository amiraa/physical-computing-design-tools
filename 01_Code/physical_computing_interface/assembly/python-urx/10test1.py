# wake-up meso-dice node placement tool using hardcoded joint-space coordinates
# for pickup location and four grid spots. stacks nodes 2x1x2, giving the user
# a bit of time to manually place the seven sruts between the two layers. ur10
# connected via wifi/VPN at static IP 192.168.1.52 (Roy). code needs a refactor.

import urx
import time

loc_pickup_j_10mm = (0.043008387088775635, -1.296274487172262, 2.373403787612915, -1.3105805555926722, 0.04273087903857231, -1.3469165007220667)

loc_grid_3_0_50mm = (0.02622373215854168, -1.2583287397967737, 2.196824550628662, -1.2858575026141565, 0.026594867929816246, -1.2329323927508753)
loc_grid_2_0_50mm = (0.02478637360036373, -1.2227914969073694, 2.1424121856689453, -1.2808602491961878, 0.02525300905108452, -1.218954865132467)

rob = urx.Robot("192.168.1.52")
rob.set_tcp((0, 0, 0.1, 0, 0, 0))
rob.set_payload(2, (0, 0, 0.1))
time.sleep(0.2)  #leave some time to robot to process the setup commands

v = 1
a = 1

rob.movej(loc_pickup_j_10mm, acc = a, vel = v)
rob.send_program('set_tool_digital_out(0, False)') #open tool
time.sleep(0.2)
rob.movel((0,0,-0.01,0,0,0), acc = a, vel = v, relative = True)
rob.send_program('set_tool_digital_out(0, True)') #close tool
time.sleep(0.2)
rob.movel((0,0,0.01,0,0,0), acc = a, vel = v, relative = True)
rob.movej(loc_grid_3_0_50mm, acc = a, vel = v)
rob.movel((0,0,-0.05,0,0,0), acc = a, vel = v, relative = True)
rob.send_program('set_tool_digital_out(0, False)') #open tool
time.sleep(0.2)
rob.movel((0,0,0.05,0,0,0), acc = a, vel = v, relative = True)

rob.movej(loc_pickup_j_10mm, acc = a, vel = v)
rob.send_program('set_tool_digital_out(0, False)') #open tool
time.sleep(0.2)
rob.movel((0,0,-0.01,0,0,0), acc = a, vel = v, relative = True)
rob.send_program('set_tool_digital_out(0, True)') #close tool
time.sleep(0.2)
rob.movel((0,0,0.01,0,0,0), acc = a, vel = v, relative = True)
rob.movej(loc_grid_2_0_50mm, acc = a, vel = v)
rob.movel((0,0,-0.05,0,0,0), acc = a, vel = v, relative = True)
rob.send_program('set_tool_digital_out(0, False)') #open tool
time.sleep(0.2)
rob.movel((0,0,0.05,0,0,0), acc = a, vel = v, relative = True)

time.sleep(20)

rob.movej(loc_pickup_j_10mm, acc = a, vel = v)
rob.send_program('set_tool_digital_out(0, False)') #open tool
time.sleep(0.2)
rob.movel((0,0,-0.01,0,0,0), acc = a, vel = v, relative = True)
rob.send_program('set_tool_digital_out(0, True)') #close tool
time.sleep(0.2)
rob.movel((0,0,0.01,0,0,0), acc = a, vel = v, relative = True)
rob.movej(loc_grid_3_0_50mm, acc = a, vel = v)
rob.movel((0,0,-0.043,0,0,0), acc = a, vel = v, relative = True)
rob.send_program('set_tool_digital_out(0, False)') #open tool
time.sleep(0.2)
rob.movel((0,0,0.043,0,0,0), acc = a, vel = v, relative = True)

rob.movej(loc_pickup_j_10mm, acc = a, vel = v)
rob.send_program('set_tool_digital_out(0, False)') #open tool
time.sleep(0.2)
rob.movel((0,0,-0.01,0,0,0), acc = a, vel = v, relative = True)
rob.send_program('set_tool_digital_out(0, True)') #close tool
time.sleep(0.2)
rob.movel((0,0,0.01,0,0,0), acc = a, vel = v, relative = True)
rob.movej(loc_grid_2_0_50mm, acc = a, vel = v)
rob.movel((0,0,-0.043,0,0,0), acc = a, vel = v, relative = True)
rob.send_program('set_tool_digital_out(0, False)') #open tool
time.sleep(0.2)
rob.movel((0,0,0.043,0,0,0), acc = a, vel = v, relative = True)

print ("Current pose: ", rob.getj())

import sys
import urx
from urx.robotiq_two_finger_gripper import Robotiq_Two_Finger_Gripper
import time
import math3d as m3d


if __name__ == '__main__':
    robot = urx.Robot("169.254.241.97")
    robotiqgrip = Robotiq_Two_Finger_Gripper(robot,socket_host="169.254.241.97")




    mytcp = m3d.Transform()  # create a matrix for our tool tcp
    mytcp.pos.z = 0.18 
    robot.set_tcp(mytcp) # set tcp


    trans = robot.get_pose()  # get current transformation matrix (tool to base)
    print(trans)
    time.sleep(0.1)

    # robotiqgrip.close_gripper()
    # time.sleep(0.1)

    #################


    robotiqgrip.open_gripper()
    time.sleep(0.1)

    trans.pos.z += 0.2
    robot.set_pose(trans, acc=0.1, vel=0.1)  # got up
    time.sleep(0.1)

    trans.pos.x += 0.2
    robot.set_pose(trans, acc=0.1, vel=0.1)  # go left
    time.sleep(0.1)

    trans.pos.z -= 0.2
    robot.set_pose(trans, acc=0.1, vel=0.1)  # got down
    time.sleep(0.1)

    robotiqgrip.close_gripper()
    time.sleep(0.1)

    #################

    robotiqgrip.open_gripper()
    time.sleep(0.1)

    trans.pos.z += 0.2
    robot.set_pose(trans, acc=0.1, vel=0.1)  # got up
    time.sleep(0.1)

    trans.pos.x -= 0.2
    robot.set_pose(trans, acc=0.1, vel=0.1)  # got right
    time.sleep(0.1)

    trans.pos.z -= 0.2
    robot.set_pose(trans, acc=0.1, vel=0.1)  # got up
    time.sleep(0.1)

    robotiqgrip.close_gripper()
    time.sleep(0.1)

    
    # robotiqgrip.open_gripper()


    # print(len(sys.argv))
    # print(sys.argv)
    
    # if(len(sys.argv) != 2):
    #     print ("falsse")
    #     sys.exit()
    
    # if(sys.argv[1] == "close") :
    #     print ("heree")
    #     robotiqgrip.close_gripper()
    
    # if(sys.argv[1] == "open") :
    #     robotiqgrip.open_gripper()
    
    # # rob.send_program(robotiqgrip.ret_program_to_run())
    
    # rob.close()
    # print ("true")
    # sys.exit()

    # rob = urx.Robot("192.168.0.100")
    # rob.set_tcp((0, 0, 0.1, 0, 0, 0))
    # rob.set_payload(2, (0, 0, 0.1))
    # time.sleep(0.2)  #leave some time to robot to process the setup commands
    # # rob.movej((1, 2, 3, 4, 5, 6), a, v)
    # # rob.movel((x, y, z, rx, ry, rz), a, v)
    # print ("Current tool pose is: ",  rob.getl())
    # rob.movel((0.1, 0, 0, 0, 0, 0), a, v, relative=true)  # move relative to current pose
    # rob.translate((0.1, 0, 0), a, v)  #move tool and keep orientation
    # rob.stopj(a)
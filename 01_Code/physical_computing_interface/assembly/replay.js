

//////////////////////occupancy//////////////////

// var timeStepOccupancy=[];
// timeStepOccupancy.push([occupancy.slice()]);

var slider = document.getElementById("time");

slider.oninput = function() {
    replay(this.value);
};

////////////////////////
document.addEventListener('addNode', function (e) { 
    
    if(!e.detail.replay){
        // console.log("Add:"+e.detail.x+" "+e.detail.y+" "+e.detail.z);
        GLOBALS.adjustTimeStep(true,e.detail.x,e.detail.y,e.detail.z);
    }else{
        // console.log("Replay Add:"+e.detail.x+" "+e.detail.y+" "+e.detail.z);
    }
    
    
}, false);

document.addEventListener('removeNode', function (e) { 
    if(!e.detail.replay){
        // console.log("Remove:"+e.detail.x+" "+e.detail.y+" "+e.detail.z);
        GLOBALS.adjustTimeStep(false,e.detail.x,e.detail.y,e.detail.z);
        
    }else{
        // console.log("Replay Remove:"+e.detail.x+" "+e.detail.y+" "+e.detail.z);
    }
}, false);

document.addEventListener('adjustTimeStep', function (e) { 
    GLOBALS.totalTimeSteps++;
    GLOBALS.occupancy[e.detail.x][e.detail.y][e.detail.z]=e.detail.addRemove;
    
    
    slider.max=GLOBALS.totalTimeSteps;
    slider.value=GLOBALS.totalTimeSteps;
    GLOBALS.timeline.push([e.detail]);
    replay(parseInt(GLOBALS.totalTimeSteps-1));
    GLOBALS.timeStep=parseInt(GLOBALS.totalTimeSteps);
    

}, false);

//todo enclose to prototype
function replay(currValue){
    
    if(currValue<GLOBALS.timeStep){
        for(var i=GLOBALS.timeStep;i>currValue;i--) {
            var ii=i-1;
            for(var j=0;j<GLOBALS.timeline[ii].length;j++) {
                if(!GLOBALS.timeline[ii][j].addRemove){
                    GLOBALS.occupancy[GLOBALS.timeline[ii][j].x][GLOBALS.timeline[ii][j].y][GLOBALS.timeline[ii][j].z]=true;
                    GLOBALS.addNode(GLOBALS.timeline[ii][j].x,GLOBALS.timeline[ii][j].y,GLOBALS.timeline[ii][j].z,true);
                }else{
                    GLOBALS.occupancy[GLOBALS.timeline[ii][j].x][GLOBALS.timeline[ii][j].y][GLOBALS.timeline[ii][j].z]=false;
                    GLOBALS.removeNode(GLOBALS.timeline[ii][j].x,GLOBALS.timeline[ii][j].y,GLOBALS.timeline[ii][j].z,true);
                }
                
            }
        }
    }else if (currValue>GLOBALS.timeStep){
        for(var i=GLOBALS.timeStep;i<currValue;i++) {
            var ii=i-0;
            for(var j=0;j<GLOBALS.timeline[ii].length;j++) {
                if(GLOBALS.timeline[ii][j].addRemove){
                    GLOBALS.occupancy[GLOBALS.timeline[ii][j].x][GLOBALS.timeline[ii][j].y][GLOBALS.timeline[ii][j].z]=true;
                    GLOBALS.addNode(GLOBALS.timeline[ii][j].x,GLOBALS.timeline[ii][j].y,GLOBALS.timeline[ii][j].z,true);
                }else{
                    GLOBALS.occupancy[GLOBALS.timeline[ii][j].x][GLOBALS.timeline[ii][j].y][GLOBALS.timeline[ii][j].z]=false;
                    GLOBALS.removeNode(GLOBALS.timeline[ii][j].x,GLOBALS.timeline[ii][j].y,GLOBALS.timeline[ii][j].z,true);
                }
                
            }

            
        }
    }else{
        // console.log("No Replay!");
    }
    
    GLOBALS.timeStep=parseInt(currValue);
    

}
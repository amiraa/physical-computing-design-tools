// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2019

/////////////////function calls/////////////////
//todo when calling say which gridsize and grid type
var utils= new utilities();
var GLOBALS=new globals(utils);

// 
var three=new threejs(GLOBALS,utils,'webgl','threejs1');
three.init();


initGraph();// todo enclose into class
initEditor();// todo enclose into class

// changed for assembler
// var assembler= new Assembler(three,GLOBALS,1,50,[new THREE.Vector3(GLOBALS.gridSize*GLOBALS.voxelSpacing,0,0)],[new THREE.Vector3(GLOBALS.gridSize*GLOBALS.voxelSpacing,0,GLOBALS.gridSize/2.0*GLOBALS.voxelSpacing)]);
// assembler.run();


/////////////assmebly////////////////////
var assembler= new Assembler(three,GLOBALS,1,50,[new THREE.Vector3(0,0,0)],[new THREE.Vector3(GLOBALS.gridSize/2.0*GLOBALS.voxelSpacing,0,0)]);
assembler.run();



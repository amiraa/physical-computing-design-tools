// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2019
if (viewRobotArm === undefined) {
    var viewRobotArm=true;
}
function globals(utils){
    this.gridSize=20;
    this.voxelSpacing=50;
    this.color1= 0xffffff; /*white*/
    this.color2= 0x020227;  /*kohly*/
    this.color3= 0x1c5c61; /*teal*/
    this.color4= 0xfa6e70; //red/orange
    this.color5=0x380152; //purple
    this.color6=0x696767; //grey
    this.color7=0x42f2f5; //light blue
    this.viewRobotArm=viewRobotArm;
    this.gridPresets={
        Cubic :  {
        
            xScale:1.0,
            yScale:1.0,
            zScale:1.0,
            xLineOffset:0.0,
            yLineOffset:0.0,
            xLayerOffset:0.0,
            yLayerOffset:0.0,
            zLayerRotation:0.0,
            doubleOffset:false,
            voxelSpacing:this.voxelSpacing,
            gridSize:this.gridSize,
            voxelScaleX:0.95,
            voxelScaleY:0.95,
            voxelScaleZ:0.95,
            neighbors:[
                {x: 1,y: 0,z: 0},
                {x:-1,y: 0,z: 0},
                {x: 0,y: 1,z: 0},
                {x: 0,y:-1,z: 0},
                {x: 0,y: 0,z: 1},
                {x: 0,y: 0,z:-1}
            ],
            neighborsLayerOffset:[
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0}
            ]
            
        },
        Aspect_2_1_4 :  {
            xScale:1.0,
            yScale:0.5,
            zScale:2.0,
            xLineOffset:0.0,
            yLineOffset:0.0,
            xLayerOffset:0.0,
            yLayerOffset:0.0,
            zLayerRotation:0.0,
            doubleOffset:false,
            voxelSpacing:this.voxelSpacing,
            gridSize:this.gridSize,
            voxelScaleX:1.0,
            voxelScaleY:1.0,
            voxelScaleZ:1.0,
            neighbors:[
                {x: 1,y: 0,z: 0},//todo fix
                {x:-1,y: 0,z: 0},//todo fix
                {x: 0,y: 1,z: 0},//todo fix
                {x: 0,y:-1,z: 0},//todo fix
                {x: 0,y: 0,z: 1},//todo fix
                {x: 0,y: 0,z:-1}
            ],
            neighborsLayerOffset:[
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0}
            ]
        },
        FaceCenteredCubic:  {
            xScale:1.0,
            yScale:1.0,
            zScale:Math.sqrt(2)/2,
            xLineOffset:0.0,
            yLineOffset:0.0,
            xLayerOffset:0.5,
            yLayerOffset:0.5,
            zLayerRotation:0.0,
            doubleOffset:false,
            voxelSpacing:this.voxelSpacing,
            gridSize:this.gridSize,
            voxelScaleX:1.0,
            voxelScaleY:1.0,
            voxelScaleZ:1.0,
            neighbors:[
                {x: 1,y: 0,z: 0},//todo fix
                {x:-1,y: 0,z: 0},//todo fix
                {x: 0,y: 1,z: 0},//todo fix
                {x: 0,y:-1,z: 0},//todo fix
                {x: 0,y: 0,z: 1},//todo fix
                {x: 0,y: 0,z:-1}
            ],
            neighborsLayerOffset:[
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0}
            ]
            
        },
        HexagonalClosePacked:  {
        
            xScale:1.0,
            yScale:Math.sqrt(3)/2,
            zScale:Math.sqrt(2)/Math.sqrt(3),
            xLineOffset:0.5,
            yLineOffset:0.0,
            xLayerOffset:0.5,
            yLayerOffset:1.0/Math.sqrt(12),
            zLayerRotation:0.0,
            doubleOffset:false,
            voxelSpacing:this.voxelSpacing,
            gridSize:this.gridSize,
            voxelScaleX:1.0,
            voxelScaleY:1.0,
            voxelScaleZ:1.0,
            neighbors:[
                {x: 1,y: 0,z: 0},//todo fix
                {x:-1,y: 0,z: 0},//todo fix
                {x: 0,y: 1,z: 0},//todo fix
                {x: 0,y:-1,z: 0},//todo fix
                {x: 0,y: 0,z: 1},//todo fix
                {x: 0,y: 0,z:-1}
            ],
            neighborsLayerOffset:[
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 0,z: 0}
            ]
            
        } ,
        Dice2:  {
            xScale:1.0,
            yScale:1.0,
            zScale:0.2,
            xLineOffset:0.0,
            yLineOffset:0.0,
            xLayerOffset:0.0,
            yLayerOffset:0.5,
            zLayerRotation:Math.PI/2.0,
            voxelSpacing:this.voxelSpacing,
            gridSize:this.gridSize,
            doubleOffset:false,
            voxelScaleX:0.5,
            voxelScaleY:0.9,
            voxelScaleZ:0.2,
            neighbors:[
                {x: 0,y: 0,z: 1},
                {x: 0,y: 0,z:-1},
                {x: 0,y:-1,z: 1},
                {x: 0,y:-1,z:-1}
            ],
            neighborsLayerOffset:[
                {x: 0,y: 1,z: 0},
                {x: 0,y: 0,z: 0},
                {x: 0,y: 1,z: 0},
                {x: 0,y: 1,z: 0} //if rotated
            ]
            
        },
        Dice:  {
            xScale:1.0,
            yScale:1.0,
            zScale:0.2,
            xLineOffset:0.0,
            yLineOffset:0.0,
            xLayerOffset:0.0,
            yLayerOffset:0.5,
            doubleOffset:true,
            xLayerOffset2:0.5,
            yLayerOffset2:0.5,
            zLayerRotation:Math.PI/2.0,
            voxelSpacing:this.voxelSpacing,
            gridSize:this.gridSize,
            voxelScaleX:0.5,
            voxelScaleY:0.9,
            voxelScaleZ:0.2,
            neighbors:[ // layer 0, 4,8
                {x: 0,y:-1,z:-1},
                {x:-1,y:-1,z:-1},
                {x: 0,y:-1,z: 1},
                {x: 0,y: 0,z: 1}
            ],
            neighborsLayerOffset:[ // layer 1, 5, 9
                {x: 0,y: 1,z:-1},
                {x: 0,y: 0,z:-1},
                {x:-1,y: 0,z: 1},
                {x: 0,y: 0,z: 1} 
            ],
            neighborsLayerOffset1:[ // layer 2, 6, 19
                {x: 1,y: 0,z:-1},
                {x: 0,y: 0,z:-1},
                {x: 0,y:-1,z: 1},
                {x: 0,y: 0,z: 1} 
            ],
            neighborsLayerOffset2:[ // layer 3, 7, 11
                {x: 0,y: 1,z:-1},
                {x: 0,y: 0,z:-1},
                {x: 0,y: 1,z: 1},
                {x: 1,y: 1,z: 1} 
            ]
            
        },
        Dice3:  {
            xScale:1.0,
            yScale:0.5,
            zScale:0.2,
            xLineOffset:0.5,
            yLineOffset:0.0,
            xLayerOffset:0.0,
            yLayerOffset:0.5,
            doubleOffset:true,
            xLayerOffset2:0.5,
            yLayerOffset2:0.5,
            zLayerRotation:Math.PI/2.0,
            voxelSpacing:this.voxelSpacing,
            gridSize:this.gridSize,
            voxelScaleX:0.5,
            voxelScaleY:0.9,
            voxelScaleZ:0.2,
            neighbors:[ // layer 0, 4,8
                {x: 0,y:-1,z:-1},
                {x:-1,y:-1,z:-1},
                {x: 0,y:-1,z: 1},
                {x: 0,y: 0,z: 1}
            ],
            neighborsLayerOffset:[ // layer 1, 5, 9
                {x: 0,y: 1,z:-1},
                {x: 0,y: 0,z:-1},
                {x:-1,y: 0,z: 1},
                {x: 0,y: 0,z: 1} 
            ],
            neighborsLayerOffset1:[ // layer 2, 6, 19
                {x: 1,y: 0,z:-1},
                {x: 0,y: 0,z:-1},
                {x: 0,y:-1,z: 1},
                {x: 0,y: 0,z: 1} 
            ],
            neighborsLayerOffset2:[ // layer 3, 7, 11
                {x: 0,y: 1,z:-1},
                {x: 0,y: 0,z:-1},
                {x: 0,y: 1,z: 1},
                {x: 1,y: 1,z: 1} 
            ]
            
        }
        
        
    };

    this.grid=this.gridPresets.Cubic;
    this.utils=utils;

    this.occupancy=[];
    for (var i=0;i<this.gridSize;++i){
        this.occupancy.push([]);
        for (var j=0;j<this.gridSize;++j){
            this.occupancy[i].push([]);
            for (var k=0;k<this.gridSize;++k){
                this.occupancy[i][j].push(false);
            }
        }
    }
    this.timeStep=0;
    this.totalTimeSteps=0;
    this.timeline=[];
    this.selectedjson;
    this.editor;
    this.globalJson={nodes:[],
        simulationSteps:10};
    

}

//////////////////////events//////////////////
globals.prototype.addNode=function (x,y,z,replay=false,data={}){
    x=parseInt(x);
    y=parseInt(y);
    z=parseInt(z);
    [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y]=this.utils.getTransforms(this.grid,x, y,z);
    var addNodeEvent = new CustomEvent('addNode', { 
        detail: 
        {
            x:x,
            y:y,
            z:z,
            id:'[' +x +"," +y+","+z+']',
            posX:p_x,
            posY:p_y,
            posZ:p_z,
            rotY:r_y,
            replay:replay,
            data:data,
        }
    });
    document.dispatchEvent(addNodeEvent);
};

globals.prototype.removeNode=function (x,y,z,replay=false){
    x=parseInt(x);
    y=parseInt(y);
    z=parseInt(z);
    [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y]=this.utils.getTransforms(this.grid,x, y,z);
    var removeNodeEvent = new CustomEvent('removeNode', { 
        detail: 
        {
            x:x,
            y:y,
            z:z,
            id:'[' +x +"," +y+","+z+']',
            posX:p_x,
            posY:p_y,
            posZ:p_z,
            rotY:r_y,
            replay:replay
        }
    });
    document.dispatchEvent(removeNodeEvent);
};

globals.prototype.updateNode=function (data){
    var pos=utils.getXYZfromName(data.id);
    [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y]=this.utils.getTransforms(this.grid,pos.x,pos.y,pos.z);
    var updateNodeEvent = new CustomEvent('updateNode', { 
        detail: 
        {
            id: data.id,
            name: data.name,
            parent: data.parent,
            code: data.code,
            state:data.state,
            neighbors:data.neighbors,
            inputs:data.inputs,
            outputs:data.outputs,
            locked:data.locked,
            numRuns:data.numRuns,
            dnn:data.cnn,
            x:pos.x,
            y:pos.y,
            z:pos.z,
            posX:p_x,
            posY:p_y,
            posZ:p_z,
            rotY:r_y
        }
    });
    document.dispatchEvent(updateNodeEvent);
};

globals.prototype.runNode=function (x,y,z){
    x=parseInt(x);
    y=parseInt(y);
    z=parseInt(z);
    [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y]=this.utils.getTransforms(this.grid,x, y,z);
    var runNodeEvent = new CustomEvent('runNode', { 
        detail: 
        {
            x:x,
            y:y,
            z:z,
            id:'[' +x +"," +y+","+z+']',
            posX:p_x,
            posY:p_y,
            posZ:p_z,
            rotY:r_y,
            replay:replay
        }
    });
    document.dispatchEvent(runNodeEvent);
};

globals.prototype.adjustTimeStep=function (addRemove,x,y,z){
    var adjustTimeStepEvent = new CustomEvent('adjustTimeStep', { 
        detail: 
        {
            x:x,
            y:y,
            z:z,
            id:'[' +x +"," +y+","+z+']',
            addRemove:addRemove,
            posX:p_x,
            posY:p_y,
            posZ:p_z,
            rotY:r_y
        }
    });
    document.dispatchEvent(adjustTimeStepEvent);
};

globals.prototype.changeNodeColor=function (x,y,z,replay=false,data={}){
    x=parseInt(x);
    y=parseInt(y);
    z=parseInt(z);
    [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y]=this.utils.getTransforms(this.grid,x, y,z);
    var addNodeEvent = new CustomEvent('changeNodeColor', { 
        detail: 
        {
            x:x,
            y:y,
            z:z,
            id:'[' +x +"," +y+","+z+']',
            posX:p_x,
            posY:p_y,
            posZ:p_z,
            rotY:r_y,
            replay:replay,
            data:data
        }
    });
    document.dispatchEvent(addNodeEvent);
};

globals.prototype.selectNode=function (x,y,z){
    var selectNodeEvent = new CustomEvent('selectNode', { 
        detail: 
        {
            x:x,
            y:y,
            z:z,
            id:'[' +x +"," +y+","+z+']',
            posX:p_x,
            posY:p_y,
            posZ:p_z
        }
    });
    document.dispatchEvent(selectNodeEvent);
};

globals.prototype.selectEdge=function (){
    var selectEdgeEvent = new CustomEvent('selectEdge', { 
        detail: 
        {
            // x:x,
            // y:y,
            // z:z,
            // id:'[' +x +"," +y+","+z+']',
            // posX:p_x,
            // posY:p_y,
            // posZ:p_z
        }
    });
    document.dispatchEvent(selectEdgeEvent);
};

globals.prototype.runExamplePrediction=function (){
    var runExamplePredictionEvent = new CustomEvent('runExamplePrediction', { 
        detail: 
        {
            // x:x,
            // y:y,
            // z:z,
            // id:'[' +x +"," +y+","+z+']',
            // posX:p_x,
            // posY:p_y,
            // posZ:p_z
        }
    });
    document.dispatchEvent(runExamplePredictionEvent);
};
//////////////////////////////////////////////

//////////////////////utils//////////////////
//rule: utilities shouldn't be dependent on globals
function utilities(){
}

utilities.prototype.getXYZfromName=function(name){
    var nums=name.match(/\d+/g);
    var x=nums[0];
    var y=nums[1];
    var z=nums[2];
    return new THREE.Vector3(x,y,z);
};

utilities.prototype.getNeighborsList=function(grid,x,y,z){
    var list=[];
    for(var i=0;i<grid.neighbors.length;i++){
        //if 0,4,8
        var x1=parseInt(x)+grid.neighbors[i].x;
        var y1=parseInt(y)+grid.neighbors[i].y;
        var z1=parseInt(z)+grid.neighbors[i].z;
        if(z%2!=0){
            x1+=grid.neighborsLayerOffset[i].x;
            y1+=grid.neighborsLayerOffset[i].y;
            z1+=grid.neighborsLayerOffset[i].z;
        }
        if(grid.doubleOffset){
            if(parseInt(z/2)%2!=0){ //2,3 , 6,7
                if(z%2!=0){//3,7,11
                    x1=parseInt(x)+grid.neighborsLayerOffset2[i].x;
                    y1=parseInt(y)+grid.neighborsLayerOffset2[i].y;
                    z1=parseInt(z)+grid.neighborsLayerOffset2[i].z;
                }else{//2,6
                    x1=parseInt(x)+grid.neighborsLayerOffset1[i].x;
                    y1=parseInt(y)+grid.neighborsLayerOffset1[i].y;
                    z1=parseInt(z)+grid.neighborsLayerOffset1[i].z;

                }
            }else{ //0,1 ,4,5
                if(z%2!=0){//1,5
                    x1=parseInt(x)+grid.neighborsLayerOffset[i].x;
                    y1=parseInt(y)+grid.neighborsLayerOffset[i].y;
                    z1=parseInt(z)+grid.neighborsLayerOffset[i].z;
                }else{ //0,4
                }

            }
        }
        if(this.inBounds(grid.gridSize,x1,y1,z1)){
            list.push([x1,y1,z1]);
        }
        

    }
    return list;

};

utilities.prototype.inBounds=function(gridSize,x,y,z){
    return (x>=0&&x<gridSize&& y>=0&&y<gridSize&& z>=0&&z<gridSize);
};

utilities.prototype.getTransforms=function(grid,x, y,z){
    var s_x=grid.voxelScaleX;
    var s_z=grid.voxelScaleY;
    var s_y=grid.voxelScaleZ;

    var p_x=0.0;
    var p_y=0.0;
    var p_z=0.0;
    var r_y=0.0;
    if(z%2!=0){
        r_y=grid.zLayerRotation;
        p_x+=grid.voxelSpacing*grid.xScale*(grid.xLayerOffset);
        p_z+=grid.voxelSpacing*grid.yScale*(grid.yLayerOffset);

    }
    if(grid.doubleOffset){
        if(parseInt(z/2)%2!=0){
            p_x+=grid.voxelSpacing*grid.xScale*(grid.xLayerOffset2);
            p_z+=grid.voxelSpacing*grid.yScale*(grid.yLayerOffset2);
        }
    }

    p_x+=grid.voxelSpacing*grid.xScale*(x );
    p_z+=grid.voxelSpacing*grid.yScale*(y );
    p_y+=grid.voxelSpacing*grid.zScale*(z );

    if(y%2!=0){
        p_x+=grid.voxelSpacing*grid.xScale*(grid.xLineOffset);
        p_z+=grid.voxelSpacing*grid.yScale*(grid.yLineOffset);

    }
    return [p_x ,p_y ,p_z ,s_x ,s_y,s_z,r_y];
};

//////////////////////////////////////////////




//////////performance calculations/////////////////



//////////////////////////////////////////////


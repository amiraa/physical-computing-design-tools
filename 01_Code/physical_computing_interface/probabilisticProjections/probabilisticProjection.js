
console.log(webppl)
var code=
`var foo = function(x) {
    var bar = Math.exp(x)
    var baz =  x==0 ? [] : [Math.log(bar), foo(x-1)]
    return baz
  }
  
  foo(5)`;

var code1=
`
viz(Bernoulli({ p: 0.5 }))
`;

var code1=
`
var model = function() {
	//MODEL
	var strength = mem(function (person) {return gaussian(50, 10)})
	var lazy = function(person) {return flip(1/3) }
	var pulling = function(person) {
		return lazy(person) ? strength(person) / 2 : strength(person) }
	var totalPulling = function (team) {return sum(map(pulling, team))}
	var winner = function (team1, team2) {
		totalPulling(team1) > totalPulling(team2) ? team1 : team2 }
	var beat = function(team1,team2){winner(team1,team2) == team1}

	//CONDITION
	condition(beat(['Tom'], ['Steve','Bill']))

	//QUERY
	return strength('Tom')
}
var options = {method: 'MCMC', kernel: 'MH', samples: 10000}
var dist = Infer(options, model)
viz(dist)
display('Expected strength: ' + expectation(dist))

`

var code=
`
let processors={
  type:{
    stm32f412 :
    {
      name: "STM32F412",
      ram: 256e3,
      // cyclesPerSecond : 100e6, // ZF: this is based on 100 MHz clock rate
      cyclesPerSecond : 12.8e6, // ZF: this is based on MFlops from pi benchmark at 84 MHz
      bitsPerSecond : 6e6, // ZF: 6 MBit UART max spec (from datasheet)
      costPerChip : 3.34925, // reel qty (5000)
      flopsPerWatt : 1.71e8 // ZF: calculated from datasheet and pi test
    },
    samd51j19:
    {
      name : "SAMD51J19",
      ram : 192e3,
      // cyclesPerSecond : 120e6, // ZF: this is based on 120 MHz clock rate
      cyclesPerSecond : 16.8e6, // ZF: this is based on MFlops from pi benchmark at ~160 MHz
      bitsPerSecond : 3e6, // ZF: 3 MBit UART max spec (from datasheet)
      costPerChip : 3.44003, // reel qty (5000)
      flopsPerWatt : 2.33e8 // ZF: calculated from datasheet and pi test
    },
    samd51j20:
    {
      name : "SAMD51J20",
      ram : 262e3,
      // cyclesPerSecond : 120e6, // ZF: this is based on 120 MHz clock rate
      cyclesPerSecond : 16.8e6, // ZF: this is based on MFlops from pi benchmark at ~160 MHz
      bitsPerSecond : 3e6, // ZF: 3 MBit UART max spec (from datasheet)
      costPerChip : 4.08002, // reel qty (5000)
      flopsPerWatt : 2.33e8 // ZF: calculated from datasheet and pi test
    },
    MAX32660:
    {
      name : "MAX32660",
      ram : 96e3,
      // cyclesPerSecond : 96e6, // ZF: this is based on 96 MHz clock rate
      cyclesPerSecond : 12.8e6, // ZF: this is based on extrapolating MFlops from STM32F412 pi benchmark
      bitsPerSecond : 48e6,
      costPerChip : 1.1745, // reel qty (2000)
      flopsPerWatt: 4.75e8 // ZF: calculated from datasheet and STM32F412 pi test

    },
    STM32F412:
    {
      name : "STM32F412",
      ram : 256e3,
      // cyclesPerSecond : 100e6, // ZF: this is based on 100 MHz clock rate
      cyclesPerSecond : 12.8e6, // ZF: this is based on MFlops from pi benchmark at 84 MHz
      bitsPerSecond : 6e6, // ZF: 6 MBit UART max spec (from datasheet)
      costPerChip : 3.34925, // reel qty (5000)
      flopsPerWatt : 1.71e8 // ZF: calculated from datasheet and pi test

    },
    XMEGA:
    {
      name: 'XMEGA',
      ram:10,
      cyclesPerSecond:100,
      bitsPerSecond:10

    }
  }
};

var model=function (){

  var chipType=function () {	return uniformDraw(["STM32F412","samd51j19","samd51j20","STM32F412","XMEGA"])};//"MAX32660",
  var chip=processors.type[chipType()];
  //display(chip.name);

  var ram=chip.ram;
  var cyclesPerSecond =chip.cyclesPerSecond;
  var bitsPerSecond   =chip.bitsPerSecond  ;
  var costPerChip     =chip.costPerChip    ;
  var flopsPerWatt    =chip.flopsPerWatt   ;

  var interconnect=mem(function (model) {	return uniformDraw([2,4,6])});
  var nodesNumber= mem(function (model) {	return uniformDraw([10,20,30,40,50,100,200])});

  var dataExchangeSize=mem(function (model) {return gaussian(1e3, 10)}); //really needed?
  var memoryPerComputation=mem(function (model) {return gaussian(35e3, 100)}); //really needed?
  var numberofComputations=mem(function (model) {return gaussian(1, 0.1)}); //nodes of similar computations
  var computation = mem(function (model) {return gaussian(105e6, 10)}); //Todo i could remove 
  var maxNumberOfInterconnects= mem(function (model) {return gaussian(10, 2)}); //Todo i could remove 

  // var dataExchangeSize=mem(function (model) {return gaussian(1e3, 10)}); //really needed?
  // var memoryPerComputation=mem(function (model) {return gaussian(10e3, 10)}); //really needed?
  // var numberofComputations=mem(function (model) {return gaussian(200, 10)}); //nodes of similar computations
  // var computation = mem(function (model) {return gaussian(100, 20)}); //Todo i could remove 
  // var maxNumberOfInterconnects= mem(function (model) {return gaussian(10, 2)}); //Todo i could remove 



  var getComputationDelay=function(memoryPerComputation,ram,numberofComputations){
    // 1- look data size/storage
    if(memoryPerComputation>ram){
      var nodesPerComputation=Math.ceil(memoryPerComputation/ram);
      var numberofNodesNeeded= nodesPerComputation*numberofComputations;
      // 3- Look for number of computations vs number of nodes
      // if number of nodes needed <number of nodes
      if(numberofNodesNeeded>nodesNumber("DICE")){
        //console.log("need more nodes!!!!");
        //Then overload some of the computations where one node doing double of work others have to wait for it
      }
      var computationPerNode=1/nodesPerComputation;

      return [nodesPerComputation,computationPerNode,numberofNodesNeeded]

      //update communication delay 

    }else{ //if data less than ram
      var computationPerNode=Math.floor(ram/memoryPerComputation);
      var nodesPerComputation=1/computationPerNode;

      var numberofNodesNeeded= Math.ceil(nodesPerComputation*numberofComputations);

      return [nodesPerComputation,computationPerNode,numberofNodesNeeded]

      //update communication delay 
    }
    ////questions///
    //do i use less memory the
    /////////////////

  }

  var getCommunicationDelay= function(memoryPerComputation,ram,interconnect,maxNumberOfInterconnects,nodesPerComputation,computationPerNode){
    if(memoryPerComputation>ram){
      // 2- look for number of interconnects in the software
      if(interconnect<maxNumberOfInterconnects*nodesPerComputation){
        return Math.ceil(maxNumberOfInterconnects*nodesPerComputation/interconnect);//could be expressed in form of a distribuition
        //Or add time for interchange?
      } else{
        return 1.0;
      }
    }else{
      // 2- look for number of interconnects in the software
      if(interconnect<maxNumberOfInterconnects*computationPerNode){
        return Math.ceil(maxNumberOfInterconnects*computationPerNode/interconnect);//could be expressed in form of a distribuition
        //Or add time for interchange?
      }else{
        return 1.0;
      }
    }
  }

  var computationDelay=getComputationDelay(memoryPerComputation("DICE"),ram,numberofComputations("DICE"));

  var nodesPerComputation=computationDelay[0];
  var computationPerNode =computationDelay[1];
  var numberofNodesNeeded=computationDelay[2];

  var communicationDelay= getCommunicationDelay(memoryPerComputation,ram,interconnect("DICE"),maxNumberOfInterconnects("DICE"),nodesPerComputation,computationPerNode);

  var storagePercent= memoryPerComputation("DICE")*computationPerNode/ram*100;

  // 4- compute performance
  //    Time of computation
  var computationTime=computation("DICE")*computationPerNode/cyclesPerSecond; //?? remove nodes percomp
  var communicationTime=dataExchangeSize("DICE")/bitsPerSecond*communicationDelay; //assume all in parallel


  var speed=computationTime+communicationTime; //frame
  var cost=nodesNumber("DICE")*costPerChip;
  var energy= 1 / flopsPerWatt*numberofNodesNeeded*computationTime;//per frame

  // display('speed: ' + DICE[0])
  // display('cost: ' + DICE[1])
  // display('energy: ' + DICE[2])

  condition(speed<100 && cost<18000);
  // condition(speed<0.0012 && cost<180);
  // return [speed,cost,energy]

  if(result==1){
    return chip.name;

  }else if(result==2){
    return interconnect("DICE");

  }else{
    return nodesNumber("DICE");
  }

  //[chip.name,interconnect("DICE"),nodesNumber("DICE")];
}

// var DICE= model();

var result=3;
var options = {method: 'MCMC', kernel: 'MH', samples: 10000}
var dist = Infer(options, model)
viz(dist)

// viz(dist[1])
// viz(dist[2])
display('Expected nodesNumber: ' + expectation(dist))
expectation(dist)

// display('speed: ' + DICE[0])
// display('cost: ' + DICE[1])
// display('energy: ' + DICE[2])
`
var compiled = webppl.compile(code, { debug: true });

var endJob = function (store, returnValue) {
    var renderedReturnValue = renderReturnValue(returnValue);
    console.log(renderedReturnValue);
    // comp.addResult({ type: 'text', message: renderedReturnValue });
    // cleanup();
};

var renderReturnValue = function (x) {
    if (x === undefined) {
      return '';
    }
    if (x && x.score != undefined && x.sample != undefined) if (typeof viz == 'undefined') {
      return util.serialize(x);
    } else {
      if (typeof viz.auto == 'function') {
        viz.auto(x);
        return;
      }
    }
  
    if (typeof x == 'function') return '<function ' + x.name + '>';
  
    if (typeof x == 'string') {
      return x;
    }
  
    return JSON.stringify(x);
};

var prepared = webppl.prepare(compiled,endJob);
// console.log(prepared);


prepared.run();
// console.log(prepared)
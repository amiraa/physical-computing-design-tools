var color1= "#ffffff"; //white
var color11= "#ffffff8c"; //white transparent
var color2= "#020227";  //dark blue
var color3= "#1c5c61"; //teal
var color33= "#1c5c618c"; //teal transparent
var color4= "#fa6e70"; //red/orange
var color44= "#fa6e708c"; //red/orange
var color5="#380152"; //purple
var color6="#696767"; //grey
var font= "consolas";//'font-family'

var cy = window.cy= cytoscape({
  container: document.getElementById('cy'),
  ready: function(){
      var api = this.expandCollapse({
          layoutBy: {
              name: "cose-bilkent",
              animate: "end",
              randomize: false,
              fit: true
          },
          fisheye: true,
          animate: false,
          undoable: false
      });
      // api.collapseAll();
  },
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'content': 'data(name)',
        'text-valign': 'center',
        'color': 'white',
        'font-family': "consolas",
        // 'font-family':"Times New Roman",
        'width': 80,
        'height': 80
      })
    .selector('edge')
      .css({
        'content': 'data(name)',
        'width': 8,
        'line-color': '#888',
        'target-arrow-color': '#888',
        'source-arrow-color': '#888',
        'target-arrow-shape': 'triangle'
      })
    .selector(':selected')
    .selector('$node > node')
      .css({
        'shape': 'roundrectangle',
        'text-valign': 'top',
        'background-color': '#ccc',
        'background-opacity': 0.1,
        'color': '#888',
        'text-outline-width':
        0,
        'font-size': 40
      })
    .selector('#core, #app')
      .css({
        'width': 120,
        'height': 120,
        'font-size': 40
      })
    .selector('#api')
      .css({
        'padding-top': 20,
        'padding-left': 20,
        'font-size': 40,
        'padding-bottom': 20,
        'padding-right': 20
      })
    .selector('#ext, .ext')
      .css({
        'background-color': color6,
        // 'text-outline-color': '#93CDDD',
        'line-color': color6,
        'target-arrow-color': color6,
        'color': color1,
        'font-family':font,
        "text-wrap": "wrap",
        'font-size': 30,
      })
    .selector('#input, .input')
      .css({
        'background-color': color3,
        // 'text-outline-color': '#93CDDD',
        'line-color': color3,
        'target-arrow-color': color3,
        'color': color1,
        'font-family':font,
        'font-size': 30,
        "text-wrap": "wrap",
      })
    .selector('#output, .output')
      .css({
        'background-color': color4,
        // 'text-outline-color': '#93CDDD',
        'line-color': color4,
        'target-arrow-color': color4,
        'color': color1,
        'font-family':font,
        'font-size': 30,
        "text-wrap": "wrap",
      })
    .selector('#variable, .variable')
      .css({
        'background-color': color5,
        // 'text-outline-color': '#93CDDD',
        'line-color': color5,
        'target-arrow-color': color5,
        'color': color1,
        'font-family':font,
        'font-size': 30,
        "text-wrap": "wrap",
      })
      .selector('#exte, .exte')
      .css({
        'background-color': color3,
        // 'text-outline-color': '#93CDDD',
        'line-color': color3,
        'target-arrow-color': color3,
        'color': color1,
        'font-family':font,
        'curve-style': 'bezier',
        'width': 2,
        'font-size': 30,
        "text-wrap": "wrap",
      })
    .selector('#app, .app')
      .css({
        'background-color': '#F79646',
        'text-outline-color': '#F79646',
        'line-color': '#F79646',
        'target-arrow-color': '#F79646',
        'font-size': 30,
        'color': '#fff'
      })
    .selector('#cy')
      .css({
        'background-opacity': 0,
        'border-width': 1,
        'border-color': '#aaa',
        'border-opacity': 0.3,
        'font-size': 50,
        'padding-top': 40,
        'padding-left': 40,
        'padding-bottom': 40,
        'font-size': 30,
        'padding-right': 40
      }),

  elements: {
    nodes: [
      
      ///legend
      {
        data: { id: 'legend', name: 'Legend' ,},
      },
      {
        data: { id: 'inputs', name: 'inputs',parent:'legend' },
        position: { x: 0, y: 0 },classes: 'input',
      },
      {
        data: { id: 'outputs', name: 'outputs',parent:'legend'},
        position: { x: 0, y: 0 },classes: 'output',
      },
      {
        data: { id: 'variables', name: 'variables',parent:'legend'},
        position: { x: 0, y: 0 },classes: 'variable',
      },
      {
        data: { id: 'intermediate1', name: 'intermediate',parent:'legend'},
        position: { x: 0, y: 0 },classes: 'ext',
      },
      ////

      {
        data: { id: 'DICE', name: 'DICE' },
      },

      ///////////hardware

      {
        data: { id: 'hardware', name: 'Hardware', parent: 'DICE' },
        position: { x: 0, y: 0 },
      },
      {
        data: { id: 'chip', name: 'Chip', parent: 'hardware' },
        position: { x: 0, y: 0 },
      },
      {
        data: { id: 'ram', name: 'ram', parent: 'chip' },
        position: { x: 0, y: 0 },classes: 'variable',
      },
      {
        data: { id: 'cyclesPerSecond', name: 'cycles Per second', parent: 'chip' },
        position: { x: 0, y: 0 },classes: 'variable',
      },
      {
        data: { id: 'bitsPerSecond', name: 'bits per second', parent: 'chip' },
        position: { x: 0, y: 0 },classes: 'variable',
      },
      {
        data: { id: 'costPerChip', name: 'cost per chip', parent: 'chip' },
        position: { x: 0, y: 0 },classes: 'variable',
      },
      {
        data: { id: 'flopsPerWatt', name: 'flops per watt', parent: 'chip' },
        position: { x: 0, y: 0 },classes: 'variable',
      },
      {
        data: { id: 'interconnect', name: '# of Interconnect', parent: 'hardware' },
        position: { x: 0, y: 0 },classes: 'input', //could be variable
      },
      {
        data: { id: 'nodesNumber', name: '# of Nodes', parent: 'hardware' },
        position: { x: 0, y: 0 },classes: 'variable',
      },

      ///////////////software
      {
        data: { id: 'software', name: 'Software', parent: 'DICE' },
        position: { x: 1000, y: 0 },
      },
      {
        data: { id: 'dataExchangeSize', name: 'Data Exchanged Size', parent: 'software' },
        position: { x: 1000, y: 0 },classes: 'input',
      },
      {
        data: { id: 'computation', name: 'Computation', parent: 'software' },
        position: { x: 1000, y: 0 },classes: 'input',
      },
      {
        data: { id: 'memoryPerComputation', name: 'memory per computation', parent: 'software' },
        position: { x: 1000, y: 0 },classes: 'input',
      },
      {
        data: { id: 'maxNumberOfInterconnects', name: '# of Interconnects', parent: 'software' },
        position: { x: 1000, y: 0 },classes: 'input',
      },
      {
        data: { id: 'numberofComputations', name: '# of Computations', parent: 'software' },
        position: { x: 1000, y: 0 },classes: 'input',
      },

      /////////////performance

      {
        data: { id: 'performance', name: 'Performance per Frame' },
        position: { x: 0, y: 2000 },
      },

      {
        data: { id: 'speed', name: 'Speed' , parent: 'performance'},
        position: { x: 0, y: 2000 },classes: 'output',
      },
      {
        data: { id: 'energy', name: 'Energy' , parent: 'performance'},
        position: { x: 0, y: 2000 },classes: 'output',
      },
      {
        data: { id: 'cost', name: 'Cost' , parent: 'performance'},
        position: { x: 0, y: 2000 },classes: 'output',
      },

      ///intermediate numbers in between computations
      {
        data: { id: 'intermediate', name: 'intermediate calculations',parent:"DICE" },
        position: { x: 0, y: 1000 },
      },
      {
        data: { id: 'nodesPerComputation', name: 'nodes per computation' , parent: 'intermediate'},
        position: { x: 0, y: 1000 },classes: 'ext',
      },
      {
        data: { id: 'numberofNodesNeeded', name: '# of nodes needed' , parent: 'intermediate'},
        position: { x: 0, y: 1000 },classes: 'ext',
      },
      {
        data: { id: 'computationTime', name: 'computation time' , parent: 'intermediate'},
        position: { x: 0, y: 1000 },classes: 'ext',
      },
      {
        data: { id: 'communicationTime', name: 'communication time' , parent: 'intermediate'},
        position: { x: 0, y: 1000 },classes: 'ext',
      },
      {
        data: { id: 'communicationDelay', name: 'communication delay' , parent: 'intermediate'},
        position: { x: 0, y: 1000 },classes: 'ext',
      },
      {
        data: { id: 'computationPerNode', name: 'computation per node' , parent: 'intermediate'},
        position: { x: 0, y: 1000 },classes: 'ext',
      },
      {
        data: { id: 'storagePercent', name: 'storage Percent' , parent: 'intermediate'},
        position: { x: 0, y: 1000 },classes: 'ext',
      },

    ],
    edges: [

      { data: { source: 'ram', target: 'nodesPerComputation' , name: ''},classes: 'exte', },
      { data: { source: 'memoryPerComputation', target: 'nodesPerComputation', name: '' },classes: 'exte', },

      { data: { source: 'ram', target: 'computationPerNode', name: '' },classes: 'exte', },
      { data: { source: 'memoryPerComputation', target: 'computationPerNode', name: '' },classes: 'exte', },

      { data: { source: 'maxNumberOfInterconnects', target: 'communicationDelay' , name: ''},classes: 'exte', },
      { data: { source: 'interconnect', target: 'communicationDelay' , name: ''},classes: 'exte', },

      { data: { source: 'nodesPerComputation', target: 'numberofNodesNeeded' , name: ''},classes: 'exte', },
      { data: { source: 'numberofComputations', target: 'numberofNodesNeeded' , name: ''},classes: 'exte', },

      { data: { source: 'computationPerNode', target: 'nodesPerComputation', name: '' },classes: 'exte', },

      { data: { source: 'nodesNumber', target: 'numberofNodesNeeded' , name: ''},classes: 'exte', },//???//check

      { data: { source: 'computation', target: 'computationTime' , name: ''},classes: 'exte', },
      { data: { source: 'cyclesPerSecond', target: 'computationTime' , name: ''},classes: 'exte', },

      { data: { source: 'dataExchangeSize', target: 'communicationTime' , name: ''},classes: 'exte', },
      { data: { source: 'bitsPerSecond', target: 'communicationTime' , name: ''},classes: 'exte', },
      { data: { source: 'communicationDelay', target: 'communicationTime' , name: ''},classes: 'exte', },

      { data: { source: 'computationTime', target: 'speed' , name: ''},classes: 'exte', },
      { data: { source: 'communicationTime', target: 'speed', name: '' },classes: 'exte', },

      { data: { source: 'nodesNumber', target: 'cost', name: '' },classes: 'exte', },
      { data: { source: 'costPerChip', target: 'cost' , name: ''},classes: 'exte', },

      { data: { source: 'flopsPerWatt', target: 'energy' , name: ''},classes: 'exte', },
      { data: { source: 'numberofNodesNeeded', target: 'energy', name: '' },classes: 'exte', },
      { data: { source: 'computation', target: 'energy' , name: ''},classes: 'exte', },


      { data: { source: 'memoryPerComputation', target: 'storagePercent', name: '' },classes: 'exte', },
      { data: { source: 'computationPerNode', target: 'storagePercent', name: '' },classes: 'exte', },
      { data: { source: 'ram', target: 'storagePercent', name: '' },classes: 'exte', },




    ]
  },

  layout: {
    name: 'preset'
  }
});

var api = cy.expandCollapse('get');

api.expandAll();

///hardware library
let processors={
  type:{
    stm32f412 :
    {
      name: "STM32F412",
      ram: 256e3,
      // cyclesPerSecond : 100e6, // ZF: this is based on 100 MHz clock rate
      cyclesPerSecond : 12.8e6, // ZF: this is based on MFlops from pi benchmark at 84 MHz
      bitsPerSecond : 6e6, // ZF: 6 MBit UART max spec (from datasheet)
      costPerChip : 3.34925, // reel qty (5000)
      flopsPerWatt : 1.71e8 // ZF: calculated from datasheet and pi test
    },
    samd51j19:
    {
      name : "SAMD51J19",
      ram : 192e3,
      // cyclesPerSecond : 120e6, // ZF: this is based on 120 MHz clock rate
      cyclesPerSecond : 16.8e6, // ZF: this is based on MFlops from pi benchmark at ~160 MHz
      bitsPerSecond : 3e6, // ZF: 3 MBit UART max spec (from datasheet)
      costPerChip : 3.44003, // reel qty (5000)
      flopsPerWatt : 2.33e8 // ZF: calculated from datasheet and pi test
    },
    samd51j20:
    {
      name : "SAMD51J20",
      ram : 262e3,
      // cyclesPerSecond : 120e6, // ZF: this is based on 120 MHz clock rate
      cyclesPerSecond : 16.8e6, // ZF: this is based on MFlops from pi benchmark at ~160 MHz
      bitsPerSecond : 3e6, // ZF: 3 MBit UART max spec (from datasheet)
      costPerChip : 4.08002, // reel qty (5000)
      flopsPerWatt : 2.33e8 // ZF: calculated from datasheet and pi test
    },
    MAX32660:
    {
      name : "MAX32660",
      ram : 96e3,
      // cyclesPerSecond : 96e6, // ZF: this is based on 96 MHz clock rate
      cyclesPerSecond : 12.8e6, // ZF: this is based on extrapolating MFlops from STM32F412 pi benchmark
      bitsPerSecond : 48e6,
      costPerChip : 1.1745, // reel qty (2000)
      flopsPerWatt: 4.75e8 // ZF: calculated from datasheet and STM32F412 pi test

    },
    STM32F412:
    {
      name : "STM32F412",
      ram : 256e3,
      // cyclesPerSecond : 100e6, // ZF: this is based on 100 MHz clock rate
      cyclesPerSecond : 12.8e6, // ZF: this is based on MFlops from pi benchmark at 84 MHz
      bitsPerSecond : 6e6, // ZF: 6 MBit UART max spec (from datasheet)
      costPerChip : 3.34925, // reel qty (5000)
      flopsPerWatt : 1.71e8 // ZF: calculated from datasheet and pi test

    },
    XMEGA:
    {
      name: 'XMEGA',
      ram:10,
      cyclesPerSecond:100,
      bitsPerSecond:10

    }
  }
};

const chip=processors.type.samd51j19;
const ram=chip.ram;
const cyclesPerSecond =chip.cyclesPerSecond;
const bitsPerSecond   =chip.bitsPerSecond  ;
const costPerChip     =chip.costPerChip    ;
const flopsPerWatt    =chip.flopsPerWatt   ;

updateVariable("chip", chip.name,"");
updateVariable("ram", ram,"bytes");
updateVariable("cyclesPerSecond", cyclesPerSecond),"";
updateVariable("bitsPerSecond", bitsPerSecond  ,"");
updateVariable("costPerChip", costPerChip   ,"dollars" );
updateVariable("flopsPerWatt", flopsPerWatt ,""  );

const interconnect=4; //2,4,6
const nodesNumber= 10; //10,20,30,40,50
updateVariable("interconnect", interconnect,"neighbour");
updateVariable("nodesNumber", nodesNumber,"nodes");

var dataExchangeSize= 37e6*32; 
var memoryPerComputation= 37e6*32;
var numberofComputations= 1; //nodes of similar computations
var computation= 150e6; //cylces
var maxNumberOfInterconnects= 10;

updateVariable("dataExchangeSize", dataExchangeSize,"bytes");
updateVariable("memoryPerComputation", memoryPerComputation,"bytes");
updateVariable("numberofComputations", numberofComputations,"");
updateVariable("computation", computation,"cycle");
updateVariable("maxNumberOfInterconnects", maxNumberOfInterconnects,"neighbour");

var nodesPerComputation=1;
var computationPerNode=1;
var numberofNodesNeeded=1;
var computationTime;
var communicationTime;
var speed;
var cost;
var energy;
var communicationDelay=1.0; //frames
var storagePercent=0;;




// 1- look data size/storage
if(memoryPerComputation>ram){
  nodesPerComputation=Math.ceil(memoryPerComputation/ram);
  numberofNodesNeeded= nodesPerComputation*numberofComputations;
  // 3- Look for number of computations vs number of nodes
  // if number of nodes needed <number of nodes
  if(numberofNodesNeeded>nodesNumber){
    console.log("need more nodes!!!!");
    //Then overload some of the computations where one node doing double of work others have to wait for it
  }
  computationPerNode=1/nodesPerComputation;

  // 2- look for number of interconnects in the software
  if(interconnect<maxNumberOfInterconnects*nodesPerComputation){
    communicationDelay=Math.ceil(maxNumberOfInterconnects*nodesPerComputation/interconnect);//could be expressed in form of a distribuition
    //Or add time for interchange?
  } 

  //update communication delay 

}else{ //if data less than ram
  computationPerNode=Math.floor(ram/memoryPerComputation);
  nodesPerComputation=1/computationPerNode;
  
  numberofNodesNeeded= Math.ceil(nodesPerComputation*numberofComputations);
 

  // 2- look for number of interconnects in the software
  if(interconnect<maxNumberOfInterconnects*computationPerNode){
    communicationDelay=Math.ceil(maxNumberOfInterconnects*computationPerNode/interconnect);//could be expressed in form of a distribuition
    //Or add time for interchange?
  } 

  //update communication delay 
}
////questions///
//do i use less memory the
/////////////////

storagePercent= memoryPerComputation*computationPerNode/ram*100;

  

updateVariable("nodesPerComputation", nodesPerComputation,"nodes");
updateVariable("computationPerNode", computationPerNode,"computation");
updateVariable("numberofNodesNeeded", numberofNodesNeeded,"nodes");
updateVariable("communicationDelay", communicationDelay,"frame");
updateVariable("storagePercent", storagePercent,"%");


// 4- compute performance
//    Time of computation
computationTime=computation*computationPerNode/cyclesPerSecond; //?? remove nodes percomp
communicationTime=dataExchangeSize/bitsPerSecond*communicationDelay; //assume all in parallel
updateVariable("computationTime", computationTime/1000,"ms");
updateVariable("communicationTime", communicationTime/1000,"ms");


speed=computationTime+communicationTime; //frame
cost=nodesNumber*costPerChip;
energy= 1 / flopsPerWatt*numberofNodesNeeded*computation;//perframe

updateVariable("speed", speed/1000,"ms/frame");
updateVariable("cost", cost,"dollars");
updateVariable("energy", energy,"watt/frame");


function updateVariable(name, value,unit){
  var oldName=cy.$id(name).data('name');
  cy.$id(name).data('name', oldName+'\n'+value+'\n'+unit);
}


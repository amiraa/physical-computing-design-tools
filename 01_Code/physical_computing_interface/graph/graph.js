// todo: enclose this
function initGraph(){
    //todo change these and put to global
    var color1= "#ffffff"; //white
    var color11= "#ffffff8c"; //white transparent
    var color2= "#020227";  //dark blue
    var color3= "#1c5c61"; //teal
    var color33= "#1c5c618c"; //teal transparent
    var color4= "#fa6e70"; //red/orange
    var color44= "#fa6e708c"; //red/orange
    var color5="#380152"; //purple
    var color6="#696767"; //grey
    var font= "consolas";//'font-family'


    var cy = window.cy = cytoscape({
        container: document.getElementById('cy'),

        ready: function(){
            var api = this.expandCollapse({
                layoutBy: {
                    name: "cose-bilkent",
                    animate: "end",
                    randomize: false,
                    fit: true
                },
                fisheye: true,
                animate: false,
                undoable: false
            });
            api.collapseAll();
        },

        style: [
            {
                selector: 'node',
                style: {
                    'background-color': color3,
                    'content': 'data(name)',
                    // 'text-valign': 'center',
                    "text-wrap": "wrap",
                    'color': color6,
                    'font-family':font,
                    "font-size": 8
                    
                }
            },

            {
                selector: ':parent',
                style: {
                    'background-opacity': 0.333
                }
            },

            {
                selector: "node.cy-expand-collapse-collapsed-node",
                style: {
                    "background-color": color5,
                    "shape": "rectangle"
                }
            },

            {
                selector: 'edge',
                style: {
                    'width': 2,
                    'line-color': color3,
                    'curve-style': 'bezier', //straight
                    'target-arrow-color': color3,
                    'target-arrow-shape': 'vee',
                    'content': 'data(name)',
                    'color': color6,
                    'font-family':font,
                    "font-size": 6
                }
            },

            {
                selector: 'edge.meta',
                style: {
                    'width': 2,
                    'line-color': color4
                }
            },

            {
                selector: ':selected',
                style: {
                    "border-width": 3,
                    'line-color': color4,
                    'target-arrow-color': color4,
                    "border-color": color4
                }
            }
        ],

        elements: {
            nodes: [
            ],
            edges: [
           ]
        },
        

                
    });

    var api = cy.expandCollapse('get');
    //////////////////////////

    //nodes
    cy.cxtmenu({
        selector: 'node',//'node, edge',
        menuRadius: 50,
        fillColor: color11, // the background colour of the menu
        activeFillColor: color44, // the colour used to indicate the selected command
        // activePadding: 20, // additional size in pixels for the active command
        indicatorSize: 20, // the size in pixels of the pointer to the active command
        // separatorWidth: 3, // the empty spacing in pixels between successive commands
        spotlightPadding: 4, // extra spacing in pixels between the element and the spotlight
        minSpotlightRadius: 15, // the minimum radius in pixels of the spotlight
        maxSpotlightRadius: 10, // the maximum radius in pixels of the spotlight
        // openMenuEvents: 'cxttapstart taphold', // space-separated cytoscape events that will open the menu; only `cxttapstart` and/or `taphold` work here
        itemColor: color6, // the colour of text in the command's content
        // itemTextShadowColor: 'transparent', // the text shadow colour of the command's content
        zIndex: 2*9999, // the z-index of the ui div
        // atMouse: false // draw menu at mouse position

        commands: [
            {
                //play //TODO change to event
                content: '<span class="fa fa-play fa-1x"></span>',
                select: function(ele){
                    // runNode(ele);
                    var pos=utils.getXYZfromName(ele.data('id'));
                    GLOBALS.selectNode(pos.x,pos.y,pos.z);
                    GLOBALS.runNode(pos.x,pos.y,pos.z);
                    
                }
            },

            {   //todo remove clutter and put into function
                content: '<span class="fa fa-trash fa-1x"></span>',
                select: function(ele){
                    var tgt = ele; 
                    var pos=utils.getXYZfromName(tgt._private.data.id);
                    if(tgt._private.data.name=="N/A"){
                        tgt.remove();

                    }else{
                        GLOBALS.removeNode(pos.x,pos.y,pos.z);
                    }
                },
                // enabled: true
            },

            {
                // content: 'Select',
                content: '<span class="fa fa-mouse-pointer fa-1x"></span>',
                select: function(ele){
                    var pos=utils.getXYZfromName(ele.data('id'));
                    GLOBALS.selectNode(pos.x,pos.y,pos.z);
                    
                }
            }
        ]
    });

    //expand and collapse and min cut and bottleneck
    cy.cxtmenu({
        selector: 'core',
        menuRadius: 50,
        fillColor: color11, // the background colour of the menu
        activeFillColor: color44, // the colour used to indicate the selected command
        // activePadding: 20, // additional size in pixels for the active command
        indicatorSize: 20, // the size in pixels of the pointer to the active command
        // separatorWidth: 3, // the empty spacing in pixels between successive commands
        spotlightPadding: 4, // extra spacing in pixels between the element and the spotlight
        minSpotlightRadius: 15, // the minimum radius in pixels of the spotlight
        maxSpotlightRadius: 10, // the maximum radius in pixels of the spotlight
        // openMenuEvents: 'cxttapstart taphold', // space-separated cytoscape events that will open the menu; only `cxttapstart` and/or `taphold` work here
        itemColor: color6, // the colour of text in the command's content
        // itemTextShadowColor: 'transparent', // the text shadow colour of the command's content
        zIndex: 2*9999, // the z-index of the ui div
        // atMouse: false // draw menu at mouse position


        commands: [
            {
                //expand
                content: '<span class="fa fa-expand-arrows-alt fa-1.5x"></span>',
                select: function(){
                    cy.edges().unselect();//unselect the rest
                    api.expandAll();
                }
            },

            {
                //min cut //TODO NOT WORKING/RELEVANT
                content: '<span class="fa fa-cut fa-1.5x"></span>',
                select: function(){
                    var ee=cy.edges('[first<1]');
                    // console.log(ee.length);
                    // console.log(cy.elements().length);
                    // console.log(cy.elements().difference(ee).length);
                    cy.edges().unselect();//unselect the rest
                    var ks = cy.elements().components()[0].kargerStein();
                    ks.cut.select();
                    // console.log(ks);
                }
                
            },
            {
                //collapse
                content: '<span class="fa fa-compress-arrows-alt fa-1.5x"></span>',
                select: function(){
                    cy.edges().unselect();//unselect the rest
                    api.collapseAll();
                }
                
            },
            {
                //find bottleneck or max data flow
                content: '<span class="fas fa-wine-bottle fa-1.5x"></span>',
                select: function(){
                    cy.edges().unselect();//unselect the rest
                    var max = cy.edges().max(function(ele){
                        return ele._private.data.flow;
                    });
                    max.ele.select();
                }
                
            }
        ]
    });
    /////////////////////////

    /////////////////neighborhood groups/////////////////
    function addLayerNodes(n){
        for(var i=0;i<n;i++){
            cy.add({
                classes: 'automove-viewport',
                // data: { id: 'new' + Math.round( Math.random() * 100 ) },
                data: { 
                    id: ''+i,
                    name: 'Layer ' +i,
                    data:{
                        id: ''+i,
                        name: 'Layer ' +i
                    }
                },
                position: {
                    x: i*GLOBALS.voxelSpacing,
                    y: i*GLOBALS.voxelSpacing
                }
            });
            if(i>0){
                // cy.add([
                //     { group: "edges", data: { source: ''+i, target: ''+i-1}}
                // ]);

            }

        }
        

    }

    function addNeighborhood(){
        // var i=cy.nodes('[isNeighborhood > 0]').length;
        var i=0;
        while(cy.$id(''+i).length!=0){
            i++;
        }
        cy.add({
            classes: 'automove-viewport',
            // data: { id: 'new' + Math.round( Math.random() * 100 ) },
            data: { 
                id: ''+i,
                name: 'Neighborhood \n' +i,
                isNeighborhood :1,
                data:{
                    id: ''+i,
                    name: 'Neighborhood ' +i,
                }
                
            },
            position: {
                x: i*GLOBALS.voxelSpacing,
                y: i*GLOBALS.voxelSpacing
            }
        });
        return i;

    }

    function findNeighborhood(node){//when add new node
        var neigh=node.neighborhood(function( ele ){
            return ele.isNode();
        });
        // console.log(neigh.length);
        //if no neighbors then add new neighborhood and return its num
        if(neigh.length==0){
            var n=addNeighborhood();
            return n;
        }else if(neigh.length==1){ //if only one just return its num
            return neigh[0]._private.data.data.parent;
        }else {//if multiple
            //look at smallest and return it's num
            var neighValues=[];
            for (var i=0;i<neigh.length;i++){
                neighValues.push(neigh[i]._private.data.data.parent);
            }
            var minNeighValue=Math.min(...neighValues);
            
            //for each of the other neighborhoods
            for (var i=0;i<neigh.length;i++){
                var val=neigh[i]._private.data.data.parent;
                if(val!=minNeighValue){
                    //find successors/connected nodes
                    var successors=neigh[i].successors(function( ele ){
                        return ele.isNode();
                    });
                    //change their parent to the smallest
                    neigh[i].move({parent: ''+minNeighValue});
                    neigh[i]._private.data.data.parent=minNeighValue;//don't need just make sure in case not in successor

                    successors.move({parent: ''+minNeighValue});
                    for (var j=0;j<successors.length;j++){
                        successors[i]._private.data.data.parent=minNeighValue;
                    }
                    //delete the group
                    var tgt=cy.$id(''+val);
                    tgt.remove();
                }
            }
            return minNeighValue;
        }

    }

    function updateNeighborhood(node){//when delete node
        var neigh=node.neighborhood(function( ele ){
            return ele.isNode();
        });
        var val=node._private.data.data.parent;
        node.remove();
        if(neigh.length==0){//if no neighbors 
            //delete neighborhood
            var tgt=cy.$id(''+val);
            tgt.remove();
        } else if(neigh.length>1){ //if one neighbor do nothing
            //if more than one
            //check for each two
            for (var i=0;i<neigh.length;i++){
                for (var j=i+1;j<neigh.length;j++){
                    //if not the same
                    if(neigh[i].id()!==neigh[j].id()){
                        var id=neigh[j].id();
                        var successors=neigh[i].successors(function( ele ){
                            return ele.isNode();
                        });
                        var f=successors.$id(neigh[j].id());
                        if(f.length==0) {//if not connected //if connected do nothing
                            //one part do nothing (j do noting)
                            //other part create new neighborhood and add them to it
                            var n=addNeighborhood();
                            neigh[i].move({parent: ''+n});
                            neigh[i]._private.data.data.parent=n;
                            
                            successors.move({parent: ''+n});
                            for (var k=0;k<successors.length;k++){
                                successors[k]._private.data.data.parent=n;
                            }
                        }                            
                    }
                }
            }
        }
    }

    //todo: fix updateNeighborhood so I don't need this
    function pruneNeighborhoods(){
        var neighborhoods=cy.nodes('[isNeighborhood > 0]');
        for (var i=0;i<neighborhoods.length;i++){
            // console.log(neighborhoods[i].children().length);
            if(neighborhoods[i].children().length==0){
                neighborhoods[i].remove();
            }
        }
    }

    //////////////////////////////////////////////////
    
    //select node
    cy.on('tap', 'node', function(){
        // var nodes = this;
        // GLOBALS.selectedjson=this._private.data.data;
        var pos=utils.getXYZfromName(this.data('id'));
        GLOBALS.selectNode(pos.x,pos.y,pos.z);
        // this.trigger('blaa');
    });

    document.addEventListener('selectNode', function (e) { 
        
        var tgt=cy.$id(e.detail.id);
        cy.nodes().unselect();//unselect the rest
        tgt.select();
        GLOBALS.selectedjson=tgt._private.data.data;
        
    }, false);

    //select edge
    //TODO: FIX
    cy.on('tap', 'edge', function(){
        var edge = this;
        var pos=utils.getXYZfromName(this.data('id'));
        // GLOBALS.selectNode();
        cy.edges().unselect();//unselect the rest
        edge.select();
        GLOBALS.selectedjson=edge._private.data;
        GLOBALS.selectEdge();
        

    });

    //TODO: fix add node to random location threejs
    cy.on('tap', function( evt ){
        var tgt = evt.target || evt.cyTarget; // 3.x || 2.x

        if( tgt === cy ){
            // console.log( evt.position.x+" "+evt.position.y);
            cy.add({
                classes: 'automove-viewport',
                data: { 
                    id: 'new' + Math.round( Math.random() * 100 ) ,
                    name:"N/A",
                    data:{
                        name:"N/A"

                    }
                },
                position: {
                    x: evt.position.x,
                    y: evt.position.y
                }
            });
        }
    });

    document.addEventListener('addNode', function (e) { 
        cy.edges().unselect();//unselect the rest

        var neighborhood=0;
        
        cy.add({
            classes: 'automove-viewport',
            // data: { id: 'new' + Math.round( Math.random() * 100 ) },
            data: { 
                id: e.detail.id,
                name: '[' +e.detail.x +"," +e.detail.y+"," +e.detail.z+']\n'+0,
                parent: ''+e.detail.z,
                isNeighborhood :0,
                data:{
                    id: e.detail.id,
                    name: '[' +e.detail.x +"," +e.detail.y+"," +e.detail.z+']',
                    type: "rigid",
                    parent: neighborhood,
                    code: "Math.max(...inValues)",
                    state:Math.floor(Math.random() * 100),
                    locked:false,
                    runOnce:true,
                    neighbors:[],
                    inValues:[],
                    outValues:[],
                    numRuns:0,
                    maxRuns:10,
                    dnn:e.detail.data
                }
            },
            position: {
                x: e.detail.posX,
                y: e.detail.posY,
            }
        });
        

        api.expandAll();

        

        addEdges(e.detail.x,e.detail.y,e.detail.z);

        var tgt=cy.$id('[' +e.detail.x +"," +e.detail.y+","+e.detail.z+']');
        updateName(tgt);

        neighborhood=findNeighborhood(tgt);

        tgt.move({parent: ''+neighborhood});
        tgt._private.data.data.parent= neighborhood;

        api.expandAll();
        
    }, false);


    document.addEventListener('changeNodeColor', function (e) { 
        var tgt=cy.$id('[' +e.detail.x +"," +e.detail.y+","+e.detail.z+']');
        tgt._private.data.data.type= e.detail.data.type;
        // console.log(tgt._private.data.data)
        // console.log(e.detail.data.type)

    }, false);


    //todo: change edge ID??
    function addEdges(x,y,z){
        x=parseInt(x);
        y=parseInt(y);
        z=parseInt(z);
        var list=utils.getNeighborsList(GLOBALS.grid,x,y,z);
        
        for(var i=0;i<list.length;i++){
            var i1,j1,k1;
            [i1,j1,k1]=list[i];
            if (GLOBALS.occupancy[i1][j1][k1]) {
                var tgt=cy.$id('[' +x +"," +y+","+z+']');
                var valIn=tgt._private.data.data.state;
                tgt._private.data.data.neighbors.push('[' +i1 +"," +j1+","+k1+']');
                tgt=cy.$id('[' +i1 +"," +j1+","+k1+']');
                var valOut=tgt._private.data.data.state;
                tgt._private.data.data.neighbors.push('[' +x +"," +y+","+z+']');

                cy.add([
                    { group: "edges",data: { name:valIn, source: '[' +x +"," +y+","+z+']', target: '[' +i1 +"," +j1+","+k1+']',
                    id:'[' +x +"," +y+","+z+']'+'to[' +i1 +"," +j1+","+k1+']',flow:valIn,flowRate:20/valIn,first:0,weight:valIn}}
                ]);
                cy.add([
                    { group: "edges",data: { name:valOut, target: '[' +x +"," +y+","+z+']', source: '[' +i1 +"," +j1+","+k1+']',
                    id:'[' +i1 +"," +j1+","+k1+']'+'to[' +x +"," +y+","+z+']',flow:valOut,flowRate:20/valOut,first:1,weight:valIn}}
                ]);
            }
        }
    }

    //remove node
    cy.on('cxttap', 'node', function( evt ){
        var tgt = evt.target || evt.cyTarget; // 3.x || 2.x
        // console.log(tgt._private.data.id);
        var pos=utils.getXYZfromName(tgt._private.data.id);
        if(tgt._private.data.name=="N/A"){
            tgt.remove();

        }else{
            GLOBALS.removeNode(pos.x,pos.y,pos.z);
        }
        
    });


    document.addEventListener('removeNode', function (e) { 
        // console.log("Remove:"+e.detail.x+" "+e.detail.y+" "+e.detail.z);
        removeNeighborsGraph(e.detail.x,e.detail.y,e.detail.z);
        var tgt=cy.$id(e.detail.id);
        updateNeighborhood(tgt);
        //prune neighborhoods
        pruneNeighborhoods();
        api.expandAll();
        //remove from Neighbor's graph
        // tgt.remove();//removed in updateNeighborhood
        
    }, false);

    //TODO: NEIGHBORS FOR LAYERS
    function removeNeighborsGraph(x,y,z){
        x=parseInt(x);
        y=parseInt(y);
        z=parseInt(z);
        var list=utils.getNeighborsList(GLOBALS.grid,x,y,z);
        
        for(var i=0;i<list.length;i++){
            var i1,j1,k1;
            [i1,j1,k1]=list[i];
            if (GLOBALS.occupancy[i1][j1][k1]) {
                var value='[' +x +"," +y+","+z+']';
                tgt=cy.$id('[' +i1 +"," +j1+","+k1+']');
                tgt._private.data.data.neighbors = tgt._private.data.data.neighbors.filter(item => item !== value);
                
            }
        }


    }

    ////////

    //////////UPDATE NODE JSON AND RUN

    document.addEventListener('updateNode', function (e) { 
        var tgt=cy.$id(e.detail.id);
        var data=e.detail;
        // tgt.data('data',data); //todo:later change more than code
        tgt._private.data.data.code=data.code;
        tgt._private.data.data.locked=false;
        tgt._private.data.data.numRuns=0;
        tgt._private.data.data.dnn=data.dnn;

        runNode(tgt); //todo: call event instead??
    
    }, false);

    ////////RUN NODE
    document.addEventListener('runNode', function (e) { 
        var tgt=cy.$id(e.detail.id);
        runNode(tgt); //local run node //TODO CHANGE ADD CLOSURE
    
    }, false);

    function runNode(node){
        // console.log("run node "+node.id());
        if(!node._private.data.data.locked){
            // var neigh=tgt.neighborhood(function( ele ){
            //     return ele.isNode();
            // });
            // var outEdges=node.outgoers(function( ele ){
            //     return ele.isEdge();
            // });
            var outNodes=node.outgoers(function( ele ){
                return ele.isNode();
            });
            var inEdges=node.incomers(function( ele ){
                return ele.isEdge();
            });
            var inNodes=node.incomers(function( ele ){
                return ele.isNode();
            });
            
            //reduce input values list to one list
            //TODO find a smarter way to do this
            var initialValue = node._private.data.data.state;
            var fn = function( prevVal, ele, i, eles ){
            if( prevVal ){
                return prevVal + ',' + ele.data('name');
                } else {
                    return ele.id();
                }
            };
            var inValues = inEdges.reduce( fn, initialValue );
            // if(inValues!=initialValue){
                inValues = inValues.split(",");
                inValues = inValues.map(function (x) { 
                    return parseInt(x); });
                // console.log(inValues);
                node._private.data.data.inValues=inValues; //update inValues //TODO:move elsewhere??
            // }
            
            
            node._private.data.data.state=eval(node._private.data.data.code);

            
            

            updateName(node);
            node._private.data.data.numRuns++;
            if(node._private.data.data.numRuns==node._private.data.data.maxRuns){
                node._private.data.data.locked=true;
            }

            // todo: later run available neighbor 
            for(var i=0;i<outNodes.length;i++){
                // if(!outNodes[i]._private.data.data.triggered){
                    runNode(outNodes[i]);
                // }
                // outEdges[i].data('name', node._private.data.data.state);
            }


            //select
            GLOBALS.selectedjson=node.data('data');
            var pos=utils.getXYZfromName(node.data('id'));
            GLOBALS.selectNode(pos.x,pos.y,pos.z);

        }
        
    }

    function updateName(node){
        node.data('name', node._private.data.data.name+'\n'+node._private.data.data.state);
        var outEdges=node.outgoers(function( ele ){
            return ele.isEdge();
        });
        for(var i=0;i<outEdges.length;i++){
            outEdges[i].data('name', node._private.data.data.state);
        }

        //reduce output values list to one list
        var initialValue = node._private.data.data.state;
        var fn = function( prevVal, ele, i, eles ){
        if( prevVal ){
            return prevVal + ',' + ele.data('name');
            } else {
                return ele.id();
            }
        };
        var outValues = outEdges.reduce( fn, initialValue );
        if(outValues!=initialValue){
            outValues = outValues.split(",");
            outValues = outValues.map(function (x) { 
                return parseInt(x); });
            node._private.data.data.outValues=outValues;
        }
        
        // node._private.data.name= node._private.data.data.name+'\n'+node._private.data.data.state;
    }

    ////////try custom events////
    cy.on('blaa', 'node', function(evt) { 
        var tgt = evt.target || evt.cyTarget;
        console.log("hahablaa");
        console.log(tgt.id());
    });

}







// field.js
// A representation of the whole node grid.
var an = AddNode,
    sn = SubNode,
    sqrtn = SqrtNode,
    mn = MulNode,
    dn = DivNode,
    pn = PowNode,
    cn = ConstNode,
    nn = NopNode,
    max= MaxNode,
    min= MinNode,
    sin= SinNode,
    cos=CosNode;

var nodeTypes = [cn, an, sn, nn, mn, dn, pn, sqrtn, max, min, sin, cos];

var Field = function(inputs, width, height) {
    this.width = width;
    this.height = height;
    this.inputs = inputs;

    this.nodes = inputs.concat(new Array(width * height));
    this.genome=[];
    this.unformattedGenome=[];
};

// Creates a random start field.
Field.prototype.initialize = function() {
    var len = this.nodes.length;
    var inputLen = this.inputs.length;

    for (var i = this.inputs.length; i < this.nodes.length; i++) {
        var nodeIndex=Math.floor(Math.random() * nodeTypes.length);

        var nodeType = nodeTypes[nodeIndex];
        var tempN=new nodeType(0,0);
        this.nodes[i] = tempN.randomNode(this.getMaxIndex(i));

        this.genome.push([ nodeIndex, this.nodes[i].getEdges()]);
        this.unformattedGenome.push(nodeIndex, this.nodes[i].getEdges()[0],this.nodes[i].getEdges()[1]);

    }

    var pos = Math.floor(Math.random() * len);
    this.output = new nn(pos);

    this.genome.push([pos]);
    this.unformattedGenome.push(pos);
};

Field.prototype.getUnformattedGenome=function(){
    return this.unformattedGenome;
};

// Creates a field from genome.
Field.prototype.initializeWithGenome = function(genome) {
    var len = this.nodes.length;
    var inputLen = this.inputs.length;

    for (var i = 0; i < this.nodes.length-this.inputs.length; i++) {
        // console.log(genome[i]);
        var nodeIndex=genome[i][0];
        var nodeType = nodeTypes[nodeIndex];
        this.nodes[i+this.inputs.length]=new nodeType(genome[i][1][0],genome[i][1][1]);
    }

    this.genome=genome;
    var pos=genome[this.nodes.length-this.inputs.length][0];
    this.output = new nn(pos);
    
};

// Get the maximal source index for a given position.
Field.prototype.getMaxIndex = function(pos) {
    return Math.floor((pos - this.inputs.length) / this.height) *
           this.height + this.inputs.length - 1;
};

Field.prototype.clone = function() {
    var field = new Field(this.inputs, this.width, this.height);

    for (var i = this.inputs.length; i < this.nodes.length; i++) {
        field.nodes[i] = this.nodes[i].clone();
    }

    return field;
};

Field.prototype.mutate = function(factor) {
    var len = this.nodes.length;
    var inputLen = this.inputs.length;

    for (var i = 0; i < (factor * len); i++) {
        var pos = Math.floor(Math.random() * (len - inputLen + 1)) + inputLen;

        if (pos === len) {
            var outPos = Math.floor(Math.random() * len);
            this.output = new nn(outPos);
        } else {
            var nodeType = nodeTypes[Math.floor(Math.random() * nodeTypes.length)];
            var tempN=new nodeType(0,1);
            this.nodes[pos] = tempN.randomNode(this.getMaxIndex(pos));
        }
    }
};

Field.prototype.rateFitness = function(insets, outset) {
    // assume that len(insets[i]) === len(outset)

    var fitness = 0.0;

    for (var i = 0; i < insets.length; i++) {
        for (var j = 0; j < insets[i].length; j++) {
            this.nodes[j].setValue(insets[i][j]);
        }

        var val = this.output.calculate(this);
        fitness += Math.abs((isNaN(val) ? Infinity : val) - outset[i]);
    }

    this.fitness = fitness;
};
// module.exports = Field;


Field.prototype.printGenome= function() {
    genome=" ";
    for(var i=0;i<this.genome.length;++i){
        m=19-i;
        m=this.inputs.length+i;
        genome+=m+": ";
        for(var j=0;j<this.genome[i].length;++j){
            if(this.genome[i][j].length>1){
                for(var k=0;k<this.genome[i][j].length;++k){
                    if(k+1<this.genome[i][j].length){
                        genome+=this.genome[i][j][k]+"-";
                    }else{
                        genome+=this.genome[i][j][k]+" ";
                    }
                }
            }else{
                genome+=this.genome[i][j]+" ";
            }
        }
        genome+="/ ";
    }
    // console.log(genome);
};

Field.prototype.printGenome1= function() {
    genome="[";
    for(var i=0;i<this.genome.length;++i){
        genome+="[";
        for(var j=0;j<this.genome[i].length;++j){
            
            if(this.genome[i][j].length>1){
                genome+="[";
                for(var k=0;k<this.genome[i][j].length;++k){
                    if(k+1<this.genome[i][j].length){
                        genome+=this.genome[i][j][k]+",";
                    }else{
                        genome+=this.genome[i][j][k]+"";
                    }
                }
                genome+="]";
            }else{
                genome+=this.genome[i][j]+",";
            }
            
            
        }
        genome+="],";
    }
    genome+="]";
    // console.log(genome);
};

'use strict';
var data = {};
var CGP= {};
var best;
var net= {};

data.init= function() {
    data.generations = 100.0;
    data.mutations = 100.0;
    data.rate = 10;
    data.initNum = 1000;
    data.height = 1;
    data.width = 10;
    data.inputs = [[1],[2],[3]];
    data.outputs = [[1],[4],[9]];
    // data.inputs = [[1],[4],[8]];
    // data.outputs = [[-8],[-4],[15]];

};

// CGP.init= function()
{
    data.init();

    var Field = Field,
        vn =   VarNode;

    var fields = new Array(data.initNum);

    for (var i = 0; i < fields.length; i++) {
        fields[i] = new Field([new vn('x')], data.width, data.height);
        fields[i].initialize();
        fields[i].rateFitness(data.inputs, data.outputs);
    }

    fields = fields.sort(function(a, b) {
        return a.fitness - b.fitness;
    });



    best = generations(fields[0], data.generations,
        data.mutations,
        data.rate,
        data.inputs, data.outputs);
  
    var term = best.output.toString(best);
    var fitness = best.fitness;

    document.getElementById("term").innerHTML=`${term}, Fitness: ${fitness}`;
    console.log(best);

    functionPlot({
        target: '#graph',
        width: 1000,
        height: 500,
        text:[{color: 'white'}],
        data: [{
            fn: term, color: '#56ffcc',
            nSamples: 500,
            graphType: 'polyline'

        },
        {
            points: zip([data.inputs, data.outputs]),
            fnType: 'points',
            color: '#ff5c56',
            graphType: 'scatter'
        }]
    });

    function zip(arrays) {
        return arrays[0].map(function(_,i){
            return arrays.map(function(array){return array[i];});
        });
    }
}

net.init= function() {
    
    var nodes=[];
    var edges=[];
    var inputColor='#56ffcc', 
        outputColor='#ff5c56',
        activeColor='#d7dfea',
        inactiveColor='#5e6060';

    var checked=[];
    var smoothOption="cubicBezier";//"cubicBezier";//"dynamic",//"curvedCCW"

    for(var i=0; i<best.nodes.length;i++) {
        checked.push(0);
        nodes.push({id: i, label: best.nodes[i].getName(), color: inactiveColor});//, group: best.nodes[i].getGroup()});
        var tempEdges= best.nodes[i].getEdges();
        for(var j=0; j<tempEdges.length;j++) {
            edges.push({from: i, to: best.nodes[i].getEdges()[j],smooth:smoothOption});
        }
    }

    //output node
    nodes.push({id: best.nodes.length, label: "output", color: outputColor});//, group: 9});
    edges.push({from: best.nodes.length, to: best.output.getEdges()[0],smooth:smoothOption});

    highlightActiveNodes( best.output.getEdges()[0]);
    nodes[0].color=inputColor;

    console.log(nodes);
    console.log(edges);


    // create a network
    var container = document.getElementById('mynetwork');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        nodes: {
            shape: 'dot',
            size: 12,
            font: {
                size: 24,
                color: '#ffffff'
            },
            borderWidth: 0
        },
        edges: {
            width: 2,
            smooth:"true"
        },
        layout: {
            hierarchical: {
              direction: "RL",//"UD"
              sortMethod:"directed",//"hubsize"
              shakeTowards:"roots",//"leaves"
            },
          },
    };
    var network = new vis.Network(container, data, options);
    ////////////////////////////////////////////////////////////////////////////////

    function highlightActiveNodes(n) {
        console.log("check "+nodes[n].label);
        nodes[n].color=activeColor; //light gray
        
        if(!checked[n])
        {
            checked[n]=1;

            var tempEdges= best.nodes[n].getEdges();
            for(var j=0; j<tempEdges.length;j++) {
                highlightActiveNodes(tempEdges[j]);
            }

        }
    }

}


// CGP.init();
net.init();


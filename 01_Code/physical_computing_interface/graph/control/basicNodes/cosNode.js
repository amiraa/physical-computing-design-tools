// cosNode.js
// A node that computes the cos of x.

var CosNode = function(inNode) {
    this.inNode = inNode;
};

CosNode.prototype.calculate = function(field) {
    return Math.cos(field.nodes[this.inNode].calculate(field));
};

CosNode.prototype.toString = function(field) {
    return "cos(" + field.nodes[this.inNode].toString(field) + ")";
};

CosNode.prototype.clone = function() {
    return new CosNode(this.inNode);
};

CosNode.prototype.randomNode = function(newPos) {
    var newIn = Math.floor(Math.random() * (newPos + 1));

    return new CosNode(newIn);
};

CosNode.prototype.getName = function() {
    return "cos";
};

CosNode.prototype.getGroup = function() {
    return 12;
};

CosNode.prototype.getEdges = function() {
    return [this.inNode];
};

// module.exports.CosNode = CosNode;
// module.exports.randomNode = randomNode;

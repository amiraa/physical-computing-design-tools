// sqrtNode.js
// A node that computes the sqrt of x.

var SqrtNode = function(inNode) {
    this.inNode = inNode;
};

SqrtNode.prototype.calculate = function(field) {
    return Math.sqrt(field.nodes[this.inNode].calculate(field));
};

SqrtNode.prototype.toString = function(field) {
    return "sqrt(" + field.nodes[this.inNode].toString(field) + ")";
};

SqrtNode.prototype.clone = function() {
    return new SqrtNode(this.inNode);
};

SqrtNode.prototype.randomNode = function(newPos) {
    var newIn = Math.floor(Math.random() * (newPos + 1));

    return new SqrtNode(newIn);
};

SqrtNode.prototype.getName = function() {
    return "√";
};

SqrtNode.prototype.getGroup = function() {
    return 6;
};

SqrtNode.prototype.getEdges = function() {
    return [this.inNode];
};

// module.exports.SqrtNode = SqrtNode;
// module.exports.randomNode = randomNode;

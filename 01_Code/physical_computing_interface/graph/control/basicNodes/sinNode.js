// sinNode.js
// A node that computes the sin of x.

var SinNode = function(inNode) {
    this.inNode = inNode;
};

SinNode.prototype.calculate = function(field) {
    return Math.sin(field.nodes[this.inNode].calculate(field));
};

SinNode.prototype.toString = function(field) {
    return "sin(" + field.nodes[this.inNode].toString(field) + ")";
};

SinNode.prototype.clone = function() {
    return new SinNode(this.inNode);
};

SinNode.prototype.randomNode = function(newPos) {
    var newIn = Math.floor(Math.random() * (newPos + 1));

    return new SinNode(newIn);
};

SinNode.prototype.getName = function() {
    return "sin";
};

SinNode.prototype.getGroup = function() {
    return 11;
};

SinNode.prototype.getEdges = function() {
    return [this.inNode];
};

// module.exports.SinNode = SinNode;
// module.exports.randomNode = randomNode;

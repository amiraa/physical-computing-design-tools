// minNode.js
// A node that computes the min of x to the y.

var MinNode = function(in1, in2) {
    this.in1 = in1;
    this.in2 = in2;
};

MinNode.prototype.calculate = function(field) {
    return Math.min(field.nodes[this.in1].calculate(field),
                    field.nodes[this.in2].calculate(field));
};

MinNode.prototype.toString = function(field) {
    return "min(" + field.nodes[this.in1].toString(field) + ", " +
           field.nodes[this.in2].toString(field) + ")";
};

MinNode.prototype.clone = function() {
    return new MinNode(this.in1, this.in2);
};

MinNode.prototype.randomNode = function(newPos) {
    var newIn1 = Math.floor(Math.random() * (newPos + 1));
    var newIn2 = Math.floor(Math.random() * (newPos + 1));

    return new MinNode(newIn1, newIn2);
};

MinNode.prototype.getName = function() {
    return "min";
};

MinNode.prototype.getGroup = function() {
    return 10;
};

MinNode.prototype.getEdges = function() {
    return [this.in1, this.in2];
};



// module.exports.MinNode = MinNode;
// module.exports.randomNode = randomNode;

// maxNode.js
// A node that computes the max of x to the y.

var MaxNode = function(in1, in2) {
    this.in1 = in1;
    this.in2 = in2;
};

MaxNode.prototype.calculate = function(field) {
    return Math.max(field.nodes[this.in1].calculate(field),
                    field.nodes[this.in2].calculate(field));
};

MaxNode.prototype.toString = function(field) {
    return "max(" + field.nodes[this.in1].toString(field) + ", " +
           field.nodes[this.in2].toString(field) + ")";
};

MaxNode.prototype.clone = function() {
    return new MaxNode(this.in1, this.in2);
};

MaxNode.prototype.randomNode = function(newPos) {
    var newIn1 = Math.floor(Math.random() * (newPos + 1));
    var newIn2 = Math.floor(Math.random() * (newPos + 1));

    return new MaxNode(newIn1, newIn2);
};

MaxNode.prototype.getName = function() {
    return "max";
};

MaxNode.prototype.getGroup = function() {
    return 9;
};

MaxNode.prototype.getEdges = function() {
    return [this.in1, this.in2];
};



// module.exports.MaxNode = MaxNode;
// module.exports.randomNode = randomNode;

'use strict';
var data = {};
var CGP= {};
var bestX,bestY,bestZ;
var net= {};
var netY= {};
var netZ= {};
var inputs;

data.init= function() {
    data.generations = 100.0;
    data.mutations = 100.0;
    data.rate = 10;
    data.initNum = 1000;
    data.height = 1;
    data.width = 11;
    data.inputs = [[1],[2],[3]];
    data.outputs = [[1],[4],[9]];
    // data.inputs = [[1],[4],[8]];
    // data.outputs = [[-8],[-4],[15]];

};



// var nodeTypes = [
//     ConstNode,//0 
//     AddNode,  //1 
//     SubNode,  //2 
//     NopNode,  //3
//     MulNode,  //4 
//     DivNode,  //5
//     PowNode,  //6
//     SqrtNode, //7 
//     MaxNode,  //8
//     MinNode,  //9
//     SinNode,  //10
//     CosNode,  //11
// ];

// CGP.init= function()
{
    data.init();

    var Field = Field,
        vn =   VarNode;

    //Old Optimization
    // var fields = new Array(data.initNum);

    // for (var i = 0; i < fields.length; i++) {
    //     fields[i] = new Field([new vn('x')], data.width, data.height);
    //     fields[i].initialize();
    //     fields[i].rateFitness(data.inputs, data.outputs);
    // }

    // fields = fields.sort(function(a, b) {
    //     return a.fitness - b.fitness;
    // });

    // best = generations(fields[0], data.generations,
    //     data.mutations,
    //     data.rate,
    //     data.inputs, data.outputs);
    ///////////////////////////////////////////

    //get random genome
    // best=new Field([new vn('x')], data.width, data.height);
    // best.initialize();
    // best.rateFitness(data.inputs, data.outputs);
    // console.log(best.genome)
    // best.printGenome1()
    //////

    //get from genome


    // var genome=[
    //     [0,[0,0]],//0
    //     [1,[0,0]],//1
    //     [2,[0,0]],//2
    //     [3,[0,0]],//3
    //     [4,[0,0]],//4
    //     [5,[0,0]],//5
    //     [6,[0,0]],//6
    //     [7,[0,0]],//7
    //     [8,[0,0]],//8
    //     [9,[0,0]],//9
    //     [0] //10 output
    // ];

    // var genome=[
    //     [0,[0,1]],//0
    //     [1,[0,1]],//1
    //     [2,[0,1]],//2
    //     [3,[0,1]],//3
    //     [4,[0,1]],//4
    //     [5,[0,1]],//5
    //     [6,[0,1]],//6
    //     [7,[0,1]],//7
    //     [8,[0,1]],//8
    //     [9,[0,1]],//9
    //     [0] //10 output
    // ];

    inputs=[new vn('timeStep'),new vn('position'),new vn('id')];


    var genomeX=[
        [0,[3.14159265359]],//3 pi
        [0,[0.1]],//4 a0 
        [0,[5e-5]],//5 f
        [0,[0.25]],//6 amp
        [0,[0.0]],//7 2.0
        [4,[3,7]],//8 pi*2.0
        [4,[0,4]],//9 t*f
        [1,[9,6]],//10 (t*f) + amp
        [4,[10,8]],//11 (pi*2.0) * ((t*f) + amp)
        [10,[11]],//12 sin(((pi*2.0) * ((t*f) + amp)))
        [4,[12,4]],//13 a0*(sin(((pi*2.0) * ((t*f) + amp))))
        [7] //14 output
    ];

    bestX=new Field(inputs, data.width, data.height);
    bestX.initializeWithGenome(genomeX);
    bestX.rateFitness(data.inputs, data.outputs);

    var genomeY=[
        [0,[3.14159265359]],//3 pi
        [0,[0.1]],//4 a0 
        [0,[5e-5]],//5 f
        [0,[0.25]],//6 amp
        [0,[2.0]],//7 2.0
        [4,[3,7]],//8 pi*2.0
        [4,[0,5]],//9 t*f
        [1,[9,6]],//10 (t*f) + amp
        [4,[10,8]],//11 (pi*2.0) * ((t*f) + amp)
        [10,[11]],//12 sin(((pi*2.0) * ((t*f) + amp)))
        [4,[12,4]],//13 a0*(sin(((pi*2.0) * ((t*f) + amp))))
        [13] //14 output
    ];
    bestY=new Field(inputs, data.width, data.height);
    bestY.initializeWithGenome(genomeY);
    bestY.rateFitness(data.inputs, data.outputs);


    var genomeZ=[
        [0,[3.14159265359]],//3 pi
        [0,[0.1]],//4 a0 
        [0,[5e-5]],//5 f
        [0,[0.25]],//6 amp
        [0,[2.0]],//7 2.0
        [4,[3,7]],//8 pi*2.0
        [4,[0,5]],//9 t*f
        [1,[9,6]],//10 (t*f) + amp
        [4,[10,8]],//11 (pi*2.0) * ((t*f) + amp)
        [11,[11]],//12 cos(((pi*2.0) * ((t*f) + amp)))
        [4,[12,4]],//13 a0*(sin(((pi*2.0) * ((t*f) + amp))))
        [13] //14 output
    ];
    bestZ=new Field(inputs, data.width, data.height);
    bestZ.initializeWithGenome(genomeZ);
    bestZ.rateFitness(data.inputs, data.outputs);

    //////
    var termX = bestX.output.toString(bestX);
    var termY = bestY.output.toString(bestY);
    var termZ = bestZ.output.toString(bestZ);
    // var fitness = best.fitness;
    console.log(bestX)
    document.getElementById("termX").innerHTML=`Force X= ${termX}`;
    document.getElementById("termY").innerHTML=`Force Y= ${termY}`;
    document.getElementById("termZ").innerHTML=`Force Z= ${termZ}`;


    // console.log(best);

    functionPlot({
        target: '#graph',
        width: 1000,
        height: 500,
        text:[{color: 'white'}],
        data: [{
            fn: termX, color: '#56ffcc',
            nSamples: 500,
            graphType: 'polyline'

        },
        {
            points: zip([data.inputs, data.outputs]),
            fnType: 'points',
            color: '#ff5c56',
            graphType: 'scatter'
        }]
    });

    function zip(arrays) {
        return arrays[0].map(function(_,i){
            return arrays.map(function(array){return array[i];});
        });
    }
}

net.init= function(best,containerName) {
    
    var nodes=[];
    var edges=[];
    var inputColor='#56ffcc', 
        outputColor='#ff5c56',
        activeColor='#d7dfea',
        inactiveColor='#5e6060';

    var checked=[];
    var smoothOption="cubicBezier";//"cubicBezier";//"dynamic",//"curvedCCW"

    for(var i=0; i<best.nodes.length;i++) {
        checked.push(0);
        nodes.push({id: i, label: best.nodes[i].getName(), color: inactiveColor});//, group: best.nodes[i].getGroup()});
        var tempEdges= best.nodes[i].getEdges();
        for(var j=0; j<tempEdges.length;j++) {
            edges.push({from: i, to: best.nodes[i].getEdges()[j],smooth:smoothOption});
        }
    }

    //output node
    nodes.push({id: best.nodes.length, label: "output", color: outputColor});//, group: 9});
    edges.push({from: best.nodes.length, to: best.output.getEdges()[0],smooth:smoothOption});

    highlightActiveNodes( best.output.getEdges()[0]);
    for(var i=0; i<inputs.length;i++) {
        nodes[i].color=inputColor;
    }
    

    // console.log(nodes);
    // console.log(edges);


    // create a network
    var container = document.getElementById(containerName);
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        nodes: {
            shape: 'dot',
            size: 12,
            font: {
                size: 24,
                color: '#ffffff'
            },
            borderWidth: 0
        },
        edges: {
            width: 2,
            smooth:true
        },
        layout: {
            hierarchical: {
              direction: "RL",//"UD"
              sortMethod:"directed",//"hubsize"
              shakeTowards:"roots",//"leaves"
            },
          },
    };
    var network = new vis.Network(container, data, options);
    ////////////////////////////////////////////////////////////////////////////////

    function highlightActiveNodes(n) {
        // console.log("check "+nodes[n].label);
        nodes[n].color=activeColor; //light gray
        
        if(!checked[n])
        {
            checked[n]=1;

            var tempEdges= best.nodes[n].getEdges();
            for(var j=0; j<tempEdges.length;j++) {
                highlightActiveNodes(tempEdges[j]);
            }

        }
    }

}


// CGP.init();
net.init(bestX,"mynetworkX");
net.init(bestY,"mynetworkY");
net.init(bestZ,"mynetworkZ");


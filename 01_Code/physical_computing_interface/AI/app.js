// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

/////////////////function calls/////////////////
//todo when calling say which gridsize and grid type
var utils= new utilities();
var GLOBALS=new globals(utils);

// 
var three=new threejs(GLOBALS,utils,'webgl','threejs1');
three.init();


initGraph();// todo enclose into class
initEditor();// todo enclose into class

var assembler= new Assembler(three,GLOBALS,1,50,[new THREE.Vector3(0,0,0)],[new THREE.Vector3(GLOBALS.gridSize/2.0*GLOBALS.voxelSpacing,0,0)]);
assembler.run();
var data={
    color:GLOBALS.color3
};
//GLOBALS.color3
offsetX=0;
offsetY=5;
for(var i=0;i<6;i++){
    for(var j=0;j<11;j++){
        GLOBALS.addNode(i+offsetX,j+offsetY,0,false,data);
    }
}
data={
    color:GLOBALS.color5
};


//0008.out
var nodes=[
     [0, 0], 
     [0, 1], 
     [0, 2], 
     [0, 3], 
     [0, 4], 
     [0, 5], 
     [5, 1], 
     [5, 2], 
     [5, 4], 
     [10, 3]
];

// //0072.out
// var nodes=[
//    [ 0, 0], 
//    [ 0, 1], 
//    [ 0, 2], 
//    [ 0, 3], 
//    [ 0, 4], 
//    [ 0, 5], 
//    [ 2, 1], 
//    [ 2, 2], 
//    [ 2, 4], 
//    [ 4, 3], 
//    [ 4, 5], 
//    [ 6, 4], 
//    [ 8, 2], 
//    [ 8, 4], 
//    [10, 3]
// ];

//0336.out
var nodes=[
   [0.0, 0], 
   [0.0, 1], 
   [0.0, 2], 
   [0.0, 3], 
   [0.0, 4], 
   [0.0, 5], 
   [1, 0],
   [1, 1],
   [1, 2],
   [1, 3],
   [1, 4],
   [1, 5],
   [2, 0], 
   [2, 1], 
   [2, 3], 
   [2, 5], 
   [3, 1], 
   [3, 3], 
   [3, 5], 
   [4, 1], 
   [4, 3], 
   [4, 5], 
   [5, 2], 
   [5, 4], 
   [6, 3], 
   [7, 2], 
   [7, 4], 
   [8, 1], 
   [8, 3], 
   [8, 5], 
   [9, 2], 
   [10, 3]
];

for(var c=0;c<nodes.length;c++){
    i=nodes[c][1];
    j=nodes[c][0];
    GLOBALS.changeNodeColor(i+offsetX,j+offsetY,0,false,data);
}

var full_demo_settings = {
    divName: 'intro_demo',
    smallMode: false,
    timeLimit: 3000,
    useFineTune: false,
    drawBar: true,
    showInstructions: true,
    hardMode: false,
    aVec: [1,1,1,1,1,1,7,7,5,1,5,5,4,1,7,3,9,1,3,7,9,5,4,3,9,7,1,7,1],
    wKey: [10,35,36,41,64,69,95,97,108,125,128,142,157,202,231,257,289,302,331,361,362,363,364,367,368,373,374,376,394,395,398,401,403,425,461,484,517,543,574,576,602,603,604,606,633,662,692,722,723,753,782,811],
    weights: [-0.1783,-0.0303,1.5435,1.8088,-0.857,1.024,-0.3872,0.2639,-1.138,-0.2857,0.3797,-0.199,1.3008,-1.4126,-1.3841,7.1232,-1.5903,-0.6301,0.8013,-1.1348,-0.7306,0.006,1.4754,1.1144,-1.5251,-1.277,1.0933,0.1666,-0.5483,2.6779,-1.2728,0.4593,-0.2608,0.1183,-2.1036,-0.3119,-1.0469,0.2662,0.7156,0.0328,0.3441,-0.1147,-0.0553,-0.4123,-3.2276,2.5201,1.7362,-2.9654,0.9641,-1.7355,-0.1573,2.9135],
    wBias: -1.5,
    displayTitle: true,
  };
  
  /*var full_demo_settings = {
    divName: 'intro_demo',
    smallMode: false,
    timeLimit: 500,
    useFineTune: false,
    drawBar: false,
    showInstructions: false,
    hardMode: false,
    aVec: [1,1,1,1,1,1,7,7,5,1,5,5,4,1,7,3,9,1,3,7,9,5,4,3,9,7,1,7,1],
    wKey: [10,35,36,41,64,69,95,97,108,125,128,142,157,202,231,257,289,302,331,361,362,363,364,367,368,373,374,376,394,395,398,401,403,425,461,484,517,543,574,576,602,603,604,606,633,662,692,722,723,753,782,811],
    weights: [-0.1783,-0.0303,1.5435,1.8088,-0.857,1.024,-0.3872,0.2639,-1.138,-0.2857,0.3797,-0.199,1.3008,-1.4126,-1.3841,7.1232,-1.5903,-0.6301,0.8013,-1.1348,-0.7306,0.006,1.4754,1.1144,-1.5251,-1.277,1.0933,0.1666,-0.5483,2.6779,-1.2728,0.4593,-0.2608,0.1183,-2.1036,-0.3119,-1.0469,0.2662,0.7156,0.0328,0.3441,-0.1147,-0.0553,-0.4123,-3.2276,2.5201,1.7362,-2.9654,0.9641,-1.7355,-0.1573,2.9135],
    wBias: -1.5,
    displayTitle: true,
  };*/
  var custom_p5 = new p5(cartpole_demo(full_demo_settings), 'intro_demo');
// info={
//     name:"prediction",
//     parent: 'CNN',
//     classes: 'output',
//     outputNeigh:-1 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1
// };
// GLOBALS.addNode(0,12,0,false,info);
// info={
//     name:"categoricalCrossentropy",
//     metrics: ['accuracy'],
//     parent: 'CNN',
//     classes: 'loss',
//     outputNeigh:6 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

// };
// GLOBALS.addNode(0,11,1,false,info);
// info={
//     name:"dense",
//     inputShape:"(batch,256)",
//     kernelInitializer: 'varianceScaling',
//     activation: 'softmax',
//     outputShape:"(batch,10)",
//     numParams:2570,
//     Trainable:true,
//     parent: 'CNN',
//     classes: 'layers',
//     outputNeigh:4 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

// };
// GLOBALS.addNode(0,11,0,false,info);

// info={
//     name:"flatten",
//     inputShape:"(batch,4,4,16)",
//     outputShape:"(batch,256)",
//     numParams:0,
//     Trainable:true,
//     parent: 'CNN',
//     classes: 'layers',
//     outputNeigh:6 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

// };
// GLOBALS.addNode(0,10,1,false,info);

// info={
//     name:"maxPooling2d",
//     inputShape:"(batch,12,12,8)",
//     poolSize:"[2,2]",
//     strides:"[2,2]",
//     outputShape:"(batch,4,4,16)",
//     numParams:0,
//     Trainable:true,
//     parent: 'CNN',
//     classes: 'layers',
//     outputNeigh:4 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

// };
// GLOBALS.addNode(0,10,0,false,info);

// info={
//     name:"conv2d",
//     inputShape:"(batch,12,12,8)",
//     kernelSize:5,
//     filters:16,
//     strides:1,
//     activation: 'relu',
//     kernelInitializer: 'varianceScaling',
//     outputShape:"(batch,8,8,16)",
//     numParams:3216,
//     Trainable:true,
//     parent: 'CNN',
//     classes: 'layers',
//     outputNeigh:6 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

// };
// GLOBALS.addNode(0,9,1 ,false,info);

// info={
//     name:"maxPooling2d",
//     inputShape:"(batch,24,24,8)",
//     poolSize:"[2,2]",
//     strides:"[2,2]",
//     outputShape:"(batch,12,12,8)",
//     numParams:0,
//     Trainable:true,
//     parent: 'CNN',
//     classes: 'layers',
//     outputNeigh:4 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

// };
// GLOBALS.addNode(0,9,0 ,false,info);

// info={
//     name:"conv2d",
//     inputShape:"(batch,28,28,1)",
//     kernelSize:5,
//     filters:8,
//     strides:1,
//     activation: 'relu',
//     kernelInitializer: 'varianceScaling',
//     outputShape:"(batch,24,24,8)",
//     numParams:208,
//     Trainable:true,
//     parent: 'CNN',
//     classes: 'layers',
//     outputNeigh:6 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

// };
// GLOBALS.addNode(0,8,1 ,false,info);

// var info={
//     name:"MNIST",
//     imageSize:"(28,28)",
//     numDatasets:65000,
//     numTraining:55000,
//     numTest:65000-55000,
//     parent: 'CNN',
//     classes: 'input',
//     outputNeigh:4 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1
// };
// GLOBALS.addNode(0,8,0 ,false,info);

// info={
//     name:"Test",
//     imageSize:"(1,28,28)",
//     parent: 'CNN',
//     classes: 'viz',
//     outputNeigh:5 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1
// };
// GLOBALS.addNode(0,8,2 ,false,info);
var color1= "#ffffff"; //white
var color11= "#ffffff8c"; //white transparent
var color2= "#020227";  //dark blue
var color3= "#1c5c61"; //teal
var color33= "#1c5c618c"; //teal transparent
var color4= "#fa6e70"; //red/orange
var color44= "#fa6e708c"; //red/orange
var color5="#380152"; //purple
var color6="#696767"; //grey
var font= "consolas";//'font-family'

var cyy = cytoscape({
  container: document.getElementById('cyy'),
  ready: function(){
      var api = this.expandCollapse({
          layoutBy: {
              name: "cose-bilkent",
              animate: "end",
              randomize: false,
              fit: true
          },
          fisheye: true,
          animate: false,
          undoable: false
      });
      // api.collapseAll();
  },
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'content': 'data(name)',
        'text-valign': 'center',
        'color': 'white',
        'font-family': "consolas",
        // 'font-family':"Times New Roman",
        'width': 120,
        'height': 120
      })
    .selector('edge')
      .css({
        'content': 'data(name)',
        'width': 8,
        'line-color': color6,
        'target-arrow-color': color6,
        'source-arrow-color': color6,
        'target-arrow-shape': 'vee',
        'curve-style': 'bezier', //straight
      })
    .selector(':selected')
      .css({
        "border-width": 4,
        'line-color': color4,
        'target-arrow-color': color4,
        "border-color": color4
      })
    .selector('$node > node')
      .css({
        'shape': 'roundrectangle',
        'text-valign': 'top',
        'background-color': '#ccc',
        'background-opacity': 0.1,
        'color': '#888',
        'text-outline-width':
        0,
        'font-size': 25
      })
    .selector('#core, #app')
      .css({
        'width': 120,
        'height': 120,
        'font-size': 25
      })
    .selector('#api')
      .css({
        'padding-top': 20,
        'padding-left': 20,
        'padding-bottom': 20,
        'padding-right': 20
      })
    .selector('#ext, .ext')
      .css({
        'background-color': color6,
        // 'text-outline-color': '#93CDDD',
        'line-color': color6,
        'target-arrow-color': color6,
        'color': color1,
        'font-family':font,
        "text-wrap": "wrap",
      })
    .selector('#input, .input')
      .css({
        'background-color': color3,
        // 'text-outline-color': '#93CDDD',
        'line-color': color3,
        'target-arrow-color': color3,
        'color': color1,
        'font-family':font,
        "text-wrap": "wrap",
      })
    .selector('#output, .output')
      .css({
        'background-color': color4,
        // 'text-outline-color': '#93CDDD',
        'line-color': color4,
        'target-arrow-color': color4,
        'color': color1,
        'font-family':font,
        "text-wrap": "wrap",
      })
    .selector('#layers, .layers')
      .css({
        'background-color': color5,
        // 'text-outline-color': '#93CDDD',
        'line-color': color5,
        'target-arrow-color': color5,
        'color': color1,
        'font-family':font,
        "text-wrap": "wrap",
      })
      .selector('#exte, .exte')
      .css({
        'background-color': color3,
        // 'text-outline-color': '#93CDDD',
        'line-color': color3,
        'target-arrow-color': color3,
        'color': color1,
        'font-family':font,
        'curve-style': 'bezier',
        'width': 2,
        "text-wrap": "wrap",
      })
    .selector('#app, .app')
      .css({
        'background-color': '#F79646',
        'text-outline-color': '#F79646',
        'line-color': '#F79646',
        'target-arrow-color': '#F79646',
        
        'color': '#fff'
      })
    .selector('#viz, .viz')
      .css({
        'background-fit': 'cover',
        'background-image': 'https://live.staticflickr.com/7272/7633179468_3e19e45a0c_b.jpg'
      })
    .selector('#cy')
      .css({
        'background-opacity': 0,
        'border-width': 1,
        'border-color': '#aaa',
        'border-opacity': 0.3,
        'font-size': 50,
        'padding-top': 40,
        'padding-left': 40,
        'padding-bottom': 40,
        'padding-right': 40
      })
      .selector('.eh-handle')
      .css({
        'background-color': color4,
        'width': 12,
        'height': 12,
        'shape': 'ellipse',
        'overlay-opacity': 0,
        'border-width': 12, // makes the handle easier to hit
        'border-opacity': 0
      })
      .selector('.eh-hover')
        .css({
          'background-color': color4
      })
      .selector('.eh-source')
        .css({
          'border-width': 2,
          'border-color': color4
      })
      .selector('.eh-target')
        .css({
          'border-width': 2,
          'border-color': color4
      })
      .selector('.eh-preview, .eh-ghost-edge')
        .css({
          'background-color': color4,
          'line-color': color4,
          'target-arrow-color': color4,
          'source-arrow-color': color4
      })
      .selector('.eh-ghost-edge.eh-preview-active')
        .css({
          'opacity': 0
      }),

  elements: {
    nodes: [
      
      ///legend
      {
        data: { id: 'legend', name: 'Legend' ,},
      },
      {
        data: { id: 'inputs', name: 'inputs',parent:'legend' },
        position: { x: 0, y: 0 },classes: 'input',
      },
      {
        data: { id: 'outputs', name: 'outputs',parent:'legend'},
        position: { x: 0, y: 0 },classes: 'output',
      },
      {
        data: { id: 'layers', name: 'Layers',parent:'legend'},
        position: { x: 0, y: 0 },classes: 'layers',
      },
      {
        data: { id: 'losses', name: 'Losses',parent:'legend'},
        position: { x: 0, y: 0 },classes: 'loss',
      },
      {
        data: { id: 'vizs', name: 'Viz',parent:'legend'},
        position: { x: 0, y: 0 },classes: 'viz',
      },
      ////

      {
        data: { id: 'CNN', name: 'CNN' },
      },

      ///////////hardware
      
      
      
      // {
      //   data: { id: '[0,12,0]', name: 'prediction', parent: 'CNN' },
      //   position: { x: 0, y: 0 },classes: 'output',
      // },
      // {
      //   data: { id: '[0,11,1]', name: 'categoricalCrossentropy', parent: 'CNN' },
      //   position: { x: 0, y: 0 },classes: 'loss',
      // },
      // {
      //   data: { id: '[0,11,0]', name: 'dense', parent: 'CNN' },
      //   position: { x: 0, y: 0 },classes: 'layers',
      // },
      // {
      //   data: { id: '[0,10,1]', name: 'flatten', parent: 'CNN' },
      //   position: { x: 0, y: 0 },classes: 'layers',
      // },
      // {
      //   data: { id: '[0,10,0]', name: 'maxPooling2d', parent: 'CNN' },
      //   position: { x: 0, y: 0 },classes: 'layers',
      // },
      // {
      //   data: { id: '[0,9,1]', name: 'conv2d', parent: 'CNN' },
      //   position: { x: 0, y: 0 },classes: 'layers',
      // },
      // {
      //   data: { id: '[0,9,0]', name: 'maxPooling2d', parent: 'CNN' },
      //   position: { x: 0, y: 0 },classes: 'layers',
      // },
      // {
      //   data: { id: '[0,8,1]', name: 'conv2d', parent: 'CNN' },
      //   position: { x: 0, y: 0 },classes: 'layers',
      // },
      // {
      //   data: { id: '[0,8,0]', name: 'MNIST',parent:'CNN' },
      //   position: { x: 0, y: 0 },classes: 'input',
      // },
      // {
      //   data: { id: '[0,8,2]', name: 'Test',parent:'CNN' },
      //   position: { x: 0, y: 0 },classes: 'viz',
      // },


    ],
    edges: [

      // { data: { source: '[0,8,0]', target: '[0,8,1]' , name: ''},classes: 'exte', },
      // { data: { source: '[0,8,1]', target: '[0,9,0]' , name: ''},classes: 'exte', },
      // { data: { source: '[0,9,0]', target: '[0,9,1]' , name: ''},classes: 'exte', },
      // { data: { source: '[0,9,1]', target: '[0,10,0]' , name: ''},classes: 'exte', },
      // { data: { source: '[0,10,0]', target: '[0,10,1]' , name: ''},classes: 'exte', },
      // { data: { source: '[0,10,1]', target: '[0,11,0]' , name: ''},classes: 'exte', },
      // { data: { source: '[0,11,0]', target: '[0,11,1]' , name: ''},classes: 'exte', },
      // { data: { source: '[0,11,1]', target: '[0,12,0]' , name: ''},classes: 'exte', },
      // { data: { source: '[0,8,2]', target: '[0,8,1]' , name: ''},classes: 'exte', },



    ]
  },

  layout: {
    name: 'preset'
  }
});

var api = cyy.expandCollapse('get');

api.expandAll();

// cy.$('#vizs').style('background-image', 'https://farm6.staticflickr.com/5109/5817854163_eaccd688f5_b.jpg');
// cy.$('#vizs').style('background-image', dataUri);

// console.log(cyy.$id("vizs"))


function createImage(width,height){
    var buffer = new Uint8ClampedArray(width * height * 4); // have enough bytes

    for(var y = 0; y < height; y++) {
        for(var x = 0; x < width; x++) {
            var pos = (y * width + x) * 4; // position in buffer based on x and y
            buffer[pos  ] = 255;           // some R value [0, 255]
            buffer[pos+1] = 0;           // some G value
            buffer[pos+2] = 0;           // some B value
            buffer[pos+3] = 255;           // set alpha channel
        }
    }
    // create off-screen canvas element
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d');

    canvas.width = width;
    canvas.height = height;

    // create imageData object
    var idata = ctx.createImageData(width, height);

    // set our buffer as source
    idata.data.set(buffer);

    // update canvas with new data
    ctx.putImageData(idata, 0, 0);

    var dataUri = canvas.toDataURL(); // produces a PNG file
    // console.log(dataUri)
    return dataUri;
    
}

document.addEventListener('selectNode', function (e) { 
        
  var tgt=cyy.$id(e.detail.id);
  cyy.nodes().unselect();//unselect the rest
  tgt.select();
  
}, false);

cyy.on('tap', 'node', function(){

  if (this.selected()) {
    //console.log('double clicked ' + this.id());
    if(this.id()=="[0,8,2]") //example node
    {
      GLOBALS.runExamplePrediction();

    }
  }

  // var nodes = this;
  // GLOBALS.selectedjson=this._private.data.data;
  var pos=utils.getXYZfromName(this.data('id'));
  GLOBALS.selectNode(pos.x,pos.y,pos.z);

  
  // this.trigger('blaa');
});

document.addEventListener('addNode', function (e) { 
  cyy.edges().unselect();//unselect the rest

  var neighborhood=0;
  
  cyy.add({
      classes: ''+e.detail.data.classes,
      data: { 
          id: e.detail.id,
          name: e.detail.data.name,
          parent: ''+e.detail.data.parent,
          
          isNeighborhood :0,
          data:{
              id: e.detail.id,
              name: '[' +e.detail.x +"," +e.detail.y+"," +e.detail.z+']',
              neighbors:[],
              dnn:e.detail.data
          }
      },
      position: {
          x: e.detail.posX,
          y: e.detail.posY,
      }
  });

  x=parseInt(e.detail.x);
  y=parseInt(e.detail.y);
  z=parseInt(e.detail.z);
  var i1,j1,k1;

  //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1
  if(parseInt(e.detail.data.outputNeigh)==0){
    i1=x+1;
    j1=y+0;
    k1=z+0;

  }else if(parseInt(e.detail.data.outputNeigh)==1){
    i1=x-1;
    j1=y+0;
    k1=z+0;

  }else if(parseInt(e.detail.data.outputNeigh)==2){
    i1=x+0;
    j1=y+1;
    k1=z+0;

  }else if(parseInt(e.detail.data.outputNeigh)==3){
    i1=x+0;
    j1=y-1;
    k1=z+0;

  }else if(parseInt(e.detail.data.outputNeigh)==4){
    i1=x+0;
    j1=y+0;
    k1=z+1;

  }else if(parseInt(e.detail.data.outputNeigh)==5){
    i1=x+0;
    j1=y+0;
    k1=z-1;

  }else if(parseInt(e.detail.data.outputNeigh)==6){
    i1=x+0;
    j1=y+1;
    k1=z-1;

  }

  if(parseInt(e.detail.data.outputNeigh)>=0){
    cyy.add([
      { group: "edges",data: { name:'', source: '[' +x +"," +y+","+z+']', target: '[' +i1 +"," +j1+","+k1+']'}}
    ]);

  }
  

  

  // addEdges(e.detail.x,e.detail.y,e.detail.z);

  // var tgt=cy.$id('[' +e.detail.x +"," +e.detail.y+","+e.detail.z+']');
  // updateName(tgt);

  // neighborhood=findNeighborhood(tgt);

  // tgt.move({parent: ''+neighborhood});
  // tgt._private.data.data.parent= neighborhood;

  api.expandAll();
  
}, false);

document.addEventListener('updateNode', function (e) { 
  cyy.edges().unselect();//unselect the rest

  var tgt=cyy.$id(''+e.detail.id);
  tgt.remove();
  console.log(tgt._private.position.x)

  // tgt._private.data.name=e.detail.dnn.name;
  // tgt._private.data.classes=e.detail.dnn.classes;
  // tgt._private.data.parent=''+e.detail.dnn.parent;
  // tgt._private.data.data.dnn=''+e.detail.dnn;
  
  cyy.add({
      classes: ''+e.detail.dnn.classes,
      data: { 
          id: e.detail.id,
          name: e.detail.dnn.name,
          parent: ''+e.detail.dnn.parent,
          
          isNeighborhood :0,
          data:{
              id: e.detail.id,
              name: '[' +e.detail.x +"," +e.detail.y+"," +e.detail.z+']',
              neighbors:[],
              dnn:e.detail.dnn
          }
      },
      position: {
          x: e.detail.posX,
          y: e.detail.posY,
      }
  });

  x=parseInt(e.detail.x);
  y=parseInt(e.detail.y);
  z=parseInt(e.detail.z);
  var i1,j1,k1;

  //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1
  if(parseInt(e.detail.dnn.outputNeigh)==0){
    i1=x+1;
    j1=y+0;
    k1=z+0;

  }else if(parseInt(e.detail.dnn.outputNeigh)==1){
    i1=x-1;
    j1=y+0;
    k1=z+0;

  }else if(parseInt(e.detail.dnn.outputNeigh)==2){
    i1=x+0;
    j1=y+1;
    k1=z+0;

  }else if(parseInt(e.detail.dnn.outputNeigh)==3){
    i1=x+0;
    j1=y-1;
    k1=z+0;

  }else if(parseInt(e.detail.dnn.outputNeigh)==4){
    i1=x+0;
    j1=y+0;
    k1=z+1;

  }else if(parseInt(e.detail.dnn.outputNeigh)==5){
    i1=x+0;
    j1=y+0;
    k1=z-1;

  }else if(parseInt(e.detail.dnn.outputNeigh)==6){
    i1=x+0;
    j1=y+1;
    k1=z-1;

  }

  if(parseInt(e.detail.dnn.outputNeigh)>=0){
    cyy.add([
      { group: "edges",data: { name:'', source: '[' +x +"," +y+","+z+']', target: '[' +i1 +"," +j1+","+k1+']'}}
    ]);

  }
  

  

  // addEdges(e.detail.x,e.detail.y,e.detail.z);

  // var tgt=cy.$id('[' +e.detail.x +"," +e.detail.y+","+e.detail.z+']');
  // updateName(tgt);

  // neighborhood=findNeighborhood(tgt);

  // tgt.move({parent: ''+neighborhood});
  // tgt._private.data.data.parent= neighborhood;

  // api.expandAll();
  
}, false);

document.addEventListener('removeNode', function (e) { 
  
  var tgt=cyy.$id(e.detail.id);
  tgt.remove();

}, false);


var eh = cyy.edgehandles();

document.querySelector('#draw-on').addEventListener('click', function() {
  eh.enableDrawMode();
});

document.querySelector('#draw-off').addEventListener('click', function() {
  eh.disableDrawMode();
});

document.querySelector('#start').addEventListener('click', function() {
  eh.start( cyy.$('node:selected') );
});



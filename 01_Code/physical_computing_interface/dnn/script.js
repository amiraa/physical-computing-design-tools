console.log('Hello TensorFlow');

import {MnistData} from './data.js';

async function showExamples(data) {
  // Create a container in the visor
  const surface =
    tfvis.visor().surface({ name: 'Input Data Examples', tab: 'Input Data'});  

  // Get the examples
  const examples = data.nextTestBatch(20);
  const numExamples = examples.xs.shape[0];
  
  // Create a canvas element to render each example
  for (let i = 0; i < numExamples; i++) {
    const imageTensor = tf.tidy(() => {
      // Reshape the image to 28x28 px
      return examples.xs
        .slice([i, 0], [1, examples.xs.shape[1]])
        .reshape([28, 28, 1]);
    });
    
    const canvas = document.createElement('canvas');
    canvas.width = 28;
    canvas.height = 28;
    canvas.style = 'margin: 4px;';
    
    await tf.browser.toPixels(imageTensor, canvas);
    console.log(imageTensor)
    surface.drawArea.appendChild(canvas);

    imageTensor.dispose();
  }
}

async function vizExample(data){
    const examples = data.nextTestBatch(1);
    const imageTensor = tf.tidy(() => {
        // Reshape the image to 28x28 px
        return examples.xs
            .slice([0, 0], [1, examples.xs.shape[1]])
            .reshape([28, 28, 1]);
    });
    const imageArray=imageTensor.arraySync();
    var dataUri= createImage(imageArray);
    // cyy.$('#ex').style('background-image', dataUri); //?? todo

    cyy.$id("[0,8,2]").style('background-image', dataUri); //?? todo
    updateVariable('[0,12,0]', "????");

    
}

function updateVariable(name, value){
    cyy.$id(name).data('name', 'Prediction\n'+value);
}

function createImage(imageArray){
    const width=imageArray.length;
    const height=imageArray[0].length;

    var buffer = new Uint8ClampedArray(width * height * 4); // have enough bytes

    for(var y = 0; y < height; y++) {
        for(var x = 0; x < width; x++) {
            var pos = (y * width + x) * 4; // position in buffer based on x and y
            buffer[pos  ] = imageArray[y][x]*255;           // some R value [0, 255]
            buffer[pos+1] = imageArray[y][x]*255;           // some G value
            buffer[pos+2] = imageArray[y][x]*255;           // some B value
            buffer[pos+3] = 255;           // set alpha channel
        }
    }
    // create off-screen canvas element
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d');

    canvas.width = width;
    canvas.height = height;

    // create imageData object
    var idata = ctx.createImageData(width, height);

    // set our buffer as source
    idata.data.set(buffer);

    // update canvas with new data
    ctx.putImageData(idata, 0, 0);

    var dataUri = canvas.toDataURL(); // produces a PNG file
    // console.log(dataUri)
    return dataUri;
    
}

var data,model;
async function load(){
    data = new MnistData();
    await data.load();
    await vizExample(data);
    console.log(data)
    model = getModel();
}

async function run() {
    // const data = new MnistData();
    // await data.load();
    // await vizExample(data)
    // // await showExamples(data);
    // console.log(data)
   
    tfvis.show.modelSummary({name: 'Model Architecture'}, model);
    await train(model, data);
    await doPrediction1(model, data,  1)
    await showAccuracy(model, data);
    await showConfusion(model, data);


}

async function runExample() {

    await doPrediction1(model, data,  1);
}

function getModel() {
    const model = tf.sequential();

    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;
    const IMAGE_CHANNELS = 1;  

    // In the first layer of our convolutional neural network we have 
    // to specify the input shape. Then we specify some parameters for 
    // the convolution operation that takes place in this layer.
    model.add(tf.layers.conv2d({
        inputShape: [IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS],
        kernelSize: 5,
        filters: 8,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'varianceScaling'
    }));

    // The MaxPooling layer acts as a sort of downsampling using max values
    // in a region instead of averaging.  
    model.add(tf.layers.maxPooling2d({poolSize: [2, 2], strides: [2, 2]}));

    // Repeat another conv2d + maxPooling stack. 
    // Note that we have more filters in the convolution.
    model.add(tf.layers.conv2d({
        kernelSize: 5,
        filters: 16,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'varianceScaling'
    }));
    model.add(tf.layers.maxPooling2d({poolSize: [2, 2], strides: [2, 2]}));

    // Now we flatten the output from the 2D filters into a 1D vector to prepare
    // it for input into our last layer. This is common practice when feeding
    // higher dimensional data to a final classification output layer.
    model.add(tf.layers.flatten());

    // Our last layer is a dense layer which has 10 output units, one for each
    // output class (i.e. 0, 1, 2, 3, 4, 5, 6, 7, 8, 9).
    const NUM_OUTPUT_CLASSES = 10;
    model.add(tf.layers.dense({
        units: NUM_OUTPUT_CLASSES,
        kernelInitializer: 'varianceScaling',
        activation: 'softmax'
    }));


    // Choose an optimizer, loss function and accuracy metric,
    // then compile and return the model
    const optimizer = tf.train.adam();
        model.compile({
        optimizer: optimizer,
        loss: 'categoricalCrossentropy',
        metrics: ['accuracy'],
    });

    return model;
}

async function train(model, data) {
    const metrics = ['loss', 'val_loss', 'acc', 'val_acc'];
    const container = {
        name: 'Model Training', styles: { height: '1000px' }
    };
    const fitCallbacks = tfvis.show.fitCallbacks(container, metrics);

    const BATCH_SIZE = 512;
    const TRAIN_DATA_SIZE = 5500;
    const TEST_DATA_SIZE = 1000;

    const [trainXs, trainYs] = tf.tidy(() => {
        const d = data.nextTrainBatch(TRAIN_DATA_SIZE);
        return [
        d.xs.reshape([TRAIN_DATA_SIZE, 28, 28, 1]),
        d.labels
        ];
    });

    const [testXs, testYs] = tf.tidy(() => {
        const d = data.nextTestBatch(TEST_DATA_SIZE);
        return [
        d.xs.reshape([TEST_DATA_SIZE, 28, 28, 1]),
        d.labels
        ];
    });

    return model.fit(trainXs, trainYs, {
        batchSize: BATCH_SIZE,
        validationData: [testXs, testYs],
        epochs: 10,
        shuffle: true,
        callbacks: fitCallbacks
    });
}

const classNames = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];

function doPrediction1(model, data, testDataSize = 1) {
    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;
    const testData = data.nextTestBatch(1);


    const imageTensor = tf.tidy(() => {
        // Reshape the image to 28x28 px
        return testData.xs
            .slice([0, 0], [1, testData.xs.shape[1]])
            .reshape([28, 28, 1]);
    });
    const imageArray=imageTensor.arraySync();
    var dataUri= createImage(imageArray);
    cyy.$id("[0,8,2]").style('background-image', dataUri);
    


    const testxs = testData.xs.reshape([testDataSize, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);

    const labels = testData.labels.argMax([-1]);
    const preds = model.predict(testxs).argMax([-1]);
  
    testxs.dispose();
    console.log(preds.arraySync());

    updateVariable('[0,12,0]', ""+preds.arraySync());
    return [preds, labels];
}


function doPrediction(model, data, testDataSize = 500) {
  const IMAGE_WIDTH = 28;
  const IMAGE_HEIGHT = 28;
  const testData = data.nextTestBatch(testDataSize);
  const testxs = testData.xs.reshape([testDataSize, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);
  const labels = testData.labels.argMax([-1]);
  const preds = model.predict(testxs).argMax([-1]);

  testxs.dispose();
  return [preds, labels];
}


async function showAccuracy(model, data) {
  const [preds, labels] = doPrediction(model, data);
  const classAccuracy = await tfvis.metrics.perClassAccuracy(labels, preds);
  const container = {name: 'Accuracy', tab: 'Evaluation'};
  tfvis.show.perClassAccuracy(container, classAccuracy, classNames);

  labels.dispose();
}

async function showConfusion(model, data) {
  const [preds, labels] = doPrediction(model, data);
  const confusionMatrix = await tfvis.metrics.confusionMatrix(labels, preds);
  const container = {name: 'Confusion Matrix', tab: 'Evaluation'};
  tfvis.render.confusionMatrix(
      container, {values: confusionMatrix}, classNames);

  labels.dispose();
}


document.addEventListener('DOMContentLoaded', load);
document.addEventListener('runNode', run);
document.addEventListener('runExamplePrediction', runExample);

// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

/////////////////function calls/////////////////
//todo when calling say which gridsize and grid type
var utils= new utilities();
var GLOBALS=new globals(utils);

// 
var three=new threejs(GLOBALS,utils,'webgl','threejs1');
three.init();


initGraph();// todo enclose into class
initEditor();// todo enclose into class

var assembler= new Assembler(three,GLOBALS,1,50,[new THREE.Vector3(0,0,0)],[new THREE.Vector3(GLOBALS.gridSize/2.0*GLOBALS.voxelSpacing,0,0)]);
assembler.run();

info={
    name:"prediction",
    parent: 'CNN',
    classes: 'output',
    outputNeigh:-1 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1
};
GLOBALS.addNode(0,12,0,false,info);
info={
    name:"categoricalCrossentropy",
    metrics: ['accuracy'],
    parent: 'CNN',
    classes: 'loss',
    outputNeigh:6 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

};
GLOBALS.addNode(0,11,1,false,info);
info={
    name:"dense",
    inputShape:"(batch,256)",
    kernelInitializer: 'varianceScaling',
    activation: 'softmax',
    outputShape:"(batch,10)",
    numParams:2570,
    Trainable:true,
    parent: 'CNN',
    classes: 'layers',
    outputNeigh:4 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

};
GLOBALS.addNode(0,11,0,false,info);

info={
    name:"flatten",
    inputShape:"(batch,4,4,16)",
    outputShape:"(batch,256)",
    numParams:0,
    Trainable:true,
    parent: 'CNN',
    classes: 'layers',
    outputNeigh:6 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

};
GLOBALS.addNode(0,10,1,false,info);

info={
    name:"maxPooling2d",
    inputShape:"(batch,12,12,8)",
    poolSize:"[2,2]",
    strides:"[2,2]",
    outputShape:"(batch,4,4,16)",
    numParams:0,
    Trainable:true,
    parent: 'CNN',
    classes: 'layers',
    outputNeigh:4 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

};
GLOBALS.addNode(0,10,0,false,info);

info={
    name:"conv2d",
    inputShape:"(batch,12,12,8)",
    kernelSize:5,
    filters:16,
    strides:1,
    activation: 'relu',
    kernelInitializer: 'varianceScaling',
    outputShape:"(batch,8,8,16)",
    numParams:3216,
    Trainable:true,
    parent: 'CNN',
    classes: 'layers',
    outputNeigh:6 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

};
GLOBALS.addNode(0,9,1 ,false,info);

info={
    name:"maxPooling2d",
    inputShape:"(batch,24,24,8)",
    poolSize:"[2,2]",
    strides:"[2,2]",
    outputShape:"(batch,12,12,8)",
    numParams:0,
    Trainable:true,
    parent: 'CNN',
    classes: 'layers',
    outputNeigh:4 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

};
GLOBALS.addNode(0,9,0 ,false,info);

info={
    name:"conv2d",
    inputShape:"(batch,28,28,1)",
    kernelSize:5,
    filters:8,
    strides:1,
    activation: 'relu',
    kernelInitializer: 'varianceScaling',
    outputShape:"(batch,24,24,8)",
    numParams:208,
    Trainable:true,
    parent: 'CNN',
    classes: 'layers',
    outputNeigh:6 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1

};
GLOBALS.addNode(0,8,1 ,false,info);

var info={
    name:"MNIST",
    imageSize:"(28,28)",
    numDatasets:65000,
    numTraining:55000,
    numTest:65000-55000,
    parent: 'CNN',
    classes: 'input',
    outputNeigh:4 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1
};
GLOBALS.addNode(0,8,0 ,false,info);

info={
    name:"Test",
    imageSize:"(1,28,28)",
    parent: 'CNN',
    classes: 'viz',
    outputNeigh:5 //-1:none,0:x+1,1:x-1,2:y+1,3:y-1,4:z+1,5:z-1,6:y+1&&z-1
};
GLOBALS.addNode(0,8,2 ,false,info);
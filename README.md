# Physical Computing Design Tools (PC Tools)

![](02_Presentation/dice_design.png)

## Description

The Physical Computing Design tool is an integrated design tool for the design, simulation, optimization and fabrication of reconfigurable computing systems.

Traditional electronics design workflows follow a sequential linear process that starts with design, then analysis and simulation, and finally system fabrication and testing. Often these stages are executed independently from each other, using different tools, making it hard to translate the feedback from the simulation or testing stages into viable design amendments. This adds considerable inefficiency to an inherently iterative design workflow. As an alternative, we developed an integrated, closed loop DICE design tool where one can design, simulate, optimize and fabricate re-configurable computing systems

The following steps explain in detail the proposed design workflow:

1. **Design**: The first step is to place the DICE nodes and visualize the 3D assembly of the computing system. 
2. **Programming**: The user then selects individual nodes to specify details, such as processor type, connected neighbors, program, number of operations, maximum token size, and so forth. During these steps, a connectivity graph is generated in real-time, showing the average computation and communication cost of each node based on these preprogrammed node details. In the future, these numbers could be automatically calculated by pre-compiling and profiling code for each node. 
3. **Analysis**: By treating the computation graph as a flow network, graphical algorithms are used to find and highlight computation and communication bottlenecks. Based on this analysis, design recommendations are generated and displayed, including suggestions for different processors, extra nodes, or alternative routing.
4. **Optimization**: If the user wants to further optimize the reconfigurable computing system, the performance projection graph tool visualizes the effect of each hardware and software decision on projected overall system performance. A generative probabilistic programming model is used to draw samples from these inputs and project the cost, energy use, and speed of the system. Using classic inference techniques one is able to infer the optimal hardware configuration for a given software workflow. In the future this could be extended to additionally perform hardware architecture discovery.
5. **Simulation**: the simulation window allows the user to visualize the state and output of the DICE computation. Currently, this shows the output of a simulated computation provided on the host computer prior to physical assembly. In the future, this framework will be used to visualize data from the DICE network. 
6. **Fabrication**: In parallel with the previous steps, the real-time assembly control window provides a simple mechanism for controlling the mechanical DICE assembler. Using this feature, systems can be fabricated, programmed, and tested in real-time to quickly pull in feedback and amend the system architecture accordingly.

This novel integrated design workflow paves the way for the design of discrete integrated circuits but also reconfigurable computing systems that can change and evolve while execution as needed.

----
## Progress

### Demo Links

- **"Physical Computing Interface"** 
  - [Assembler Control Demo](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/index.html)
  - [Voxel Simulation Demo](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/demos/indexSimulation.html)
  - [Convolutional Neural Network (CNN) Demo](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/demos/indexDNN.html)
  - [WANN](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/demos/indexAI.html)
- **"Performance Calculation Graph"** demo lives [here.](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/probabilisticProjections/index.html)
- [Distributed Deep Neural Networks](https://gitlab.cba.mit.edu/amiraa/ddnn)
- UR10 voxel Assembly [demo.](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/assembly/standAloneAssembly.html)

### Demo Videos

![](02_Presentation/191202_demo.mp4)
![](02_Presentation/assemblerFirstTrial.mp4)

----

## Applications
  
### - Digital Material Modeling and Simulation ([Macro-DICE](02_Presentation/macro_dice/macro_dice.md))

### - [AI that Grows](02_Presentation/AI_that_grows/AI_grow.md)

### - [Convolutional Neural Networks](02_Presentation/CNN/CNN.md)

### - [Hardware Architecture Inference and probabilistic programming](02_Presentation/prob/prob.md)

---

### Capabilities and Updates
-  Design
    -  Parametric grid definition (cubic, hexagonal, dice)
-  Assembly/timeline
    -  Timeline for future morphing computing
    -  UR10 
-  Graph
    -  Hierarchy
    -  Automatic neighborhood
    -  highlights
-  Computing
    -  Asynchronous code propagation (distributed max example)
    -  Simple test case find max value distributed
    -  Min cut max flow concept study neighborhood 
-  Hardware optimization
   -  see section below.
-  Improved UI and integration
    -  Radial menu
    -  Json propagation
    -  Highlight selected node
    -  switch windows
 -  Applications
    -  voxel design tools
    -  Convolutional neural networks


---
## TODO and Next Steps:
- [ ] **Documentation**:
  - [ ] in depth documentation of how to do everything
  - [ ] readme page: 
    - [ ] change make it multiple pages
- [ ] **General workflow/UI**:
  - [ ] closure and prototypes for everything
  - [ ] Switch windows
    - [ ] modularize current windows
    - [ ] programmatically call and add windows to UI
    - [ ] add destroy function for each closure
  - [ ] Global selection 
    - [ ] expose global variables
    - [ ] when none of the nodes is selected you can edit the globals
      - [ ] grids (destroy all three js and create a new one)
      - [ ] voxel size
      - [ ] history of add and remove?
  - [ ] Hierarchy
    - [ ] place multiple voxels
    - [ ] change code of multiple voxels
    - [ ] select multiple dice pieces
    - [ ] create groups
  - [ ] Scaling
    - [ ] stress testing scaling
  
- [ ] **Assembly**:
  - [ ] general
    - [ ] fix errors
  - [ ] UR arm
    - [ ] real life connection 
      - [ ] code (from python? or just call python scripts??)
    - [ ] meso dice
      - [ ] work with zach on it
  - [ ] swarm assembly
    - [ ] bill-e assembly of the computation elements
      - [ ] structural robotics
  - [ ] Jiri assembler
    - [ ] cad? visualize
    - [ ] fix grid 
- [ ] **Computation and optimization**:
  - [ ] load c++ or other code
  - [ ] automatically computation delay and communication cost
  - [ ] Optimization strategy
    - [ ] simple show bottlenecks in computation or communication
    - [ ] alternatives and suggestions
      - [ ] local suggestions to divide the computation into two nodes?
    - [ ] shape discovery
      - [ ] add window of probabilistic search parameters
      - [ ] Restructuring code and automatically divide into distributed
- [ ] **Applications/Demonstrations**:
  - [ ] AI
    - [ ] Convolutional Neural networks
      - [ ] highlight bottleneck and communication delay
    - [ ] Probabilistic Programming
      - [ ] add window optimization
    - [ ] Distributed optimization/belief propagation
    - [ ] Bayesian optimization
  - [ ] Voxel Design
    - [ ] add a walking robot
  - [ ] Morphing computing systems examples
    - [ ] Voxel land
      - [ ] a big structure getting assembled assembled while getting bigger
      - [ ] computation getting bigger
    - [ ] dynamic physics simulation
      - [ ] when from one part to another so does the placement of the computing systems
  





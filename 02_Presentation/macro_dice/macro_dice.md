# Digital Material Modeling and Simulation

Case study of the design and optimization of Macro-DICE.

Hardware Implementation:
- [VoxRob](https://gitlab.cba.mit.edu/bej/voxrob)
- [rover](https://gitlab.cba.mit.edu/ccameron/rover)



<img src="./Dice_Voxrob.png" width="100%" />


<img src="./Dice_Voxrob_2.png" width="100%" />

----
## Steps
1. Find Goal
   - CNN to learn target (instructions) from Image
   - [Progress](https://gitlab.cba.mit.edu/amiraa/physical-computing-design-tools/-/blob/master/02_Presentation/CNN/CNN.md)
   - Create/get data to train
2. Auto Encoder to find latent representation for it
3. Control Strategy based on the
   - Train using the simulator + WANN
   - [Progress](../AI_that_grows/AI_grow.md)
4. Fit Arbitrary Graph/data flow on DICE
   - [Progress](https://gitlab.cba.mit.edu/amiraa/physical-computing-design-tools/-/blob/master/02_Presentation/prob/prob.md)
5. Assembly and Testing
   - [Meso-dice assembly](https://gitlab.cba.mit.edu/zfredin/meso-dice)
   - [Robotic assembly](https://gitlab.cba.mit.edu/amiraa/swarm-morphing/-/blob/master/Science_Robotics_Paper/LOI.md)


---
## Simulation Environment

### Rover


- [Check Control Function Graph](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/graph/control/control.html)
  
<img src="./1rover4.gif" width="40%" />
<img src="./1rover2.gif" width="40%" /><br/>
<img src="./1rover1.gif" width="40%" />
<img src="./1rover3.gif" width="40%" />

- [Simulation Demo](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexRover.html)
- [Progress](https://gitlab.cba.mit.edu/amiraa/metavoxels/-/blob/master/02_Presentation/robotics/rover/rover.md)

### Wal

<img src="./walker.gif" width="80%" />

- [Progress](https://gitlab.cba.mit.edu/amiraa/metavoxels/-/blob/master/02_Presentation/robotics/walking/walking.md)


----

## Optimization

- Two level optimization
  - Shape (body) => [Hierarchal Local Online Shape Optimization](https://gitlab.cba.mit.edu/amiraa/metavoxels/-/blob/master/02_Presentation/top_opt/search.md)
  - Control (mind) => [AI that grows](../AI_that_grows/AI_grow.md)
  - Simultaneous optimization of shape and control

---


## Robotics Design
Using the [MetaVoxel simulation tool](https://gitlab.cba.mit.edu/amiraa/metavoxels):

<img src="./dice_assembly.gif" width="80%" /><br/>

---

## Assembly
- Option for different kinds of assembly (swarm assembly)
  
<img src="./assembly.gif" width="80%" /> <br/>
---






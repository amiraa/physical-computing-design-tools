# Probabilistic Programming

- [Gamalon](https://gamalon.com/)
- Using Dice pieces to do **inference** 
  - in order to find the best configuration based on its performance
  - other tasks
- [WebPPL: ](http://webppl.org/)  probabilistic programming for the web
- Probabilistic Graphical Models

![](./pp.png)
![](./ppp.png)

In order to map the hardware architecture to an input dataflow program or computation graph, I modeled the hardware and software models as  probabilistic graphical model, and used probabilistic programming to infer the best hardware architecture choices that will optimize the speed, energy and cost of the system.

I used [WebPPL](http://dippl.org/chapters/02-webppl.html) a probabilistic programming language in javascript. I model the variables I want to infer as if they come from different types of distributions that integrate our priors in them. In the following example I am using markov chain monte carlo as an inference method.

[The demo lives here.](https://amiraa.pages.cba.mit.edu/physical-computing-design-tools/01_Code/physical_computing_interface/probabilisticProjections/index.html)

The structure of the probabilistic program looks like this:

```javascript
var model=function (){

  var chipType=function () {	return uniformDraw(["STM32F412","samd51j19","samd51j20","MAX32660","STM32F412","XMEGA"])};
  var chip=processors.type[chipType()];
  //display(chip.name);

  var ram=chip.ram;
  var cyclesPerSecond =chip.cyclesPerSecond;
  var bitsPerSecond   =chip.bitsPerSecond  ;
  var costPerChip     =chip.costPerChip    ;
  var flopsPerWatt    =chip.flopsPerWatt   ;

  var interconnect=mem(function (model) {	return uniformDraw([2,4,6])});
  var nodesNumber= mem(function (model) {	return uniformDraw([10,20,30,40,50])});

  var dataExchangeSize=mem(function (model) {return gaussian(1e3, 10)}); 
  var memoryPerComputation=mem(function (model) {return gaussian(10e3, 10)}); 
  var numberofComputations=mem(function (model) {return gaussian(200, 10)}); //nodes of similar computations
  var computation = mem(function (model) {return gaussian(100, 20)});  
  var maxNumberOfInterconnects= mem(function (model) {return gaussian(10, 2)}); 

  doCalculation();


  var speed=computationTime+communicationTime; //frame
  var cost=nodesNumber("DICE")*costPerChip;
  var energy= 1 / flopsPerWatt*numberofNodesNeeded*computationTime;//per frame


  condition(speed<0.0012 && cost<180);
  // return [speed,cost,energy]

  if(result==1){
    return chip.name;

  }else if(result==2){
    return interconnect("DICE");

  }else{
    return nodesNumber("DICE");
  }
}

var result=3;
var options = {method: 'MCMC', kernel: 'MH', samples: 10000}
var dist = Infer(options, model)
viz(dist)

display('Expected nodesNumber: ' + expectation(dist))
expectation(dist)
```
Until now I have a dummy computation model. Next steps would be to get a complex computational graph and for each part infer the best hardware architecture for it. Moreover, if the computation graph changes through time one can also alter the hardware architecture to respond to these changes.